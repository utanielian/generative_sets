from data import get_data
from Models import recurrent_model
import os
import torch
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping
import metrics

file_path = '/Files/1_100_100_100_apparel-diaper-feeding_regs.csv'
data_dict = get_data(file_path, ratio_train=0.8, batch_size=800, sorted=True)
hparams = {'n_cell': 500, 'n_layer': 1}
model = recurrent_model.RecurrentModelBaseline(data_dict)
checkpoint_callback = ModelCheckpoint(filepath=os.getcwd() + '/Results/Recurrent/', verbose=True)

# print('BEFORE TRAINING')
# sample = model.sample(data_dict['collection_train'])
# dist_wasserstein, dist_chamfer = metrics.wasserstein_chamfer_distance(data_dict['collection_train'],
#                                                               sample)
# print("The distance (mean, (min,max)) between model and train data")
# print("Wasserstein : ", dist_wasserstein)
# print("Chamfer : ", dist_chamfer)

print('TRAINING')
trainer = pl.Trainer(checkpoint_callback=checkpoint_callback, max_nb_epochs=1000, distributed_backend='dp', gpus=2)
trainer.fit(model)

print('AFTER TRAINING')
sample = model.sample(data_dict['collection_train'])
dist_wasserstein, dist_chamfer = metrics.wasserstein_chamfer_distance(data_dict['collection_train'],
                                                              sample)
print("The distance (mean, (min,max)) between model and train data")
print("Wasserstein : ", dist_wasserstein)
print("Chamfer : ", dist_chamfer)

#Visualisation
first_item_pred = {}
x_pred = []
y_pred = []
first_item_true = {}
for idx in range(len(sample)):
    item = len(sample[idx])
    if item in first_item_pred.keys():
        first_item_pred[item] = first_item_pred[item] + 1
    else:
        first_item_pred[item] = 1

    item = len(data_dict['collection_train'][idx])
    if item in first_item_true.keys():
        first_item_true[item] = first_item_true[item] + 1
    else:
        first_item_true[item] = 1

import matplotlib.pyplot as plt
plt.title('Set size distribution')
plt.bar(first_item_pred.keys(), first_item_pred.values(), alpha=1, label='Predicted sets')
plt.bar(first_item_true.keys(), first_item_true.values(), alpha=0.5, label='True sets')
plt.legend()
plt.show()