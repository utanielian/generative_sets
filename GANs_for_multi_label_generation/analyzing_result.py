import numpy as np
from itertools import product
import matplotlib.pyplot as plt
from tools import movingaverage
import os
import argparse
import json
import sys


def get_config():
    parser = argparse.ArgumentParser()
    parser.add_argument('--folder_result', default = 'results', type=str)
    parser.add_argument('--folder_figure', default = 'figures', type=str)
    opt = parser.parse_args()
    return opt

def find_file(syl1, syl2, listfiles):
    for filename in listfiles:
        if (syl1 in filename) and (syl2 in filename):
            print(filename)
            return filename
    print("Pb: file not found")
    sys.exit()

def plot_results(config):
    if not os.path.exists(config.folder_figure):
        os.makedirs(config.folder_figure+"/")

    for metric in config.metrics:
        empirical_scores, model_scores = list(), list()
        for sample in config.samples:
            filename = find_file("N"+str(sample), config.dataset, os.listdir(config.folder))
            with open(config.folder + "/" + filename) as json_file:
                data = json.load(json_file)
                empirical_scores.append(data["score_empirical"][metric])
                index_model = np.argmin(np.array([elem[0]+elem[1] for elem in data["score_model_training"][metric]]))
                model_scores.append(data["score_model_training"][metric][index_model])
        
        plt.clf()
        plt.figure()
        plt.fill_between(config.samples, [e[0] for e in empirical_scores], [e[1] for e in empirical_scores], \
            color="r", alpha=0.6,  linewidth=1., label="Emp. measure")
        plt.fill_between(config.samples, [e[0] for e in model_scores], [e[1] for e in model_scores], \
            color="b", alpha=0.6,  linewidth=1., label="WGANs model")
        plt.xscale("log", basex=2)
        plt.xticks(config.samples)
        plt.ylabel(metric, fontsize=18)
        plt.legend(loc=1, prop={'size':15})
        plt.tick_params(axis="both", labelsize=18)
        plt.savefig(config.folder_figure + "/" + metric + config.dataset +'.jpeg', bbox_inches="tight")
        plt.savefig(config.folder_figure + "/" + metric + config.dataset + '.pdf', bbox_inches="tight")


config = get_config()
config.samples = [32, 64, 128, 256, 512, 1024]
config.folder = "results/"
config.dataset = "m1"
config.metrics = ["emd", "hausd"]
print(config, flush = True)

plot_results(config)