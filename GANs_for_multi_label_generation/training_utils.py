from my_imports import *

from tools import convert_to_gpu
from generating_data import generate_z, generate_real_data
from defining_models import cal_gradient_penalty


def train_discriminator(discriminator, generator, d_optimizer, step, config):
    z = convert_to_gpu(generate_z(config.batch_size, config.z_var, config), config)
    gz = generator(z)
    d_fake = discriminator(gz)
    realdata = generate_real_data(config.batch_size, "training", config)
    real_data = torch.zeros((config.batch_size, config.output_dim), dtype=float)
    real_data[np.arange(config.batch_size).repeat([*map(len, realdata)]), np.concatenate(realdata)] = 1
    real_data = convert_to_gpu(real_data, config)
    d_real = discriminator(real_data.float())

    if config.disc_type == "simpleReLU":
        gradient_penalty, gradients = cal_gradient_penalty(discriminator, real_data, gz, 0, config = config)
        loss = d_fake.mean() - d_real.mean() + gradient_penalty
    else:
        loss = d_fake.mean() - d_real.mean()
    d_optimizer.zero_grad()
    loss.backward()
    d_optimizer.step()


def train_generator(discriminator, generator, g_optimizer, step, config):
    z = convert_to_gpu(generate_z(config.batch_size, config.z_var, config), config)
    z.requires_grad_(True)
    gz = generator(z)
    d_fake = discriminator(gz)
    loss = - d_fake.mean()
    g_optimizer.zero_grad()
    loss.backward()
    g_optimizer.step()


def save_models(generator, step, config):
    folder = os.path.join(config.name_exp, 'models')
    if not os.path.exists(folder):
        os.makedirs(folder)
    PATH = os.path.join(folder, 'generator_'+str(step)+'.pth')
    torch.save(generator.state_dict(), PATH)

def load_models(generator, step, config):
    folder = os.path.join(config.name_exp, 'models')
    PATH = os.path.join(folder, 'generator_'+str(step)+'.pth')
    generator.load_state_dict(torch.load(PATH))
