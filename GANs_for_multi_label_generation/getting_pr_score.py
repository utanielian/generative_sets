from my_imports import *

from tools import convert_to_gpu
from generating_data import generate_z, generate_real_data

def get_conf_int(data):
    if (np.amax(data)==np.amin(data)):
        return (data[0], data[0])
    else:
        return norm.interval(0.95, loc=np.mean(np.array(data)), scale=np.std(np.array(data))/math.sqrt(len(data)))

def get_pr_scores(metrics, config, generator=None, mode="test", threshold=0.):
    softmax = nn.Softmax(dim=1)
    dict_metrics = dict()
    metrics = ["prec_jaccard", "rec_jaccard", "emd_jaccard", "equal"]
    for elem in metrics:
        dict_metrics[elem] = list()

    for i in range(config.num_runs_knn):
        set_gz = generator(convert_to_gpu(generate_z(config.num_points_sampled_knn, config.z_var, config), config))
        set_gz = torch.where(set_gz>threshold, torch.ones(config.num_points_sampled_knn, config.output_dim), torch.zeros(config.num_points_sampled_knn, config.output_dim))
        
        realdata = generate_real_data(config.num_points_sampled_knn, mode, config)
        set_real_data = torch.zeros((config.num_points_sampled_knn, config.output_dim), dtype=float)
        set_real_data[np.arange(config.num_points_sampled_knn).repeat([*map(len, realdata)]), np.concatenate(realdata)] = 1
        set_real_data = convert_to_gpu(set_real_data, config)

        for metric in ["jaccard"]:    
            prec, rec, emd, equal = manifold_estimate_bis(set_gz, set_real_data, metric, config)
            dict_metrics["prec_"+metric].append(prec)
            dict_metrics["rec_"+metric].append(rec)
            dict_metrics["emd_"+metric].append(emd)
            dict_metrics["equal"].append(equal)

    for metric in metrics:
        dict_metrics[metric] = get_conf_int(dict_metrics[metric])
    return dict_metrics

def manifold_estimate_bis(X_a, X_b, metric, config):
    k = config.kth_nearests[0]
    X_a, X_b = X_a.detach().cpu().numpy(), X_b.detach().cpu().numpy()
    M_aa = ot.dist(X_a, X_a, metric=metric)
    M_bb = ot.dist(X_b, X_b, metric=metric)
    M_ab = ot.dist(X_a, X_b, metric=metric)
    
    precision = len(np.where(np.amin(M_ab.T-np.partition(M_bb, axis=1, kth=k)[:,k], axis=1)<-1e-4)[0])/len(X_a) 
    recall = len(np.where(np.amin(M_ab-np.partition(M_aa, axis=1, kth=k)[:,k], axis=1)<0)[0])/len(X_b)
    emd = ot.emd2(np.ones((len(X_a),))/len(X_a), np.ones((len(X_b),))/len(X_b), M_ab)
    equal = len(np.where(np.amin(M_ab, axis=1)==0)[0])/len(X_a) 
    return precision, recall, emd, equal


def manifold_estimate(X_a, X_b, config):
    size_seta, size_setb, batch_size = X_a.shape[0], X_b.shape[0], config.batch_size_knn

    k_th_distance = convert_to_gpu(torch.zeros((size_seta, len(config.kth_nearests))), config)
    pairwise_distances = convert_to_gpu(torch.zeros((size_seta, size_seta)), config)
    for i in range(size_seta):
        index = i
        while index<size_seta:
            xsquare = torch.sum(X_a[i]**2, dim=0)
            ysquare = torch.sum(X_a[index:min(index+batch_size, size_seta)]**2, dim=1)
            xdoty = torch.sum(torch.mul(X_a[i], X_a[index:min(index+batch_size, size_seta)]), dim=1)
            dist = xsquare - 2*xdoty + ysquare

            pairwise_distances[i, index:min(index+batch_size, size_seta)] = dist
            pairwise_distances[index:min(index+batch_size, size_seta), i] = dist
            index += batch_size
        for k, kth in enumerate(config.kth_nearests):
            k_th_distance_i = np.partition(pairwise_distances[i].detach().cpu().numpy(), kth)[kth]
            k_th_distance[i, k] = float(k_th_distance_i)

    scores = np.zeros(len(config.kth_nearests))
    for i in range(size_setb):
        for k, kth in enumerate(config.kth_nearests):
            index = 0
            while index<size_seta:
                xsquare = torch.sum(X_b[i]**2, dim=0)
                ysquare = torch.sum(X_a[index:min(index+batch_size, size_seta)]**2, dim=1)
                xdoty = torch.sum(torch.mul(X_b[i], X_a[index:min(index+batch_size, size_seta)]), dim=1)
                dist = xsquare - 2*xdoty + ysquare
                differences = dist - k_th_distance[index:min(index+batch_size, size_seta), k]
                if torch.min(differences) <= 0 :
                    index = size_seta
                    scores[k] += 1
                else:
                    index += batch_size
    return scores/size_setb


def hausdorff_estimate(X_a, X_b, config):
    size_seta, size_setb, batch_size = X_a.shape[0], X_b.shape[0], config.batch_size_knn
    min_distance = np.zeros(size_seta)
    for i in range(size_seta):
        index = 0
        pairwise_distances = convert_to_gpu(torch.zeros((size_setb)), config)
        while index<size_setb:
            xsquare = torch.sum(X_a[i]**2, dim=0)
            ysquare = torch.sum(X_b[index:min(index+batch_size, size_setb)]**2, dim=1)
            xdoty = torch.sum(torch.mul(X_a[i], X_b[index:min(index+batch_size, size_setb)]), dim=1)
            dist = xsquare - 2*xdoty + ysquare
            pairwise_distances[index:min(index+batch_size, size_setb)] = dist
            index += batch_size
        min_distance[i] = np.amin(pairwise_distances.detach().cpu().numpy())
    max_of_mins = np.sqrt(np.mean(min_distance))
    return max_of_mins