from my_imports import *

from data import get_data
from defining_models import Generator, Discriminator, Discriminator_bjorckGroupSort
from generating_data import generate_z, generate_real_data, rank_by_discriminator
from getting_pr_score import get_pr_scores
from tools import convert_to_gpu, get_path, get_all_interpolations_MST
from training_utils import train_discriminator, train_generator, save_models

np.random.seed(5015)
torch.manual_seed(5015)


def get_config():
    parser = argparse.ArgumentParser()
    parser.add_argument('--name_exp', type=str, default="default_exp")
    parser.add_argument('--dataset', default='synthetic', type=str)
    parser.add_argument("--device_id", type = int,  default = 0)
    parser.add_argument('-b', '--batch_size', type=int, default=128)
    parser.add_argument("--use_gpu", action='store_true', help='shuffle input data')
    parser.add_argument("--spectral_normalization", action='store_true', help='shuffle input data')
    parser.add_argument("--batch_norm_real_data", default=False, type=bool, help='batch norm input data')
    parser.add_argument('--steps', type=int, default=10001)
    parser.add_argument('--steps_eval', type=int, default=1000)
    
    parser.add_argument("--loss_type", type = str, default = 'wgan-gp')
    parser.add_argument("--gen_type", type = str, default='simple')
    parser.add_argument("--disc_type", type = str, default='simpleReLU')
    parser.add_argument("--gen_lr", type = float, default=0.0001)
    parser.add_argument("--disc_lr", type = float, default=0.0025)
    parser.add_argument('--d_step', type=int, default=3)
    parser.add_argument('--g_step', type=int, default=1)
    parser.add_argument('--g_width', type=int, default=40)
    parser.add_argument('--d_width', type=int, default=40)
    parser.add_argument('--g_depth', type=int, default=3)
    parser.add_argument('--d_depth', type=int, default=6)
    
    #TRAINING
    parser.add_argument('--training_ratio', type=float, default = 0.5)
    parser.add_argument('--training_mode', type=str, default = "training")
    parser.add_argument('--testing_mode', type=str, default = "test")
    parser.add_argument('--metrics', default = 'prec,rec,emd_euclidean,emd_jaccard,hausd,emp_hausd', type=str)
    parser.add_argument('--plot_config', default = True, type=bool)
    parser.add_argument('--num_runs', default=1, type=int)
    parser.add_argument('--num_points_plotted', type=int, default=512)

    #TRUE DIST
    parser.add_argument('--real_dataset_size', type=int, default=1)
    parser.add_argument('--output_dim', type=int, default=2)
    parser.add_argument('--output_modes_locs', default = 5., type=float)
    parser.add_argument('--output_modes', type=int, default=1)
    parser.add_argument('--out_var', type=float, default=0.15)
    parser.add_argument('--z_dim', type=int, default=5)
    parser.add_argument("--z_law", type = str, default = 'gauss')
    parser.add_argument('--z_var', type=float, default=1.0)

    #KNN !!!!!
    parser.add_argument('--num_points_sampled_knn', type=int, default=1024)
    parser.add_argument('--kth_nearests', type=str, default='5')
    parser.add_argument('--num_runs_knn', default=4, type=int)
    parser.add_argument('--batch_size_knn', type=int, default=1024)
    
    opt = parser.parse_args()
    opt.kth_nearests = [int(item) for item in opt.kth_nearests.split(',')]
    opt.metrics = [item for item in opt.metrics.split(',')]
    opt.num_pics = 0
    return opt

config = get_config()
print(config, flush = True)
if not os.path.exists(config.name_exp):
    os.makedirs(config.name_exp)

if config.dataset=="digits":
    file_path = '/Files/toy_problem.csv'
elif config.dataset =="amazon":
    file_path = '/Files/1_100_100_100_apparel_regs.csv'
elif config.dataset =="cocktails":
    file_path = '/Files/cocktail_dataset_mr-boston-flattened.csv'
elif config.dataset =="UK-retail":
    file_path = '/Files/UK-retail-joined.csv'
else:
    print("Not the right dataset")
    sys.exit()

data_dict = get_data(file_path, ratio_train=config.training_ratio, batch_size=128, config=config)
print("Maximum set size", data_dict['SET_SIZE'])
print("Number of classes", data_dict['N_CLASS'])
print("Number of training sets", len(data_dict["collection_train"]))
print("Number of test sets", len(data_dict["collection_test"]))

config.data = data_dict
config.output_dim = data_dict['N_CLASS']
config.train_index, config.test_index = 0, 0

print("Starting training!")
if config.gen_type=="simple":
    generator = convert_to_gpu(Generator(config), config)
if config.disc_type=="simpleReLU":
    discriminator = convert_to_gpu(Discriminator(config), config)
else:
    discriminator = convert_to_gpu(Discriminator_bjorckGroupSort(config), config)

generator.train()
g_optimizer = torch.optim.Adam(generator.parameters(), lr=config.gen_lr, betas=(0.5,0.5))
discriminator.train()
d_optimizer = torch.optim.Adam(discriminator.parameters(), lr=config.disc_lr, betas=(0.5,0.5))
    
for s in range(config.steps):
    for d in range(config.d_step):
        train_discriminator(discriminator, generator, d_optimizer, s, config)
    for g in range(config.g_step):
        train_generator(discriminator, generator, g_optimizer, s, config)
    
    if s%config.steps_eval == 0 and s!= 0 :
        print('Steps', s)
        save_models(generator, s, config)

        def get_scores_and_plot_graphs(generator, mode, threshold, config):
            print("########")
            equal = rank_by_discriminator(generator, discriminator, 20, 1000, threshold, mode, config)
            print("EQUAL", equal)

            dict_scores = get_pr_scores(config.metrics, config, generator, mode, threshold)
            for i in sorted(dict_scores.keys()):
                print(i, dict_scores[i], end =", ")
            print("")

        get_scores_and_plot_graphs(generator, mode="training", threshold=0.5, config=config)
        get_scores_and_plot_graphs(generator, mode="test", threshold=0.5, config=config)

        config.num_pics += 1
        print("")
        print('____________________')        
print("#######################")