from my_imports import *
from tools import convert_to_gpu

def generate_z(batch_size, variance, config):
    mean = np.zeros(config.z_dim)
    cov = np.zeros((config.z_dim,config.z_dim))
    np.fill_diagonal(cov, variance)
    z = np.random.multivariate_normal(mean, cov, batch_size)
    z = torch.from_numpy(z).float()
    return z

def generate_real_data(batch_size, mode, config):
    if mode=="training":
        batch, config.train_index = generate_next_batch(batch_size, config.data["collection_train"], config.train_index, config)
    else:
        batch, config.test_index = generate_next_batch(batch_size, config.data["collection_test"], config.test_index, config)
    return batch

def generate_next_batch(batch_size, data, index, config):
    if index + batch_size < len(data):
        return data[index:index+batch_size], index+batch_size
    else:
        return data[:batch_size], batch_size


def rank_by_discriminator(generator, discriminator, top, size, threshold, mode, config):
    set_gz = generator(convert_to_gpu(generate_z(size, config.z_var, config), config))
    set_gz = torch.where(set_gz>threshold, torch.ones(size, config.output_dim), torch.zeros(size, config.output_dim))
    set_gz = set_gz[torch.argsort(-discriminator(set_gz).squeeze(1))].detach().cpu().numpy()
    
    length = len(config.data["collection_train"]) if mode == "training" else len(config.data["collection_test"])
    realdata = generate_real_data(length, mode, config)
    set_real_data = np.zeros((length, config.output_dim), dtype=float)
    set_real_data[np.arange(length).repeat([*map(len, realdata)]), np.concatenate(realdata)] = 1
    
    M_ab = ot.dist(set_gz, set_real_data, metric="jaccard")
    indexes_present_not_in_training = np.where(np.amin(M_ab, axis=1)>0)[0]
    equal = 1-len(indexes_present_not_in_training)/len(set_gz)

    for i in range(top):
        elem = list(np.where(set_gz[indexes_present_not_in_training[i]]>0)[0])
        #closest = set_real_data[np.argpartition(M_ab[indexes_present_not_in_training[i]], kth=7)[:7]]
        closest = [realdata[index] for index in np.argpartition(M_ab[indexes_present_not_in_training[i]], kth=7)[:7]]
        print(elem, closest)
    return equal