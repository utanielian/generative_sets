import sys, json, random, math, copy
import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import distance_matrix
from scipy.sparse.csgraph import shortest_path
from scipy.spatial.distance import cdist
import os, functools, argparse
from time import time
import pandas as pd
import random
from sklearn.manifold import TSNE
from sklearn import manifold
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import minimum_spanning_tree

import matplotlib.patches as patches
from scipy.stats import gaussian_kde
import seaborn as sns
from scipy.spatial.distance import cdist
from sklearn import manifold
from scipy import ndimage

import functools
from itertools import product
from scipy.stats import norm
import ot

import torch
import torch.nn as nn
from torch.nn import init
import torch.nn.functional as F
from torch.nn.utils import spectral_norm
from torch.optim.optimizer import Optimizer, required
from torch.optim.lr_scheduler import LambdaLR, StepLR
from torch.autograd import Variable
from torch import Tensor
from torch.nn import Parameter
from torchvision import datasets
import torchvision.transforms as transforms
from torch.distributions import MultivariateNormal
from torch.utils.data import DataLoader, TensorDataset

from munch import Munch

from copy import deepcopy