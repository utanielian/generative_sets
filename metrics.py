import numpy as np
import ot
import scipy
import scipy.stats as stats
import random
random.seed(5)
np.random.seed(5)

'''
TOOLS
'''


def jaccard_distance(fst_set, snd_set):
    fst_set = set(fst_set)
    snd_set = set(snd_set)
    intersection = len(fst_set.intersection(snd_set))
    union = len(fst_set.union(snd_set))
    
    return 1 - intersection/(max(union, 10**-6))


def cost_matrix(fst_col, snd_col):
    dist_mat = np.zeros((len(fst_col), len(snd_col)))
    for i, set1 in enumerate(fst_col):
        for j, set2 in enumerate(snd_col):
            dist = float(jaccard_distance(set1, set2))
            dist_mat[i, j] = dist
    return dist_mat

def distance_size_diff(set_a, set_b):                   # Only used in order to verify the rest of the evaluation part
    return np.abs(len(set_a)-len(set_b))

def cost_matrix2(fst_col, snd_col):
    dist_mat = np.zeros((len(fst_col), len(snd_col)))
    for i, set1 in enumerate(fst_col):
        for j, set2 in enumerate(snd_col):
            dist = float(distance_size_diff(set1, set2))
            dist_mat[i, j] = dist
    return dist_mat

def mean_confidence_interval(data, confidence=0.95):
    a = np.array(data)
    n = len(a)
    m = np.mean(a)
    if n == 1:
        return m, m, m
    m, se = np.mean(a), stats.sem(a)
    h = se * scipy.stats.t.ppf((1 + confidence) / 2., n - 1)
    return round(m, 2), (round(m-h, 2), round(m+h, 2))


'''
Collection of sets distance
'''


def chamfer_distance(fst_col, snd_col):
    dist_mat = cost_matrix(fst_col, snd_col)
    sum1 = np.mean(np.amin(dist_mat, axis=0))
    sum2 = np.mean(np.amin(dist_mat, axis=1))
    return sum1 + sum2


def wasserstein_distance(fst_col, snd_col):
    M = cost_matrix(fst_col, snd_col)
    vect_no_const = [1/len(fst_col) for _ in range(len(fst_col))]
    n = ot.emd2(vect_no_const, vect_no_const, M)
    return n


'''
Compute a mean distance and confidence interval between two collections of sets considering both the Wasserstein 
distance and the Chamfer distance including a Jaccard distance between sets.
'''


def wasserstein_chamfer_distance(fst_col, snd_col, n_distance=5):
    n_samples = 200

    wasserstein_dist_list = []
    chamfer_dist_list = []
    pairs = list(zip(fst_col, snd_col))  # make pairs out of the two lists

    for _ in range(n_distance):
        bootstrap = random.sample(pairs, n_samples)  # pick 3 random pairs
        random_fst_col, random_snd_col = zip(*bootstrap)  # separate the pairs

        wasserstein_dist = wasserstein_distance(random_fst_col, random_snd_col)
        chamfer_dist = chamfer_distance(random_fst_col, random_snd_col)

        wasserstein_dist_list.append(wasserstein_dist)
        chamfer_dist_list.append(chamfer_dist)

    # Now we compute the confidence interval of the distances
    mean_wasserstein, interval_wasserstein = mean_confidence_interval(wasserstein_dist_list)
    mean_chamfer, interval_chamfer = mean_confidence_interval(chamfer_dist_list)

    return (mean_wasserstein, interval_wasserstein), (mean_chamfer, interval_chamfer)


if __name__ == '__main__':
    MAX_ITEM = 100
    MAX_SET = 20
    MIN_SET = 10
    MIN_COL = 10
    MAX_COL = 10000
    fst_toy_collection = [list(np.random.randint(MAX_ITEM, size=np.random.randint(MIN_SET, MAX_SET)))
                          for _ in range(np.random.randint(MIN_COL, MAX_COL))]
    snd_toy_collection = [list(np.random.randint(MAX_ITEM, size=np.random.randint(MIN_SET, MAX_SET)))
                          for _ in range(np.random.randint(MIN_COL, MAX_COL))]

    dist_wasserstein, dist_chamfer = wasserstein_chamfer_distance(fst_toy_collection, snd_toy_collection, n_distance=5)
    print("The distance (mean, (min,max)) between the collections are :")
    print("Wasserstein : ", dist_wasserstein)
    print("Chamfer : ", dist_chamfer)