import pandas as pd
import random
import torch
import numpy as np
import os
from torch.utils.data import DataLoader, TensorDataset
import pandas as pd
random.seed(5)


'''
TOOLS
'''


def pad_collection(set_list, set_size):
    padded_collection = []
    for one_set in set_list:
        padded_set = one_set.copy()
        zeros = [0 for _ in range(set_size-len(padded_set))]
        padded_set.extend(zeros)
        padded_collection.append(torch.Tensor(padded_set))
    return padded_collection


def build_subset_target_from_collection(collection_set):
    subset = []
    subset_target = []
    for full_set in collection_set:
        if len(full_set) == 1:                  # Impossible to have a target with a singleton
            continue

        full_set.sort()                         # Using the lexicographic order

        for idx in range(len(full_set) - 1):    # Subsets and corresponding target of the current full_set
            one_subset = full_set[:idx + 1]
            one_subset_target = full_set[idx + 1]

            subset.append(one_subset)
            subset_target.append(one_subset_target)
    return subset, subset_target


'''
Generate a dataset object containing data used to train the recurrent model baseline
'''

def split_set(arg_set, max_size_set):
    sub_set = []
    one_set = []
    for item in arg_set:
        if len(one_set) < max_size_set:
            one_set.append(item)
        else:
            sub_set.append(one_set)
            one_set = []
    return sub_set

def get_data(file, ratio_train=0.8, batch_size=128):
    data_dict = {}
    collection_all = []
    file_path = os.getcwd() + file

    # Read the file containing the data
    try:
        df = pd.read_csv(file_path, header=None)

        # Split and store the raw collection of sets for train and test
        for idx, str_set in enumerate(list(df[0])):
            one_set = [int(item) for item in str_set.split(' ')]
            if len(one_set) > 2: #max_set_size:
                splitted_set = split_set(one_set, 2)
                collection_all.extend(splitted_set)
                continue
            collection_all.append(one_set)  # Every sets : Set Size Model
    except:
        try:
            file = torch.load(file_path)
            # collection_all needs to be a list of set, where each set is a list of item, an item is an int
        except:
            try:
                file = np.load(file_path)
                # collection_all needs to be a list of set, where each set is a list of item, an item is an int
            except:
                print("Error when reading the data file")
                exit()

    all_class = np.unique([item for sublist in collection_all for item in sublist])
    max_set_size = max([len(one_set) for one_set in collection_all])
    data_dict['ALL_CLASS'] = all_class
    data_dict['SET_SIZE'] = max_set_size        # All the sets are padded up to the biggest set
    data_dict['N_CLASS'] = len(all_class)
    data_dict['ITEM_SIZE'] = len(all_class)

    random.shuffle(collection_all)              # Split into train / validation / test given the ratio
    n_set = len(collection_all)
    split_train = int(ratio_train*n_set)
    split_val = int(((1+ratio_train)/2)*n_set)
    collection_train = collection_all[:split_train]
    collection_val = collection_all[split_train:split_val]
    collection_test = collection_all[split_val:]
    data_dict['collection_train'] = collection_train
    data_dict['collection_test'] = collection_test

    # Build the subset of the train data and their target for the training
    subset_train, subset_train_target = build_subset_target_from_collection(collection_train)
    subset_val, subset_val_target = build_subset_target_from_collection(collection_val)

    # Pad the subsets to have a fixed input size for the model
    # Transform data into tensors in order to have shuffled minibatch from a DataLoader
    subset_train = pad_collection(subset_train, data_dict['SET_SIZE'])
    subset_train_target = [torch.LongTensor([target]) for target in subset_train_target]
    subset_val = pad_collection(subset_val, data_dict['SET_SIZE'])
    subset_val_target = [torch.LongTensor([target]) for target in subset_val_target]

    dataset_train = TensorDataset(torch.stack(subset_train), torch.stack(subset_train_target))
    data_dict['train_loader'] = DataLoader(dataset_train, batch_size=batch_size, shuffle=True, num_workers=0, pin_memory=True)
    dataset_test = TensorDataset(torch.stack(subset_val), torch.stack(subset_val_target))
    data_dict['val_loader'] = DataLoader(dataset_test, batch_size=batch_size, shuffle=True, num_workers=0, pin_memory=True)

    return data_dict


# '''
# Generate a toy dataset which is sequences or ordered digits,
# Ex : the input is [6, 7, 8] and the target is 9
# '''
#
#
# def get_toy_data(ratio_train=0.8, batch_size=128):
#     np.random.seed(1234)
#
#     data_dict = {}
#     collection_all = []
#
#     # Split and store the raw collection of sets for train and test
#     n_sets = 2000
#     max_set_size = 20
#     min_item = 1
#     max_item = 100
#     for idx in range(n_sets):
#         one_item = np.random.randint(min_item, max_item)
#         one_size = np.random.randint(1, max_set_size)
#         one_set = [one_item]
#         for _ in range(one_size):
#             if one_item == max_item:
#                 break
#             else:
#                 new_item = one_item + 1
#                 one_set.append(new_item)
#                 one_item = new_item
#
#         collection_all.append(one_set)
#
#     all_class = np.unique([item for sublist in collection_all for item in sublist])
#     max_set_size = max([len(one_set) for one_set in collection_all])
#     data_dict['ALL_CLASS'] = all_class
#     data_dict['SET_SIZE'] = max_set_size        # All the sets are padded up to the biggest set
#     data_dict['N_CLASS'] = len(all_class)
#     data_dict['ITEM_SIZE'] = len(all_class)
#
#     random.shuffle(collection_all)              # Split into train / validation / test given the ratio
#     n_set = len(collection_all)
#     split_train = int(ratio_train*n_set)
#     split_val = int(((1+ratio_train)/2)*n_set)
#     collection_train = collection_all[:split_train]
#     collection_val = collection_all[split_train:split_val]
#     collection_test = collection_all[split_val:]
#     data_dict['collection_train'] = collection_train
#     data_dict['collection_test'] = collection_test
#
#     # Build the subset of the train data and their target for the training
#     subset_train, subset_train_target = build_subset_target_from_collection(collection_train)
#     subset_val, subset_val_target = build_subset_target_from_collection(collection_val)
#
#     # Pad the subsets to have a fixed input size for the model
#     # Transform data into tensors in order to have shuffled minibatch from a DataLoader
#     subset_train = pad_collection(subset_train, data_dict['SET_SIZE'])
#     subset_train_target = [torch.LongTensor([target]) for target in subset_train_target]
#     subset_val = pad_collection(subset_val, data_dict['SET_SIZE'])
#     subset_val_target = [torch.LongTensor([target]) for target in subset_val_target]
#
#     dataset_train = TensorDataset(torch.stack(subset_train), torch.stack(subset_train_target))
#     data_dict['train_loader'] = DataLoader(dataset_train, batch_size=batch_size, shuffle=True)
#     dataset_test = TensorDataset(torch.stack(subset_val), torch.stack(subset_val_target))
#     data_dict['val_loader'] = DataLoader(dataset_test, batch_size=batch_size, shuffle=True)
#
#     return data_dict


'''
Generate a toy file .csv in  which the sets are sequences or ordered digits, 
Ex : the input is [6, 7, 8] and the target is 9
'''


def build_toy_pb(collection_len, set_size_max=20):
    collection = []
    min_item = 1
    max_item = 100
    for idx in range(collection_len):
        one_item = np.random.randint(min_item, max_item)
        one_size = np.random.randint(1, set_size_max)
        one_set = [one_item]
        for _ in range(one_size):
            if one_item == max_item:
                break
            else:
                new_item = one_item + 1
                one_set.append(new_item)
                one_item = new_item
        collection.append(one_set)

    file = open(os.getcwd() + '/Files/toy_problem.csv', 'w')
    for one_set in collection:
        for one_item in one_set[:-1]:
            file.write(str(one_item))
            file.write(' ')
        file.write(str(one_set[-1]))
        file.write('\n')
    file.close()
    return


if __name__ == '__main__':
    file_path = '/Files/toy_problem.csv'

    data_dict = get_data(file_path, ratio_train=0.8, batch_size=128)
    # data_dict = get_toy_data(ratio_train=0.01, batch_size=400)
    for k in data_dict.keys():
        print("The data dictionary contains : " + str(k))
