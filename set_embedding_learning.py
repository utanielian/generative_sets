from Models.dpp_builder import build_dpp
from Models.dpp_builder.nonsymmetric_dpp_learning import compute_log_likelihood
from data import  get_data
import torch.optim as optim
from torch.optim.lr_scheduler import ReduceLROnPlateau
import torch
import torch.nn as nn
import metrics
from copy import deepcopy
from dppy.finite_dpps import FiniteDPP
from dppy.utils import example_eval_L_linear
import numpy as np
from Models.modules_set_transformer import PMA, SAB, ISAB
from time import time
import os
import ot
from torch.utils.tensorboard import SummaryWriter
import random
import matplotlib.pyplot as plt
from tqdm import tqdm
from Models_Dual.dpp_wasserstein_tools import *
torch.random.manual_seed(5)
random.seed(5)
np.random.seed(5)

'''
Set Transform discrimitor class definition
'''
class ST_set_embedding(nn.Module):
    def __init__(self, vocab_size, embedding_dim, hidden_dim, cuda, n_head=4):
        super(ST_set_embedding, self).__init__()
        self.hidden_dim = hidden_dim
        self.use_cuda = cuda
        self.vocab_size = vocab_size + 1
        # vocab_size+1 to embed the padding
        self.embed = nn.Embedding(self.vocab_size, embedding_dim, padding_idx=0)
        self.item_prediction_out = nn.Linear(hidden_dim, vocab_size)
        self.model = nn.Sequential(
            SAB(dim_in=embedding_dim, dim_out=hidden_dim, num_heads=n_head),
            SAB(dim_in=hidden_dim, dim_out=hidden_dim, num_heads=n_head),
            SAB(dim_in=hidden_dim, dim_out=hidden_dim, num_heads=n_head),
            # ISAB(dim_in=hidden_dim, dim_out=hidden_dim, num_inds=1, num_heads=n_head),
            # ISAB(dim_in=hidden_dim, dim_out=hidden_dim, num_inds=1, num_heads=n_head),
            # ISAB(dim_in=hidden_dim, dim_out=hidden_dim, num_inds=1, num_heads=n_head),
            PMA(dim=hidden_dim, num_heads=1, num_seeds=1))
    '''
    In : One Set
    Out : One sigmoid value
    '''
    def forward(self, subset):
        emb = self.embed(subset.type(torch.long))
        out = self.model(emb)
        item_pred = self.item_prediction_out(out.reshape(-1, self.hidden_dim))
        return torch.log_softmax(item_pred, dim=1)

    def get_set_embedding(self, subset):
        emb = self.embed(subset.type(torch.long))
        out = self.model(emb)
        return out.squeeze(1)

def from_col_to_padded_tensor(col, set_len):
    padded_tensor = []
    max_len = max([len(s) for s in col])
    length = max(max_len, set_len)
    for s in col:
        padded_s = deepcopy(s)
        for _ in range(len(padded_s), length):
            padded_s.append(0)
        padded_tensor.append(padded_s)
    return torch.tensor(padded_tensor)

def generate_subset_with_target(set,vocab_size):
    pairs = []
    for item_idx in range(len(set)):
        # nn.NLLLos() needs to have as target the index of the class in the form of one-hot
        target_item = set[item_idx]
        target_vect_index = torch.tensor([target_item - 1])

        # the associated subset
        subset = deepcopy(set)
        subset.remove(target_item)

        pairs.append((subset, target_vect_index))
    return pairs

# def learn_embed():
if __name__ == '__main__':
    discriminator = None
    print('#####################################################')
    print('Building dataset')
    print('#####################################################\n')
    batch_size = 200
    # file_path = '/Files/1_100_100_100_apparel-diaper-feeding_regs.csv'
    file_path = '/Files/toy_problem.csv'
    dataset = get_data(file_path, ratio_train=.8, batch_size=batch_size)

    # Model's definition
    g_embed_dim = 32
    g_hidden_dim = 64
    set_len = dataset.set_size
    vocab_size = dataset.n_item
    discriminator = ST_set_embedding(vocab_size, g_embed_dim, g_hidden_dim, cuda=False)

    criterion = nn.NLLLoss()
    optimizer = optim.Adam(params=discriminator.parameters(), lr=1e-3)
    MAX_EPOCH = 300
    train_loss_list = []
    test_loss_list = []
    train_accuracy_list = []
    test_accuracy_list = []
    patience = 10

    print('#####################################################')
    print('Embedding training')
    print('#####################################################\n')
    for epoch in range(MAX_EPOCH):
        print('EPOCH : ', epoch)
        n_train = 0
        n_test = 0
        accuracy_train = 0
        epoch_loss_train = 0
        accuracy_test = 0
        epoch_loss_test = 0

        random.shuffle(dataset.train_col)
        random.shuffle(dataset.test_col)
        random.shuffle(dataset.val_col)
        for x in dataset.train_col: random.shuffle(x)
        for x in dataset.val_col: random.shuffle(x)
        for x in dataset.test_col: random.shuffle(x)

        print("TRAIN")
        for set in tqdm(dataset.train_col):
            pairs = generate_subset_with_target(set, vocab_size)
            for (subset, target) in pairs:
                n_train = n_train + 1
                subset_padded = from_col_to_padded_tensor([subset], dataset.set_size)
                disc_pred = discriminator(subset_padded) # Shape = (batch, vocab_size)
                pred_label = torch.argmax(disc_pred)
                true_label = torch.argmax(target)

                if pred_label == true_label:
                    accuracy_train = accuracy_train + 1

                loss = criterion(disc_pred, target)
                epoch_loss_train = epoch_loss_train + loss.item()
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
        mean_loss_train = epoch_loss_train / n_train
        train_loss_list.append(mean_loss_train)
        accuracy_train = accuracy_train / n_train
        train_accuracy_list.append(accuracy_train)
        print('Accuracy (TRAIN) : ', accuracy_train)
        print('Mean loss (TRAIN) : ', mean_loss_train)

        print("EVALUATION")
        for set in tqdm(dataset.test_col):
            pairs = generate_subset_with_target(set, vocab_size)
            for (subset, target) in pairs:
                n_test = n_test + 1
                subset_padded = from_col_to_padded_tensor([subset], dataset.set_size)
                disc_pred = discriminator(subset_padded) # Shape = (batch, vocab_size)
                pred_label = torch.argmax(disc_pred)
                true_label = torch.argmax(target)

                if pred_label == true_label:
                    accuracy_test = accuracy_test + 1

                loss = criterion(disc_pred, target)
                epoch_loss_test = epoch_loss_test + loss.item()
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
        mean_loss_test = epoch_loss_test / n_test
        test_loss_list.append(mean_loss_test)
        accuracy_test = accuracy_test / n_test
        test_accuracy_list.append(accuracy_test)
        print('Accuracy (TEST) : ', accuracy_test)
        print('Mean loss (TEST) : ', mean_loss_test)
        print()
        if (accuracy_test > .8) or (np.argmin(test_accuracy_list) < len(test_accuracy_list) + patience \
                              and np.argmin(test_loss_list) > len(test_loss_list) + patience):
            print("Best model found at epoch ", epoch)
            break

    torch.save(discriminator, os.getcwd() + '/set_embedding_toy.pt')

# Accuracy is not a good metric because one set can have
# multiple possible true target = [9,10] has target 8 and 11 as valid target

# # Sanity check tests :
fst_set = torch.tensor([[1,2,3]])
snd_set = torch.tensor([[2,3,4]])
thd_set = torch.tensor([[18, 19]])
fth_set = torch.tensor([[15,16]])

fst_embed = discriminator.get_set_embedding(fst_set)
snd_embed = discriminator.get_set_embedding(snd_set)
thd_embed = discriminator.get_set_embedding(thd_set)
fth_embed = discriminator.get_set_embedding(fth_set)
print(torch.norm(fst_embed - snd_embed)) #Should be low
print(torch.norm(fst_embed - thd_embed)) #Should be high
print(torch.norm(fth_embed - snd_embed)) #Should be between