from data import get_data
from Models import recurrent_model
import metrics
import matplotlib.pyplot as plt
import torch
"""
we train a recurrent model on a very small dataset. we check that  the model learns
and overfit the training set by :
    * verifying that the training loss properly decrease up to a local optimum
    * when sampling the model starting from an item in the training set, we get
      a set present in the training set.
      
"""
torch.manual_seed(0)
file_path = './Files/1_100_100_100_apparel_regs.csv'
data_dict = get_data(file_path, ratio_train=0.001, batch_size=3)


model = recurrent_model.RecurrentModelBaseline(data_dict)
print("TRAINING")
model.train(n_epoch=100)

#%%
plt.figure()
plt.loglog(data_dict['train_loss'], label='training loss')
plt.legend()
plt.show()
#%%

print('training set: \n',data_dict['collection_train'],'\n')
print('sampled set:')
print(model.sample_from_set([19],nb_new_item=1))
print(model.sample_from_set([2],nb_new_item=1))
print(model.sample_from_set([66],nb_new_item=1))
print(model.sample_from_set([3],nb_new_item=2))
print(model.sample_from_set([13],nb_new_item=5))