import copy
from random import shuffle, sample
import itertools
import csv
from tqdm import tqdm
import numpy as np
from collections import Counter
import random
import collections
import math
import os
import sys
import argparse
from tempfile import gettempdir
import zipfile
from itertools import combinations, product
from scipy.stats import describe
from sklearn import mixture, preprocessing

def read_data_UK_retail(data_file_path):
    dict_basket = dict()
    dict_from_items_to_ids = dict()
    dict_from_items_to_desc = dict()
    with open(data_file_path, encoding='utf-8') as data_csv_file:
        data_csv_reader = csv.reader(data_csv_file, delimiter = ',')
        i, num_of_ids = 0, 0
        for row in data_csv_reader:
            if (i>0):
                if (row[0] in dict_basket):
                    if (row[1] in dict_from_items_to_ids):
                        dict_basket[row[0]].append(dict_from_items_to_ids[row[1]])
                    else:
                        dict_from_items_to_ids[row[1]] = num_of_ids
                        dict_from_items_to_desc[row[1]] = row[2]
                        dict_basket[row[0]].append(dict_from_items_to_ids[row[1]])
                        num_of_ids += 1
                else:
                    if (row[1] in dict_from_items_to_ids):
                        dict_basket[row[0]] = [dict_from_items_to_ids[row[1]]]
                    else:
                        dict_from_items_to_ids[row[1]] = num_of_ids
                        dict_from_items_to_desc[row[1]] = row[2]
                        dict_basket[row[0]] = [dict_from_items_to_ids[row[1]]]
                        num_of_ids += 1
            i += 1
    baskets = [dict_basket[key] for key in sorted(list(dict_basket.keys()))]
    dict_from_ids_to_descriptions = {v: dict_from_items_to_desc[k] for k, v in dict_from_items_to_ids.items()}
    return baskets, dict_from_ids_to_descriptions

data, dictionnary = read_data_UK_retail("./Files/UK-retail-joined.csv")
print(data)
print(dictionnary)