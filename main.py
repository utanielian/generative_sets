from data import get_data
from Models import recurrent_model
import os
import torch
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping
import metrics

if __name__ == '__main__':
    file_path = '/Files/toy_problem.csv'
    data_dict = get_data(file_path, ratio_train=0.8, batch_size=800, sorted=True)
    hparams = {'n_cell': 100, 'n_layer': 1}
    model = recurrent_model.RecurrentModelBaseline(hparams, data_dict)
    checkpoint_callback = ModelCheckpoint(filepath=os.getcwd() + '/Results/Recurrent/', verbose=True)

    print('BEFORE TRAINING')
    sample = model.sample(data_dict['collection_train'])
    dist_wasserstein, dist_chamfer = metrics.wasserstein_chamfer_distance(data_dict['collection_train'],
                                                                  sample)
    print("The distance (mean, (min,max)) between model and train data")
    print("Wasserstein : ", dist_wasserstein)
    print("Chamfer : ", dist_chamfer)

    print('TRAINING')
    trainer = pl.Trainer(checkpoint_callback=checkpoint_callback, max_nb_epochs=50, distributed_backend='dp')
    trainer.fit(model)

    print('AFTER TRAINING')
    sample = model.sample(data_dict['collection_train'])
    dist_wasserstein, dist_chamfer = metrics.wasserstein_chamfer_distance(data_dict['collection_train'],
                                                                  sample)
    print("The distance (mean, (min,max)) between model and train data")
    print("Wasserstein : ", dist_wasserstein)
    print("Chamfer : ", dist_chamfer)