import torch.nn as nn
import torch
from data import get_data
from Models_Dual.set_transformer_dual import Generator, Discriminator
import numpy as np

# Define the models
file_path = '/Files/1_100_100_100_apparel-diaper-feeding_regs.csv'
data_dict = get_data(file_path, ratio_train=0.005, batch_size=3, sorted=False)
generator = Generator(data_dict)
discriminator = Discriminator(data_dict)

n_epochs = 200
lr = 0.0002
# Optimizers & Loss
optimizer_G = torch.optim.Adam(generator.parameters(), lr=lr)
optimizer_D = torch.optim.Adam(discriminator.parameters(), lr=lr)
criterion = nn.BCELoss()

#Define the noise function
valid_item_list = data_dict['ALL_CLASS'][:-1] # Minus the stop item
def generate_noise(item_list=valid_item_list):
    noise_batch = torch.tensor([])
    for _ in range(data_dict['BATCH_SIZE']):
        noise_set = torch.FloatTensor([0 for _ in range(data_dict['SET_SIZE'])])
        noise_set[0] = float(item_list[np.random.choice(len(item_list))])
        noise_batch = torch.cat((noise_batch, noise_set.unsqueeze(0)))
    noise_batch.requires_grad = True
    return noise_batch

# Training
epoch_loss_list = []
for epoch in range(n_epochs):
    epoch_loss = 0

    for i, (real_collection, _) in enumerate(data_dict['train_loader']):
        # Update of real data
        n_real = discriminator(real_collection)
        d_real = criterion(n_real, torch.tensor([1.]))
        d_loss = d_real.item()

        optimizer_D.zero_grad()
        d_real.backward()
        optimizer_D.step()

        # Update of fake data
        noise = generate_noise()
        gen_collection = generator(noise)
        n_fake = discriminator(gen_collection)
        d_fake = criterion(n_fake, torch.tensor([0.]))
        d_loss = d_loss + d_fake.item()

        optimizer_D.zero_grad()
        d_fake.backward()
        optimizer_D.step()


        epoch_loss = epoch_loss + d_loss
    print('EPOCH_LOSS : ', epoch_loss)
    epoch_loss_list.append(epoch_loss)

