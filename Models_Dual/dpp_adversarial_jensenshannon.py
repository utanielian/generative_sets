from Models.dpp_builder import build_dpp
from Models.dpp_builder.nonsymmetric_dpp_learning import compute_log_likelihood
from data import  get_data
import torch.optim as optim
import torch
import torch.nn as nn
import metrics
from copy import deepcopy
from dppy.finite_dpps import FiniteDPP
import numpy as np
from Models.modules_set_transformer import PMA, SAB
from time import time
import os
from torch.utils.tensorboard import SummaryWriter
import random
import matplotlib.pyplot as plt

'''
Set Transform discrimitor class definition
'''
class SetTransformer_fullset_Discriminator(nn.Module):
    def __init__(self, vocab_size, embedding_dim, hidden_dim, cuda, n_head=4):
        super(SetTransformer_fullset_Discriminator, self).__init__()
        self.hidden_dim = hidden_dim
        self.use_cuda = cuda
        self.vocab_size = vocab_size
        self.embed = nn.Embedding(vocab_size, embedding_dim, padding_idx=0)
        self.fc_out = nn.Linear(hidden_dim, 1)
        self.model = nn.Sequential(
            SAB(dim_in=embedding_dim, dim_out=hidden_dim, num_heads=n_head),
            PMA(dim=hidden_dim, num_heads=1, num_seeds=1))
    '''
    In : One Set
    Out : One sigmoid value
    '''
    def forward(self, x):
        emb = self.embed(x.type(torch.long))
        out = self.model(emb)
        pred = torch.sigmoid(self.fc_out(out))
        return pred[:,0,0]


'''
TOOL FUNCTIONS
'''
def sample(model, collection_len): # shifted to sublinear sampling : DPPy sampling
    collection_sample = []

    # Convert kernel to marginal kernel (K)
    L = get_kernel(model)
    num_items_in_catalog = L.size()[0]
    eye = torch.eye(num_items_in_catalog)  # Id pour le terme de normalisation
    K = eye - (L + eye).inverse()  # K = Id - (L + Id)-1

    for _ in range(collection_len):
        K_copy = K.clone().detach()
        one_set = []
        for j in range(num_items_in_catalog):
            if torch.rand(1) < K_copy[j, j]:
                item = j
                one_set.append(item)
            else:
                K_copy[j, j] -= 1
            K_copy[j + 1:, j] /= K_copy[j, j]
            K_copy[j + 1:, j + 1:] -= torch.ger(K_copy[j + 1:, j], K_copy[j, j + 1:])
        if len(one_set) == 0:
            continue
        collection_sample.append(one_set)
    return collection_sample


'''
Map a collection of sets minibatch of items {1,...,n} to their index in the DPP kernel {0,...,n-1}
'''
def map_item_to_index(minibatch):
    def map(l): return [item - 1 for item in l]
    return [map(set) for set in minibatch]


'''
Transform a collection of set (list of list) into a tensor (number_set, set_length)
'''
def from_list_to_padded_tensor(col, set_len):
    padded_tensor = []
    max_len = max([len(s) for s in col])
    length = max(max_len, set_len)
    for s in col:
        padded_s = deepcopy(s)
        for _ in range(len(padded_s), length):
            padded_s.append(0)
        padded_tensor.append(padded_s)
    return torch.tensor(padded_tensor)


'''
Build the likelihood kernel out of the nonsymmetric DPP object
'''
def get_kernel(model, nonsymmetric=False):
    V = model.get_v_embeddings()
    if nonsymmetric:
      B = model.get_b_embeddings()
      C = model.get_c_embeddings()
      L = V.mm(V.transpose(0, 1)) + B.mm(C.transpose(0, 1)) - C.mm(B.transpose(0, 1))
    else:
        L = V.mm(V.transpose(0, 1))
    return L


'''
Function needed for the VFX sublinear sampling using DPPy
'''
def eval_kernel(X, Y=None):
    if X is None:
        raise ValueError('Error of eval kernel function')
    if Y is None:
        return eval_kernel(X, Y=X)
    return L_gen[X.flatten(), :][:, Y.flatten()]


'''
Compute the loglikelihood of the collection of sets minibatch from the DPP generator.
'''
def get_loglikeli_from_gen(gen, minibatch, map_to_index=True):
    if map_to_index:
        indexed_minibatch = []
        for s in minibatch:
            indexed_s = []
            for i in s:
                indexed_s.append(i-1)
            indexed_minibatch.append(indexed_s)
        loglikeli = compute_log_likelihood(gen, indexed_minibatch, reduce=False)
    else:
        loglikeli = compute_log_likelihood(gen, minibatch, reduce=False)
    return loglikeli


'''
Compute the output of the discriminator on the collection of sets minibatch
'''
def get_realness(dis, minibatch, set_len, map_to_index=False):
    if map_to_index:
        indexed_minibatch = []
        for s in minibatch:
            indexed_s = []
            for i in s:
                indexed_s.append(i-1)
            indexed_minibatch.append(indexed_s)

        pad_minibatch = from_list_to_padded_tensor(indexed_minibatch, set_len)
    else:
        pad_minibatch = from_list_to_padded_tensor(minibatch, set_len)
    return dis(pad_minibatch)


'''
Compute ta dummy realness on the collection of sets minibatch just for sanity check
'''
def get_dummy_realness(minibatch):
    realness_list = []
    for s in minibatch:
        realness = torch.rand(1).item()
        nb_item = 0.
        for i in s:
            if i < 5:
                nb_item = nb_item + 1.
        realness_list.append(max(realness, nb_item))
    return torch.tensor(realness_list)


def save_bar_chart(collection, epoch):
    item_popularity = {}
    for set in collection:
        for item in set:
            if item in item_popularity.keys():
                item_popularity[item] = item_popularity[item] + 1
            else :
                item_popularity[item] = 1
    keys = list(item_popularity.keys())
    keys.sort()
    values = [item_popularity[k] for k in keys]
    plt.figure(figsize=(10,8))
    plt.title("Item popularity in the fake sets")
    plt.bar(keys, values, alpha=1, label='Item popularity')
    plt.savefig(os.getcwd()+'/item_popularity_fake_set_epoch' + str(epoch) + '.png')

'''
TRAINING FUNCTIONS
'''
def train_generator(gen, dis, optimizer, fake_col, n_minibatch, minibatch_len, set_len, map_to_index):
    avg_mean_loss = 0.
    avg_realness = 0.

    for idx in range(n_minibatch):
        fake_minibatch = deepcopy(fake_col[int(idx*minibatch_len) : int((idx+1)*minibatch_len)])
        random.shuffle(fake_minibatch)
        loglikeli = get_loglikeli_from_gen(gen, fake_minibatch, map_to_index=map_to_index)
        realness = get_realness(dis, fake_minibatch, set_len, map_to_index=map_to_index)
        loss = loglikeli * realness
        mean_loss = torch.mean(loss)
        optimizer.zero_grad()
        avg_mean_loss = avg_mean_loss + mean_loss.item()
        avg_realness = avg_realness + torch.mean(realness).item()
        (-mean_loss).backward(torch.ones(mean_loss.shape))
        optimizer.step()

    avg_realness = avg_realness / (n_minibatch)
    avg_mean_loss = avg_mean_loss / n_minibatch

    return avg_mean_loss, avg_realness


def train_discriminator(dis, optimizer, true_col, fake_col, minibatch_len=64, set_len=24):
    avg_loss = 0.
    true_acc = 0.
    fake_acc = 0.

    criterion = nn.BCELoss()
    n_minibatch = int(len(true_col) / minibatch_len)

    for idx in range(n_minibatch):
        optimizer.zero_grad()
        # Real data
        true_minibatch = deepcopy(true_col[int(idx*minibatch_len) : int((idx+1)*minibatch_len)])
        random.shuffle(true_minibatch)
        true_idx_minibatch = map_item_to_index(true_minibatch) # need to shift to the index to compute the BCELoss
        true_pad_idx_minibatch = from_list_to_padded_tensor(true_idx_minibatch, set_len)
        true_pred = dis(true_pad_idx_minibatch)
        true_acc = true_acc + len(true_pred[true_pred > 0.5])
        true_target = torch.ones(true_pred.shape)
        true_loss = criterion(true_pred, true_target)
        avg_loss = avg_loss + true_loss.item()
        true_loss.backward()

        # Fake data sampling
        fake_minibatch = deepcopy(fake_col[int(idx*minibatch_len) : int(minibatch_len*(idx+1))])
        random.shuffle(fake_minibatch)
        fake_idx_minibatch = fake_minibatch
        fake_pad_idx_minibatch = from_list_to_padded_tensor(fake_idx_minibatch, set_len)
        fake_pred = dis(fake_pad_idx_minibatch)
        fake_acc = fake_acc + len(fake_pred[fake_pred < 0.5])
        fake_target = torch.zeros(fake_pred.shape)
        fake_loss = criterion(fake_pred, fake_target)
        avg_loss = avg_loss + fake_loss.item()
        fake_loss.backward()
        optimizer.step()

    avg_loss = avg_loss / (len(true_col) * 2)
    fake_acc = fake_acc / (len(true_col))
    true_acc = true_acc / (len(true_col))
    return avg_loss, (true_acc, fake_acc)


'''
EVALUATION FUNCTION
'''
def eval_generator(gen, dis, fake_col, n_minibatch, minibatch_len, set_len):
    avg_loss = 0.

    for idx in range(n_minibatch):
        fake_minibatch = fake_col[int(idx*minibatch_len) : int((idx+1)*minibatch_len)]
        loglikeli = get_loglikeli_from_gen(gen, fake_minibatch, map_to_index=False)
        realness = get_realness(dis, fake_minibatch, set_len)
        loss = loglikeli * realness
        mean_loss = torch.mean(loss)
        avg_loss = avg_loss + mean_loss.item()

    return avg_loss / n_minibatch


def eval_discriminator(dis, true_col, fake_col, minibatch_len=64, set_len=24):
    avg_loss = 0.
    true_acc = 0.
    fake_acc = 0.

    criterion = nn.BCELoss()
    n_minibatch = int(len(true_col) / minibatch_len)

    for idx in range(n_minibatch):
        # Real data
        true_minibatch = true_col[int(idx*minibatch_len) : int((idx+1)*minibatch_len)]
        true_idx_minibatch = map_item_to_index(true_minibatch) # need to shift to the index to compute the BCELoss
        true_pad_idx_minibatch = from_list_to_padded_tensor(true_idx_minibatch, set_len)
        true_pred = dis(true_pad_idx_minibatch)
        true_acc = true_acc + len(true_pred[true_pred > 0.5])
        true_target = torch.ones(true_pred.shape)
        true_loss = criterion(true_pred, true_target)
        avg_loss = avg_loss + true_loss.item()

        # Fake data sampling
        fake_minibatch = deepcopy(fake_col[int(idx*minibatch_len) : int(minibatch_len*(idx+1))])
        fake_idx_minibatch = fake_minibatch
        fake_pad_idx_minibatch = from_list_to_padded_tensor(fake_idx_minibatch, set_len)
        fake_pred = dis(fake_pad_idx_minibatch)
        fake_acc = fake_acc + len(fake_pred[fake_pred < 0.5])
        fake_target = torch.zeros(fake_pred.shape)
        fake_loss = criterion(fake_pred, fake_target)
        avg_loss = avg_loss + fake_loss.item()

    avg_loss = avg_loss / (len(true_col) * 2)
    fake_acc = fake_acc / (len(true_col))
    true_acc = true_acc / (len(true_col))

    return avg_loss, (true_acc, fake_acc)


'''
Compute a distance between a real collection of sets real_collection and a fake collection of sets (sampled inside)
'''
def eval_sampling(real_collection):
    dpp_sampler = FiniteDPP('likelihood',
                            **{'L_eval_X_data': (eval_kernel, X_data)})

    for _ in range(len(real_collection)):
        dpp_sampler.sample_exact(mode='vfx', verbose=False)

    fake_collection = dpp_sampler.list_of_samples
    dist_wasserstein, dist_chamfer = metrics.wasserstein_chamfer_distance(real_collection, \
                                                                          fake_collection, \
                                                                          set_distance='jaccard', \
                                                                          dataset=dataset)
    return dist_wasserstein, dist_chamfer


if __name__ == '__main__':
    generator = None
    discriminator = None
    G_STEP = 1
    D_STEP = 1

    # Get Data
    print('#####################################################')
    print('Building dataset')
    print('#####################################################\n')
    batch_size = 200
    file_path = '/Files/1_100_100_100_apparel-diaper-feeding_regs.csv'
    # file_path = '/Files/toy_problem.csv'
    dataset = get_data(file_path, ratio_train=.8, batch_size=batch_size)
    # dataset = get_data_all_subset(file_path, ratio_train=.8, batch_size=128, max_set_size=10)
    writer = SummaryWriter(log_dir=os.getcwd()+'/tensorboard_log/')
    writer_gen_idx = 1
    writer_disc_idx = 1

    # Model's definition
    # Empty discriminator
    g_embed_dim = 32
    g_hidden_dim = 64
    g_seq_len = dataset.set_size
    vocab_size = dataset.n_item - 1     # Minus the stopping item
    discriminator = SetTransformer_fullset_Discriminator(vocab_size, g_embed_dim, g_hidden_dim, cuda=False)

    # Already pretrained DPP generator
    generator = build_dpp.build(file_path, dataset)             # Non symmetric DPP object
    generator.train()

    # Arguments for the DPPy vfx sampling
    X_data = np.arange(vocab_size)
    X_data = np.reshape(X_data, (vocab_size, 1))
    global L_gen
    L_gen = get_kernel(generator).detach().numpy()

    # Optimizer setup
    optimizer_gen = optim.Adam(params=generator.parameters(), lr=1e-3, weight_decay=1e-3)
    optimizer_dis = optim.Adam(params=discriminator.parameters(), lr=1e-3)

    # path = os.getcwd() + '/DPP_pretraining_discriminator.txt'
    # log_file = open(path, 'w')
    # print('#####################################################')
    # print('Start Discriminator Pre-Training')
    # print('#####################################################\n')
    # MAX_EPOCH = 50
    # min_loss = [np.inf, -1]
    # patience = 2
    # for epoch in range(MAX_EPOCH):
    #     print("EPOCH :", epoch)
    #     log_file.write("EPOCH : {}\n".format(epoch))
    #     print("Sampling", end = ' ')
    #     dpp_sampler = FiniteDPP('likelihood',
    #                             **{'L_eval_X_data': (eval_kernel, X_data)})
    #     for _ in range(len(dataset.train_col)+len(dataset.test_col)):
    #         dpp_sampler.sample_exact(mode='vfx', verbose=False)
    #     fake_col_train = dpp_sampler.list_of_samples[:len(dataset.train_col)]
    #     fake_col_test = dpp_sampler.list_of_samples[len(dataset.train_col):]
    #     print('DONE')
    #     print("Training", end = ' ')
    #     train_loss, (true_acc_train, fake_acc_train) = train_discriminator(discriminator, optimizer_dis,
    #                                                          dataset.train_col, fake_col_train)
    #     print("DONE")
    #     print("Evaluating", end = ' ')
    #     test_loss, (true_acc_test, fake_acc_test) = eval_discriminator(discriminator,
    #                                                          dataset.test_col, fake_col_test)
    #     print("DONE")
    #     print("TRAIN LOSS : {:.5f}, TRUE ACC : {:.5f} FAKE ACC : {:.5f}".format(train_loss, true_acc_train, fake_acc_train))
    #     print("TEST LOSS : {:.5f}, TRUE ACC : {:.5f} FAKE ACC : {:.5f}\n".format(test_loss, true_acc_test, fake_acc_test))
    #     log_file.write("TRAIN LOSS : {:.5f}, REAL ACC : {:.5f} FAKE ACC : {:.5f}\n".format(train_loss, true_acc_train, fake_acc_train))
    #     log_file.write("TEST LOSS : {:.5f}, REAL ACC : {:.5f} FAKE ACC : {:.5f}\n\n".format(test_loss, true_acc_test, fake_acc_test))
    #
    #     # Stopping criterion
    #     # if epoch > 3 and np.mean([true_acc_test, fake_acc_test]) > .75:
    #     #     print("Stopping condition reached : Mean accuracy on test data above 75%")
    #     #     break
    #
    #     # Early Stopping criterion
    #     val_loss, (true_acc_val, fake_acc_val) = eval_discriminator(discriminator,
    #                                                          dataset.val_col, fake_col_test)
    #     if val_loss < min_loss[0]:
    #         min_loss[0] = val_loss
    #         min_loss[1] = epoch
    #     else:
    #         if min_loss[1] + patience <= epoch:
    #             break
    #
    # path = os.getcwd() + 'discriminator_st_dpp.pt'
    # torch.save(discriminator, path)
    path = os.getcwd() + 'discriminator_st_dpp.pt'
    discriminator = torch.load(path)
    optimizer_dis = optim.Adam(params=discriminator.parameters(), lr=1e-3)


    path = os.getcwd() + '/DPP_adversarial_training.txt'
    log_file = open(path, 'w')
    print('#####################################################')
    print('Adversarial Training')
    print('#####################################################\n')
    MAX_EPOCH = 5
    for epoch in range(MAX_EPOCH):
        print("EPOCH :", epoch)
        log_file.write("ADVERSARIAL EPOCH : {}\n".format(epoch))

        print('############ GENERATOR UPDATES')
        for g_step in range(G_STEP):
            print("STEP :", g_step)
            log_file.write("GENERATOR STEP : {}\n".format(g_step))
            print("Sampling ", end = '')
            L_gen = get_kernel(generator).detach().numpy()
            dpp_sampler = FiniteDPP('likelihood',
                                    **{'L_eval_X_data': (eval_kernel, X_data)})
            for _ in range(len(dataset.train_col) + len(dataset.test_col)):
                dpp_sampler.sample_exact(mode='vfx', verbose=False)
            fake_col_train = dpp_sampler.list_of_samples[:len(dataset.train_col)]
            fake_col_test = dpp_sampler.list_of_samples[len(dataset.train_col):]
            print('DONE')
            print("Training ", end = '')
            train_loss, train_realness = train_generator(generator, discriminator, optimizer_gen, fake_col_train,
                                       n_minibatch=len(dataset.train_col)//batch_size, minibatch_len=batch_size,
                                       set_len=dataset.set_size, map_to_index=False)
            print('DONE')
            print("Evaluating ", end = '')
            test_loss = eval_generator(gen=generator, dis=discriminator, fake_col=fake_col_test,
                                       n_minibatch=len(dataset.test_col)//batch_size, minibatch_len=batch_size,
                                       set_len=dataset.set_size)
            print('DONE')
            print("TRAIN LOSS : ", train_loss)
            print("TEST LOSS : ", test_loss)
            loglikeli_fake_train = get_loglikeli_from_gen(generator, fake_col_train)
            print("LOGLIKELIHOOD ON FAKE TRAIN DATA : {:.5f}".format(torch.mean(loglikeli_fake_train).item()))
            loglikeli_fake_test = get_loglikeli_from_gen(generator, fake_col_test, map_to_index=False)
            print("LOGLIKELIHOOD ON FAKE TEST DATA : {:.5f}".format(torch.mean(loglikeli_fake_test).item()))
            loglikeli_true_train = get_loglikeli_from_gen(generator, dataset.train_col)
            print("LOGLIKELIHOOD ON TRUE TRAIN DATA : {:.5f}".format(torch.mean(loglikeli_true_train).item()))
            loglikeli_true_test = get_loglikeli_from_gen(generator, dataset.test_col)
            print("LOGLIKELIHOOD ON TRUE TEST DATA : {:.5f}".format(torch.mean(loglikeli_true_test).item()))

            disc_test_loss, (disc_test_real_acc, disc_test_fake_acc) = eval_discriminator(discriminator,
                                                                           dataset.test_col, fake_col_test)
            print("DISC TEST LOSS : {:.5f}, REAL ACC : {:.5f} FAKE ACC : {:.5f}\n".format(disc_test_loss,
                                                                                          disc_test_real_acc, disc_test_fake_acc))

            dist_wasserstein, dist_chamfer = eval_sampling(dataset.test_col)
            writer.add_scalar('Generator/Loss/train', train_loss,writer_gen_idx)
            writer.add_scalar('Generator/Loss/test', test_loss,writer_gen_idx)
            writer.add_scalar('Generator/Loglikelihood/Fake_Train', torch.mean(loglikeli_fake_train).item(),writer_gen_idx)
            writer.add_scalar('Generator/Loglikelihood/Fake_Test', torch.mean(loglikeli_fake_test).item(),writer_gen_idx)
            writer.add_scalar('Generator/Loglikelihood/True_Train', torch.mean(loglikeli_true_train).item(),writer_gen_idx)
            writer.add_scalar('Generator/Loglikelihood/True_Test', torch.mean(loglikeli_true_test).item(),writer_gen_idx)
            writer.add_scalar('Generator/Dis_acc/Fake_Accuracy', disc_test_fake_acc,writer_gen_idx)
            writer.add_scalar('Generator/Evaluation/Wasserstein', dist_wasserstein[0],writer_gen_idx)
            writer.add_scalar('Generator/Evaluation/Chamfer', dist_chamfer[0],writer_gen_idx)
            writer_gen_idx = writer_gen_idx + 1

        path = os.getcwd() + 'gen_adv_epoch' + str(epoch) + '.pt'
        torch.save(generator, path)
        print('Saved at : ' + path)
        save_bar_chart(fake_col_train, epoch)

        print('############ DISCRIMINATOR UPDATES')
        # global L_gen
        L_gen = get_kernel(generator).detach().numpy()
        for d_step in range(D_STEP):
            print("STEP :", d_step)
            log_file.write("DISCRIMINATOR STEP : {}\n".format(g_step))
            print("Sampling ", end = '')
            dpp_sampler = FiniteDPP('likelihood',
                                    **{'L_eval_X_data': (eval_kernel, X_data)})
            for _ in range(len(dataset.train_col) + len(dataset.test_col)):
                dpp_sampler.sample_exact(mode='vfx', verbose=False)
            fake_col_train = dpp_sampler.list_of_samples[:len(dataset.train_col)]
            fake_col_test = dpp_sampler.list_of_samples[len(dataset.train_col):]
            print('DONE')
            print("Training ", end = '')
            train_loss, (train_true_acc, train_fake_acc) = train_discriminator(discriminator, optimizer_dis,
                                                                               dataset.train_col, fake_col_train)
            print("DONE")
            print("Evaluating ", end = '')
            test_loss, (test_true_acc, test_fake_acc) = eval_discriminator(discriminator,
                                                                           dataset.test_col, fake_col_test)
            # print("DONE")
            print("TRAIN LOSS : {:.5f}, REAL ACC : {:.5f} FAKE ACC : {:.5f}".format(train_loss, train_true_acc,
                                                                                    train_fake_acc))
            print("TEST LOSS : {:.5f}, REAL ACC : {:.5f} FAKE ACC : {:.5f}\n".format(test_loss, test_true_acc,
                                                                                     test_fake_acc))
            log_file.write("TRAIN LOSS : {:.5f}, REAL ACC : {:.5f} FAKE ACC : {:.5f}\n".format(train_loss, train_true_acc, train_fake_acc))
            log_file.write("TEST LOSS : {:.5f}, REAL ACC : {:.5f} FAKE ACC : {:.5f}\n".format(test_loss, test_true_acc, test_fake_acc))

            writer.add_scalar('Discriminator/Loss/Train', train_loss,writer_disc_idx)
            writer.add_scalar('Discriminator/Loss/Test', test_loss,writer_disc_idx)
            writer.add_scalar('Discriminator/Accuracy/True_Train', train_true_acc,writer_disc_idx)
            writer.add_scalar('Discriminator/Accuracy/True_Test', train_fake_acc,writer_disc_idx)
            writer.add_scalar('Discriminator/Accuracy/Fake_Train', test_true_acc,writer_disc_idx)
            writer.add_scalar('Discriminator/Accuracy/Fake_Test', test_fake_acc,writer_disc_idx)
            writer_disc_idx = writer_disc_idx + 1

        path = os.getcwd() + 'disc_adv_epoch' + str(epoch) + '.pt'
        torch.save(discriminator, path)

    log_file.close()
    writer.close()
    print(fake_col_test)


    item_popularity = {}
    for set in dataset.train_col:
        for item in set:
            if item in item_popularity.keys():
                item_popularity[item] = item_popularity[item] + 1
            else :
                item_popularity[item] = 1
    keys = list(item_popularity.keys())
    keys.sort()
    values = [item_popularity[k] for k in keys]
    plt.figure(figsize=(10,8))
    plt.title("Item popularity in the true sets")
    plt.bar(keys, values, alpha=1, label='Item popularity')
    plt.savefig(os.getcwd()+'/item_popularity_true_set.png')
