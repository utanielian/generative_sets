import numpy as np
import matplotlib.pyplot as plt


# # Stopping condition reached : Mean accuracy on test data above 75%
# # LOGLIKELIHOOD ON TRAIN DATA : -19.99362
# # LOGLIKELIHOOD ON TEST DATA : -20.07730
# disc_pre_train = [0.00753,0.00636,0.00609,0.00586,0.00520]
# disc_pre_test = [0.00648,0.00633,0.00585,0.00570,0.00469]
# gen_true_acc_train = [0.74578,0.80083,0.81465,0.82338,0.85052]
# gen_fake_acc_train = [0.77569,0.82514,0.83150,0.83487,0.85325]
# # plt.figure(figsize=(10,6))
# fig, ax = plt.subplots(1, 2)
# fig.suptitle('Discriminator pre-training')
# # plt.title("Discriminator pre-training")
# # plt.plot(np.arange(disc_pre_train), disc_pre_train, label='Train loss')
# ax[1].plot(np.arange(len(disc_pre_train)), disc_pre_train, label='Train')
# # plt.plot(np.arange(disc_pre_test), disc_pre_test, label='Test loss')
# ax[1].plot(np.arange(len(disc_pre_test)), disc_pre_test, label='Test')
# ax[0].set_title("Accuracy")
# ax[1].set_title("Loss")
# ax[0].plot(np.arange(len(gen_true_acc_train)), gen_true_acc_train, label='True ACC')
# # plt.plot(np.arange(gen_true_acc_train), gen_true_acc_train, label='True ACC')
# ax[0].plot(np.arange(len(gen_fake_acc_train)), gen_fake_acc_train, label='Fake ACC')
# # fig.show()
# # plt.plot(np.arange(gen_fake_acc_train), gen_fake_acc_train, label='Fake ACC')
# # ax[0].ylabel("Loglikelihood")
# # ax[0].xlabel("Pre-training epochs")
# # ax[1].ylabel("Loglikelihood")
# # ax[1].xlabel("Pre-training epochs")
# # plt.legend()
# plt.legend()
# plt.show()


gen_fake_train = [-18.81703,-18.06577,-17.52952,-17.16987,-16.52751,-15.83418,-14.78671,-14.87639,-14.48024,-14.65883]
gen_fake_test = [-18.57004,-17.59168,-16.63025,-16.34664,-15.30620,-14.42538,-13.49532,-13.21778,-12.62167,-12.75295]
gen_true_train = [-18.95641,-18.70709,-19.14669,-20.43593,-21.06619,-21.45152, -22.68226,-24.16830,-25.32713,-26.92824]
gen_true_test = [-19.06765,-18.83652,-19.29706,-20.61630,-21.24049,-21.66688,-22.94832,-24.45556,-25.62485, -27.23666]
plt.figure(figsize=(10,6))
plt.title("Loglikelihood of the generator ")
plt.plot(np.arange(len(gen_fake_train)), gen_fake_train, label='Fake Train sets')
plt.plot(np.arange(len(gen_fake_test)), gen_fake_test, label='Fake Test sets')
plt.plot(np.arange(len(gen_true_train)), gen_true_train, label='True Train sets')
plt.plot(np.arange(len(gen_true_test)), gen_true_test, label='True Test sets')
plt.ylabel("Loglikelihood")
plt.xlabel("Adversarial epochs")
plt.legend()
plt.show()


# Apres on fait loss du generator
train_loss = [3.227320284998081,3.383626612802057,4.629009696218824,4.556749555760622,2.4600229613324704,2.701575280644602,\
              2.4114533008462304,2.645940717159813,2.699882540709955,3.108276412733066]
test_loss = [3.1105214439839592,3.2950287624269645,4.554364561353207,4.611211214533271,2.515066545013983,2.767896320244605,\
             2.344804293074311,2.596269750566791,2.6759833727090427,3.1050331629943835]
plt.figure(figsize=(10,6))
plt.title("Loss of the generator (eq. 23)")
plt.plot(np.arange(len(train_loss)), train_loss, label='Train loss')
plt.plot(np.arange(len(test_loss)), test_loss, label='Test loss')
plt.ylabel("Loss")
plt.xlabel("Adversarial epochs")
plt.legend()
plt.show()

# loss du disc
train_loss = [0.00593,0.00607,0.00634,0.00550,0.00460,0.00463,0.00489,0.00510,0.00525,0.00525]
test_loss = [0.00518,0.00567,0.00595,0.00476,0.00426,0.00447,0.00454,0.00491,0.00510,0.00507]
plt.figure(figsize=(10,6))
plt.title("Loss of the discriminator")
plt.plot(np.arange(len(train_loss)), train_loss, label='Train loss')
plt.plot(np.arange(len(test_loss)), test_loss, label='Test loss')
plt.ylabel("Loss")
plt.xlabel("Adversarial epochs")
plt.legend()
plt.show()





import numpy as np
import matplotlib.pyplot as plt

# Create some mock data

# loglike_true_train_list.append(torch.mean(loglikeli_true_test).item())
# loglike_true_test_list.append(torch.mean(loglikeli_true_test).item())
# d_phi_val_list.append(d_phi_val)
from numpy.random import RandomState
from dppy.finite_dpps import FiniteDPP
from dppy.utils import example_eval_L_linear

rng = RandomState(1)

r, N = 4, 10
m = np.eye(300)
DPP = FiniteDPP('likelihood',
        **{'L_eval_X_data': (example_eval_L_linear, L_gen)})

for _ in range(10):
    DPP.sample_exact(mode='vfx', random_state=rng, verbose=False)

print(DPP.list_of_samples)



### VISUALISATION CODE
was_jaccard = np.load(path_metric + 'jaccard_dist.npy')
was_perfect = np.load(path_metric + 'perfect_dist.npy')
was_sparse = np.load(path_metric + 'sparse_vector_dist.npy')
dphi = np.load(path_metric + 'd_phi.npy')
was_jaccard = was_jaccard / np.max(was_jaccard)
was_perfect = was_perfect / np.max(was_perfect)
was_sparse = was_sparse / np.max(was_sparse)
t = np.arange(1, len(was_perfect)+1)
fig, ax1 = plt.subplots()
plt.title("Evolution of d_phi and sampling evaluation through adversarial learning")

color = 'tab:red'
ax1.set_xlabel('Epochs')
ax1.set_ylabel('Sampling evaluation', color=color)
ax1.plot(t, was_jaccard, color=color, label='WD using Jaccard distance')
ax1.plot(t, was_perfect, color='orange', label='WD using Perfect item distance')
ax1.plot(t, was_sparse, color='green', label='WD using Sparse vector distance')
ax1.tick_params(axis='y', labelcolor=color)

plt.legend()
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'tab:blue'
ax2.set_ylabel('d_phi distance', color=color)  # we already handled the x-label with ax1
ax2.plot(t, dphi, color=color)
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.show()

generator = torch.load(path_gen)
fake_samples = sample_sublinear(100, generator)
print(fake_samples)
items = {}
for set in fake_samples:
    for item in set:
        if item not in items.keys():
            items[item] = 1
        else:
            items[item] = items[item] + 1
print(items.values())