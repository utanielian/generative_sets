import torch
import random
torch.random.manual_seed(5)
random.seed(5)


'''
Save true sets in external file
'''
def generate_samples(iterator, output_file):
    samples = []
    for x in iterator:
        samples.extend(x.cpu().data.numpy().tolist())
    with open(output_file, 'w') as fout:
        for sample in samples:
            string = ' '.join([str(s) for s in sample])
            fout.write('{}\n'.format(string))


'''
Save sampled from the generator sets in external file
'''
def generate_fake_samples(model, batch_size, generated_num, output_file):
    samples = []
    for _ in range(int(generated_num / batch_size)):
        sample = model.sample(batch_size).type(torch.long).cpu().data.numpy().tolist()
        samples.extend(sample)
    with open(output_file, 'w') as fout:
        for sample in samples:
            string = ' '.join([str(s) for s in sample])
            fout.write('{}\n'.format(string))


"""
    Transform a batch of item into a batch of one-hot probabilities
Input : A batch of items (batch_size, 1), the number of possible items (int)
Output : A batch of one-hot probabilities (batch_size, vocab_size)
"""
def to_one_hot(batch_item,vocab_size):
    one_hot_list = []
    for item in batch_item:
        one_hot_target = [0 for _ in range(vocab_size)]
        one_hot_target[item.item()-1] = 1        # -1 beauce items are mapped to indexes that way
        one_hot_list.append(one_hot_target)
    return torch.tensor(one_hot_list)


"""
    Train generator with MLE, given a context predicting an item
"""
def train_generator(gen, data_iter, criterion, optimizer, cuda=False):
    total_loss = 0.
    for data, target in data_iter:
        if cuda:
            data, target = data.cuda(), target.cuda()
        target = target.contiguous().view(-1)
        output = gen(data)
        target_node = target - 1
        loss = criterion(output, target_node)
        # Mean loss over the current batch
        total_loss += torch.mean(loss).item()
        optimizer.zero_grad()
        # Backward on the current batch
        loss.backward(torch.ones(loss.shape))
        optimizer.step()
    avg_loss = total_loss / len(data_iter)
    return avg_loss


"""
    Evaluate generator
"""
def eval_generator(model, data_iter, criterion, cuda=False):
    total_loss = 0.
    with torch.no_grad():
        for data, target in data_iter:
            if cuda:
                data, target = data.cuda(), target.cuda()
            target = target.contiguous().view(-1)
            pred = model(data)
            target_node = target - 1
            loss = criterion(pred, target_node)
            total_loss += torch.mean(loss).item()
    avg_loss = total_loss / len(data_iter)
    return avg_loss


"""
    Train discriminator on item conditioned by its context
"""
def train_conditional_discriminator_one_item(dis, gen, real_iter, criterion, optimizer, dataset):
    total_loss = 0.
    total_true_acc = 0.
    total_fake_acc = 0.
    for idx, (x_real, x_real_target) in enumerate(real_iter):
        # Build each class' target
        y_real = torch.tensor([[1] for _ in range(x_real.size(0))]).type(torch.float)
        y_fake = torch.tensor([[0] for _ in range(x_real.size(0))]).type(torch.float)

        # Real data
        optimizer.zero_grad()
        # Transform the batch of item into a batch of probabilities
        one_hot_x_target = to_one_hot(x_real_target,dataset.n_item)
        output = dis(x_real, one_hot_x_target)
        errD_real = criterion(output, y_real)
        acc_real = len(output[output > 0.5])        # Compute the accuracy of classification
        total_true_acc = total_true_acc + acc_real
        total_loss += errD_real.item()
        errD_real.backward()

        # Fake data
        # Produce probabilities from the generator given the subset
        x_fake_target = gen(x_real)
        x_fake_target = torch.exp(x_fake_target)
        # We give not a one-hot item vector but a probability vector coming from the generator
        output = dis(x_real, x_fake_target)
        acc_fake = len(output[output < 0.5])        # Compute the accuracy of classification
        total_fake_acc = total_fake_acc + acc_fake
        errD_fake = criterion(output, y_fake)
        total_loss += errD_fake.item()
        errD_fake.backward()
        optimizer.step()

    avg_loss = total_loss / ((idx+1)*2)
    total_fake_acc = total_fake_acc / (dataset.batch_size*len(real_iter))
    total_true_acc = total_true_acc / (dataset.batch_size*len(real_iter))
    return avg_loss, (total_true_acc, total_fake_acc)


"""
    Evaluate discriminator on item conditioned by its context
"""
def eval_conditional_discriminator(dis, gen, real_iter, criterion, dataset):
    total_loss = 0.
    total_true_acc = 0.
    total_fake_acc = 0.
    with torch.no_grad():
        for idx, (x_real, x_real_target) in enumerate(real_iter):
            # Build each class' target
            y_real = torch.tensor([[1] for _ in range(x_real.size(0))]).type(torch.float)
            y_fake = torch.tensor([[0] for _ in range(x_real.size(0))]).type(torch.float)

            # Real data
            # Transform the batch of item into a batch of probabilities
            one_hot_x_target = to_one_hot(x_real_target,dataset.n_item)
            output = dis(x_real, one_hot_x_target)
            errD_real = criterion(output, y_real)
            acc_real = len(output[output > 0.5])
            total_true_acc = total_true_acc + acc_real
            total_loss += errD_real.item()

            # Fake data
            # Produce probabilities from the generator given the subset
            x_fake_target = gen(x_real)
            x_fake_target = torch.exp(x_fake_target)
            output = dis(x_real, x_fake_target)
            acc_fake = len(output[output < 0.5])
            total_fake_acc = total_fake_acc + acc_fake
            errD_fake = criterion(output, y_fake)
            total_loss += errD_fake.item()

    avg_loss = total_loss / ((idx+1)*2)
    total_fake_acc = total_fake_acc / (dataset.batch_size*len(real_iter))
    total_true_acc = total_true_acc / (dataset.batch_size*len(real_iter))
    return avg_loss, (total_true_acc, total_fake_acc)


"""
    Adversarialy train the conditional generator and the conditional discriminator
"""
def adversarial_train_conditional_disc(gen, dis, data_iter, bce_loss, nll_loss,
                                       gen_optimizer, dis_optimizer, g_steps, d_steps, dataset):
    # Train the generator for g_steps
    print("#Train generator")
    for i in range(g_steps):
        gen_loss = train_generator_conditioned(gen, dis, data_iter, nll_loss, gen_optimizer, dataset)
        print("G-Step {}".format(i))
        print("Train loss: {:.5f}".format(gen_loss))

    # Train the discriminator for d_steps
    print("#Train discriminator")
    for i in range(d_steps):
        dis_loss, (true_acc, fake_acc) = train_conditional_discriminator_one_item(dis, gen, data_iter, bce_loss, dis_optimizer, dataset)
        print("D-Step {}".format(i))
        print("Train loss: {:.5f} | Train acc (True/Fake): {:.5f}/{:.5f}".format(dis_loss, true_acc, fake_acc))

"""
    Train generator with MLE weighted by the discriminator
"""
def train_generator_conditioned(gen, dis, data_iter, criterion, optimizer, dataset, cuda=False):
    total_loss = 0.
    total_fake_fooled = 0.
    for x_real, target in data_iter:
        if cuda:
            data, target = x_real.cuda(), target.cuda()
        target = target.contiguous().view(-1)

        # Given a subset, the generator predicts one item
        y_pred = gen(x_real)
        y_pred_proba = torch.exp(y_pred)
        target_node = target - 1
        # Compute the loss on the item prediction
        loss = criterion(y_pred, target_node)
        loss = loss.unsqueeze(-1)

        # Predicts the realness of the generator's predictions
        realness = dis(x_real, y_pred_proba)
        acc_fooled = len(realness[realness > 0.5])          # Compute accuracy of fooling the discriminator

        #Compute and backward the adversarial loss
        loss = loss * (1 - realness)
        total_loss = total_loss + torch.mean(loss).item()       # Mean loss over the batch
        total_fake_fooled = total_fake_fooled + acc_fooled
        optimizer.zero_grad()
        loss.backward(torch.ones(loss.shape))
        optimizer.step()

    total_fake_fooled = total_fake_fooled / (dataset.batch_size*len(data_iter))
    print("Fooled accuracy : ", total_fake_fooled)
    avg_loss = total_loss / len(data_iter)
    return avg_loss


################################ Some old code


# def compute_length(minibatch):
#     length_list = []
#     for set in minibatch:
#         length = len(set) - (set == 0).sum()
#         length_list.append(torch.tensor(length))
#     return torch.tensor(length_list)
# def train_generator_PG(gen, dis, data_iter, nll_loss, optimizer, args):
#     """
#     Train generator with the guidance of policy gradient
#     """
#     for data, target in data_iter:
#         if args.cuda:
#             data, target = data.cuda(), target.cuda()
#         target = target.contiguous().view(-1)
#         pred_batch = gen(data) #[batch, n_item]
#         for pred_id, (true_set, pred_vect) in enumerate(zip(data, pred_batch)):
#             reward = []
#             total_loss = 0
#             for idx, pred in enumerate(pred_vect[:-1]):
#                 data_up = true_set.clone()
#                 new_item = args.dataset.all_item[idx]
#                 for idx in range(len(data_up)):
#                     if data_up[idx].item() == 0: # First padding
#                         data_up[idx] = torch.tensor(new_item)
#                         break
#                 crt_reward = dis(data_up.unsqueeze(0))
#                 reward.append(crt_reward.squeeze(-1))
#
#             reward.append(torch.tensor([1.]))
#             reward = torch.stack(reward).squeeze(-1) # [1, 100]
#             # print(reward.shape)
#             # print(pred_vect.shape)
#             pred_vect_up = torch.mul(pred_vect, reward)
#             # print(pred_vect_up.shape)
#             # print()
#             # print(pred_vect_up)
#             # print(target[pred_id])
#             target_node = target - 1
#             loss = nll_loss(pred_vect_up.unsqueeze(0), target_node[pred_id].unsqueeze(0)) # [1, 100] et [1]
#             total_loss = total_loss + loss
#         print(total_loss)
#         optimizer.zero_grad()
#         total_loss.backward()
#         optimizer.step()
# def discriminator_data_iter(train_loader, gen):
#     fake_real_x = []
#     fake_real_y = []
#     for x_real, y_real in train_loader:
#         batch_size = x_real.size(0)
#         fake_real_x.extend(x_real.cpu().data.numpy().tolist())
#         fake_real_y.extend(torch.tensor([[1] for _ in range(batch_size)]))
#
#         x_fake = gen.sample(batch_size)
#         y_fake = torch.tensor([[0] for _ in range(batch_size)])
#         fake_real_x.extend(x_fake.cpu().data.numpy().tolist())
#         fake_real_y.extend(y_fake.cpu().data.numpy().tolist())
#
#     random.shuffle(fake_real_y)
#     random.shuffle(fake_real_x)
#
#     dataset_disc = TensorDataset(torch.tensor(fake_real_x), torch.tensor(fake_real_y))
#     return DataLoader(dataset_disc, batch_size=batch_size, shuffle=True)


# def train_discriminator_parallel(dis, gen, real_iter, fake_iter, criterion, optimizer, epoch,
#         dis_adversarial_train_loss):
#     """
#     Train discriminator
#     """
#     total_loss = 0.
#     for idx, (x_real, x_fake) in enumerate(zip(real_iter, fake_iter)):
#         optimizer.zero_grad()
#         y_real = torch.tensor([[1] for _ in range(x_real.size(0))])
#         output = dis(x_real)
#         errD_real = criterion(output, y_real.type(torch.float))
#         total_loss += errD_real.item()
#         errD_real.backward()
#         # Train with all fake data
#         # x_fake = gen.sample(x_real.size(0))
#         y_fake = torch.tensor([[0] for _ in range(x_real.size(0))])
#         output = dis(x_fake)
#         errD_fake = criterion(output, y_fake.type(torch.float))
#         total_loss += errD_fake.item()
#         errD_fake.backward()
#         optimizer.step()
#
#         # if idx % 20 == 0:
#         #     avg_loss = total_loss / ((idx+1)*2)
#         #     print('Current epoch {}, current loss : {:.5f}'.format(epoch, avg_loss))
#
#     avg_loss = total_loss / ((idx+1)*2)
#     print("Epoch {}, train loss: {:.5f}".format(epoch, avg_loss))
#     dis_adversarial_train_loss.append(avg_loss)

# def train_discriminator_non_parallel(dis, gen, real_iter, criterion, optimizer, epoch,
#         dis_adversarial_train_loss):
#     """
#     Train discriminator
#     """
#     total_loss = 0.
#     for idx, x_real in enumerate(real_iter):
#         # Train with all-real data
#         optimizer.zero_grad()
#         y_real = torch.tensor([[1] for _ in range(x_real.size(0))])
#         output = dis(x_real)
#         errD_real = criterion(output, y_real.type(torch.float))
#         total_loss += errD_real.item()
#         errD_real.backward()
#         # Train with all fake data
#         x_fake = gen.sample(x_real.size(0))
#         y_fake = torch.tensor([[0] for _ in range(x_real.size(0))])
#         output = dis(x_fake)
#         errD_fake = criterion(output, y_fake.type(torch.float))
#         total_loss += errD_fake.item()
#         errD_fake.backward()
#         optimizer.step()
#
#         # if idx % 20 == 0:
#         #     avg_loss = total_loss / ((idx+1)*2)
#         #     print('Current epoch {}, current loss : {:.5f}'.format(epoch, avg_loss))
#
#     avg_loss = total_loss / ((idx+1)*2)
#     print("Epoch {}, train loss: {:.5f}".format(epoch, avg_loss))
#     dis_adversarial_train_loss.append(avg_loss)


# def remove_last_item(set_batch):
#     set_batch_up = set_batch.clone()
#     for set in set_batch_up:
#         for idx in range(len(set)):
#             if set[idx].item() == 0:
#                 set[idx-1] = 0
#                 break
#     return set_batch_up
# def train_discriminator_one_item(dis, gen, real_iter, criterion, optimizer, epoch,
#         dis_adversarial_train_loss):
#     """
#     Train discriminator
#     """
#     total_loss = 0.
#     for idx, x_real in enumerate(real_iter):
#         # Train with all-real data
#         optimizer.zero_grad()
#         y_real = torch.tensor([[1] for _ in range(x_real.size(0))])
#         output = dis(x_real)
#         errD_real = criterion(output, y_real.type(torch.float))
#         total_loss += errD_real.item()
#         errD_real.backward()
#         # Train with all fake data
#         germ = remove_last_item(x_real)
#         x_fake = gen.sample_item_from_germ(germ)
#         y_fake = torch.tensor([[0] for _ in range(x_real.size(0))])
#         output = dis(x_fake)
#         errD_fake = criterion(output, y_fake.type(torch.float))
#         total_loss += errD_fake.item()
#         errD_fake.backward()
#         optimizer.step()
#
#         # if idx % 20 == 0:
#         #     avg_loss = total_loss / ((idx+1)*2)
#         #     print('Current epoch {}, current loss : {:.5f}'.format(epoch, avg_loss))
#
#     avg_loss = total_loss / ((idx+1)*2)
#     print("Epoch {}, train loss: {:.5f}".format(epoch, avg_loss))
#     dis_adversarial_train_loss.append(avg_loss)
# def eval_discriminator(dis, gen, real_iter, criterion, args):
#     """
#     Evaluate discriminator
#     """
#     total_loss = 0.
#     with torch.no_grad():
#         for idx, (x_real, _) in enumerate(real_iter):
#             y_real = torch.tensor([[1] for _ in range(x_real.size(0))])
#             output = dis(x_real)
#             errD_real = criterion(output, y_real.type(torch.float))
#             total_loss += errD_real.item()
#             # Train with all fake data
#             x_fake = gen.sample(x_real.size(0))
#             y_fake = torch.tensor([[0] for _ in range(x_real.size(0))])
#             output = dis(x_fake)
#             # germ = remove_last_item(x_real)
#             # x_fake = gen.sample_item_from_germ(germ)
#             # y_fake = torch.tensor([[0] for _ in range(x_real.size(0))])
#             # output = dis(x_fake)
#             errD_fake = criterion(output, y_fake.type(torch.float))
#             total_loss += errD_fake.item()
#             # if idx % 20 == 0:
#             #     avg_loss = total_loss / ((idx + 1) * 2)
#             #     print('Current eval, current loss : {:.5f}'.format(avg_loss))
#     avg_loss = total_loss / ((idx+1)*2)
#     return avg_loss

# def adversarial_train(gen, dis, data_iter, bce_loss, nll_loss, gen_optimizer, dis_optimizer,
#         dis_adversarial_train_loss, args):
#     """
#     Adversarially train generator and discriminator
#     """
#     # train generator for g_steps
#     print("#Train generator")
#     for i in range(args.g_steps):
#         print("##G-Step {}".format(i))
#         # train_generator_PG(gen, dis, rollout, pg_loss, gen_optimizer, 3, args) #args.gk_epochs, args)
#         train_generator_PG(gen, dis, data_iter, nll_loss, gen_optimizer, args) #args.gk_epochs, args)
#
#     # train discriminator for d_steps
#     print("#Train discriminator")
#     for i in range(args.d_steps):
#         print("##D-Step {}".format(i))
#         train_discriminator(dis, gen, data_iter, bce_loss, dis_optimizer, i, #args.dk_epochs,
#             dis_adversarial_train_loss)
# def custom_entropy(Y_hat, Y):
#     # flatten all the labels
#     Y = Y.view(-1)
#     # flatten all predictions
#     # create a mask by filtering out all tokens that ARE NOT the padding token
#     pad_item = 0
#     mask = (Y > pad_item).float()
#     # Count how many tokens we have
#     nb_tokens = int(torch.sum(mask).item())
#
#     # Pick the values for the label and zero out the rest with the mask
#     Y_hat = Y_hat[range(Y_hat.shape[0]), Y] * mask
#     # Y_hat Contains the real pred of the truth item of each item vector pred except the padding item
#
#     # compute cross entropy loss which ignores all pad item
#     ce_loss = -torch.sum(Y_hat) / nb_tokens
#     return ce_loss
# '''
#     Input : Batch of sets (batch_size, set_size), the number of possible items (int)
#     Output : Batch of the same sets with the last item removed (batch_size, set_size),
#              Batch of the removed items (batch_size, vocab_size)
#
#     Note : One item is encoded by a one-hot vector with a probability of 1 on the item and zero elsewhere
# '''
# def build_context_from_set(set_batch, vocab_size):
#     set_batch_up = set_batch.clone()
#     target = []
#     for set in set_batch_up:
#         for idx in range(len(set)):
#             if set[idx].item() == 0:  # First padding, then the last item is the previous one
#                 last_item = set[idx - 1]
#                 one_target = [0 for _ in range(vocab_size)]
#                 one_target[int(last_item) - 1] = 1  # -1 beauce items are mapped to indexes that way
#                 target.append(one_target)
#                 set[idx - 1] = 0
#                 break
#     return set_batch_up, torch.tensor(target)