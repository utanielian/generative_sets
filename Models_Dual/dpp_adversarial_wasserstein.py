from Models.dpp_builder import build_dpp
from Models.dpp_builder.nonsymmetric_dpp_learning import compute_log_likelihood
from data import  get_data
import torch.optim as optim
from torch.optim.lr_scheduler import ReduceLROnPlateau
import torch
import torch.nn as nn
import metrics
from copy import deepcopy
from dppy.finite_dpps import FiniteDPP
from dppy.utils import example_eval_L_linear
import numpy as np
from Models.modules_set_transformer import PMA, SAB, ISAB
from time import time
import os
import ot
from torch.utils.tensorboard import SummaryWriter
import random
import matplotlib.pyplot as plt
from Models_Dual.dpp_wasserstein_tools import *
from Models.set_transformer import SetTransformerBaseline
torch.random.manual_seed(5)
random.seed(5)
np.random.seed(5)

'''
Set Transform discrimitor class definition
'''
class SetTransformer_fullset_Discriminator(nn.Module):
    def __init__(self, vocab_size, embedding_dim, hidden_dim, cuda, n_head=4):
        super(SetTransformer_fullset_Discriminator, self).__init__()
        self.hidden_dim = hidden_dim
        self.use_cuda = cuda
        self.vocab_size = vocab_size + 1    # Plus the padding item
        self.embed = nn.Embedding(self.vocab_size, embedding_dim, padding_idx=0)
        self.classification_out = nn.Linear(hidden_dim, 1)
        self.item_prediction_out = nn.Linear(hidden_dim, vocab_size)
        self.model = nn.Sequential(
            ISAB(dim_in=embedding_dim, dim_out=hidden_dim, num_inds=1, num_heads=n_head),
            ISAB(dim_in=hidden_dim, dim_out=hidden_dim, num_inds=1, num_heads=n_head),
            ISAB(dim_in=hidden_dim, dim_out=hidden_dim, num_inds=1, num_heads=n_head),
            ISAB(dim_in=hidden_dim, dim_out=hidden_dim, num_inds=1, num_heads=n_head),
            ISAB(dim_in=hidden_dim, dim_out=hidden_dim, num_inds=1, num_heads=n_head),
            PMA(dim=hidden_dim, num_heads=1, num_seeds=1))
    '''
    In : One Set
    Out : One sigmoid value
    '''
    def forward(self, x):
        emb = self.embed(x.type(torch.long))
        out = self.model(emb)
        classification_score = self.classification_out(out)
        return classification_score[:,0,0]

    def conditional_item_prediction(self, subset):
        emb = self.embed(subset.type(torch.long))
        out = self.model(emb)
        item_pred = self.item_prediction_out(out.reshape(-1, self.hidden_dim))
        return torch.log_softmax(item_pred, dim=1)

'''
Sampling functions
'''

def linear_sample(collection_len, model):
    collection_sample = []

    # Convert kernel to marginal kernel (K)
    L = get_kernel(model)
    num_items_in_catalog = L.size()[0]
    eye = torch.eye(num_items_in_catalog)  # Id pour le terme de normalisation
    K = eye - (L + eye).inverse()  # K = Id - (L + Id)-1

    while(len(collection_sample) < collection_len):
        K_copy = K.clone().detach()
        one_set = []
        for j in range(num_items_in_catalog):
            if torch.rand(1) < K_copy[j, j]:
                item = j
                one_set.append(item)
            else:
                K_copy[j, j] -= 1
            K_copy[j + 1:, j] /= K_copy[j, j]
            K_copy[j + 1:, j + 1:] -= torch.ger(K_copy[j + 1:, j], K_copy[j, j + 1:])
        # if len(one_set) == 0:
        #     continue
        collection_sample.append(one_set)
    return collection_sample


def eval_kernel(X, Y=None):
    if X is None:
        raise ValueError('Error of eval kernel function')
    if Y is None:
        return eval_kernel(X, Y=X)
    return L_gen[X.flatten(), :][:, Y.flatten()]


def sample_sublinear(n_sets, generator):
    try:
        dpp_sampler = FiniteDPP('likelihood',
                                **{'L_eval_X_data': (eval_kernel, X_data)})
        for _ in range(n_sets):
            dpp_sampler.sample_exact(mode='vfx', verbose=False)
        fake_sets_index = dpp_sampler.list_of_samples
        fake_sets = [[i+1 for i in s] for s in fake_sets_index]
    except ValueError:
        fake_sets_index = linear_sample(n_sets, generator)
        fake_sets = [[i+1 for i in s] for s in fake_sets_index]
    return deepcopy(fake_sets)


'''
Some custom regularisation aiming to prevent mode collapse
'''
def gen_regularisation(minibatch):
    sum_of_dist = 0
    for i in range(len(minibatch)):
        for j in range(len(minibatch)):
            if i == j:
                continue
            else:
                one_set_distance = jaccard_distance(minibatch[i], minibatch[j])
                sum_of_dist = sum_of_dist + one_set_distance
    mean_dist = sum_of_dist / (2*len(minibatch))
    return mean_dist

def entropy_marginal(minibatch):
    item_freq = {}
    for item in range(1, 21):
        item_freq[item] = 0
    for set in minibatch:
        for item in set:
            if item not in item_freq.keys():
                item_freq[item] = 1
            else:
                item_freq[item] = item_freq[item] + 1
    entropy = 0.
    for key in item_freq.keys():
        entropy = entropy + item_freq[key]*np.log(item_freq[key]+ 1e-10)
    return entropy

def l2_norm_marginal(minibatch):
    item_freq = {}
    for item in range(1, 21):
        item_freq[item] = 0
    for set in minibatch:
        for item in set:
            if item not in item_freq.keys():
                item_freq[item] = 1
            else:
                item_freq[item] = item_freq[item] + 1
    marginal_list = [item_freq[item] for item in range(1,21)]
    return np.linalg.norm(marginal_list)

def minmax_marginal(minibatch):
    item_freq = {}
    for item in range(1, 21):
        item_freq[item] = 0
    for set in minibatch:
        for item in set:
            if item not in item_freq.keys():
                item_freq[item] = 1
            else:
                item_freq[item] = item_freq[item] + 1
    max_marginal = 0
    min_marginal = 1000000
    for key in item_freq.keys():
        if item_freq[key] < min_marginal:
            min_marginal = item_freq[key]
        if item_freq[key] > max_marginal:
            max_marginal = item_freq[key]
    return min_marginal


'''
Adversarial learning function
'''
def adversarial_training(gen, dis, optim_gen, optim_dis, true_col,
                         d_step, g_step, minibatch_len, lambd,set_len, model_embed=None):

    print("Discriminator updates")
    fake_col_dis = sample_sublinear(minibatch_len * d_step, gen)
    for minibatch_idx in range(d_step):
        optim_dis.zero_grad()
        one_fake_minibatch = fake_col_dis[minibatch_idx*minibatch_len: (minibatch_idx+1)*minibatch_len]
        fake_minibatch_padded = from_list_to_padded_tensor(one_fake_minibatch, set_len)
        fake_pred = dis(fake_minibatch_padded)

        idx = np.random.randint(low=0, high=len(true_col), size=minibatch_len).tolist()
        one_true_minibatch = [true_col[i] for i in idx]

        true_minibatch_padded = from_list_to_padded_tensor(one_true_minibatch, set_len)
        true_pred = dis(true_minibatch_padded)

        loss = torch.mean(true_pred) - torch.mean(fake_pred) - lambd*compute_lipschitz_const(true_pred, one_true_minibatch,
                                                                                             fake_pred, one_fake_minibatch,
                                                                                             model_embed, set_len=set_len)
        (-loss).backward()
        optim_dis.step()

    print("Generator updates")
    for minibatch_idx in range(g_step):
        optim_gen.zero_grad()
        one_fake_minibatch = sample_sublinear(minibatch_len * g_step, gen)

        fake_loglikeli = get_loglikeli_from_gen(gen, one_fake_minibatch)
        fake_realness = get_realness(dis, one_fake_minibatch, set_len)
        fake_realness = (fake_realness - torch.min(fake_realness)) / torch.max(fake_realness)

        # loss = (fake_loglikeli) * (fake_realness) + 1 * gen_regularisation(one_fake_minibatch)
        # loss = (fake_loglikeli) * (fake_realness) + 100 * entropy_marginal(one_fake_minibatch)
        # loss = (fake_loglikeli) * (fake_realness) * entropy_marginal(one_fake_minibatch)
        # loss = (fake_loglikeli) * (fake_realness) + 1000 * l2_norm_marginal(one_fake_minibatch)
        # loss = (fake_loglikeli) * (fake_realness) + 100 * minmax_marginal(one_fake_minibatch)
        loss = (fake_loglikeli) * (fake_realness)

        mean_loss = torch.mean(loss)
        (-mean_loss).backward(torch.ones(mean_loss.shape))
        optim_gen.step()


    return gen, dis

def adversarial_learning():
# if __name__ == '__main__':
    generator = None
    discriminator = None
    G_STEP = 10
    D_STEP = 5
    lambda_disc_lips = 1e-2

    print('#####################################################')
    print('Building dataset')
    print('#####################################################\n')
    batch_size = 200
    # file_path = '/Files/1_100_100_100_apparel-diaper-feeding_regs.csv'
    file_path = '/Files/toy_problem.csv'
    dataset = get_data(file_path, ratio_train=.8, batch_size=batch_size)


    embedding_model = SetTransformerBaseline(dataset)
    embedding_model.load_state_dict(torch.load(os.getcwd() + '/embedding_model_state_dict.pt'))

    # Model's definition
    g_embed_dim = 32
    g_hidden_dim = 64
    set_len = dataset.set_size
    vocab_size = dataset.n_item - 1
    discriminator = SetTransformer_fullset_Discriminator(vocab_size, g_embed_dim, g_hidden_dim, cuda=False)
    generator = build_dpp.build(file_path, dataset)    # Already pretrained DPP generator
    generator.train()

    # Arguments for the DPPy vfx sampling
    global X_data
    X_data = np.arange(vocab_size)
    X_data = np.reshape(X_data, (vocab_size, 1))
    global L_gen
    L_gen = get_kernel(generator).detach().numpy()

    # EVALUATION AFTER MLE PRETRAINING
    # fake_test_col = sample_sublinear(len(dataset.test_col), generator)
    # dist_wasserstein_jaccard, _ = eval_sampling(dataset.test_col, fake_test_col, dataset, dist='jaccard')
    # print(dist_wasserstein_jaccard[0])
    # dist_wasserstein_perfect_item_dist, _ = eval_sampling(dataset.test_col, fake_test_col, dataset, dist='perfect_item')
    # print(dist_wasserstein_perfect_item_dist[0])
    # dist_wasserstein_sparse_vect, _ = eval_sampling(dataset.test_col, fake_test_col, dataset, dist='sparse_vector')
    # print(dist_wasserstein_sparse_vect[0])
    # Optimizer setup
    optimizer_dis = optim.Adam(params=discriminator.parameters(), lr=1e-4) #, weight_decay=1e-4)
    scheduler_dis = ReduceLROnPlateau(optimizer_dis, verbose=True, factor=0.5, patience=4, mode='min')
    train_loss_list = []
    d_phi_list = []
    d_phi_true_list = []
    d_phi_tfake_list = []
    print(sample_sublinear(10, generator))
    # print('#####################################################')
    # print('Start Discriminator Pre-Training')
    # print('#####################################################\n')
    # MAX_EPOCH = 200
    # old_eval_loss = None
    # for epoch in range(MAX_EPOCH):
    #     print("EPOCH :", epoch)
    #     L_gen = get_kernel(generator).detach().numpy()
    #
    #     random.shuffle(dataset.train_col)
    #     random.shuffle(dataset.test_col)
    #     random.shuffle(dataset.val_col)
    #     for x in dataset.train_col: random.shuffle(x)
    #     for x in dataset.val_col: random.shuffle(x)
    #     for x in dataset.test_col: random.shuffle(x)
    #
    #     fake_col_train = sample_sublinear(len(dataset.train_col), generator)
    #     fake_col_eval = sample_sublinear(len(dataset.val_col), generator)
    #     fst_fake_col_test = sample_sublinear(len(dataset.test_col), generator)
    #     snd_fake_col_test = sample_sublinear(len(dataset.test_col), generator)
    #
    #     train_loss = train_discriminator(discriminator, optimizer_dis,
    #                                                          dataset.train_col, fake_col_train,
    #                                                          minibatch_len=dataset.batch_size, lambd=lambda_disc_lips,
    #                                                          set_len=dataset.set_size, model_embed=embedding_model)
    #     eval_loss = eval_discriminator(discriminator,dataset.val_col, fake_col_eval,
    #                                    minibatch_len=dataset.batch_size, lambd=lambda_disc_lips,
#                                        set_len=dataset.set_size, model_embed=embedding_model)
    #     d_phi = compute_dphi(discriminator, dataset.test_col, fst_fake_col_test, set_len=dataset.set_size)
    #     d_phi_true = compute_dphi(discriminator, dataset.test_col, dataset.val_col, set_len=dataset.set_size)
    #     d_phi_fake = compute_dphi(discriminator, fst_fake_col_test, snd_fake_col_test, set_len=dataset.set_size)
    #     train_loss_list.append(train_loss)
    #     d_phi_list.append(d_phi)
    #     d_phi_true_list.append(d_phi_true)
    #     d_phi_tfake_list.append(d_phi_fake)
    #     print('Train loss : ', train_loss)
    #     print('Eval loss : ', eval_loss)
    #     print('D_phi  : ', d_phi)
    #     print('D_phi True : ', d_phi_true)
    #     print('D_phi Fake : ', d_phi_fake)
    #     scheduler_dis.step(-eval_loss)
    #     if epoch > 1 and old_eval_loss > eval_loss and d_phi > 0.8:
    #         break
    #     old_eval_loss = eval_loss

    # path = os.getcwd() + '/discriminator_st_dpp_toy_sparse_vector_dist.pt'
    # path = os.getcwd() + '/discriminator_st_dpp_toy_perfect_dist.pt'
    path = os.getcwd() + '/discriminator_st_dpp_toy_jaccard.pt'
    # path = os.getcwd() + '/discriminator_st_dpp_toy_lambda0.pt'
    # torch.save(discriminator, path)
    # torch.save(generator, path)
    discriminator = torch.load(path)
    # generator = torch.load(path)

    # PRIMAL WD
    # fake_col = sample_sublinear(len(dataset.test_col), generator)
    # d_phi_val = compute_dphi(discriminator, dataset.val_col, fake_col, set_len=dataset.set_size)
    # print("D_Phi : ", d_phi_val)
    # primal_wasserstein, primal_chamfer = metrics.wasserstein_chamfer_distance(dataset.test_col,
    #                                                                   sample_sublinear(len(dataset.test_col), generator),
    #                                                                   set_distance='sparse_vector', total_distance=True)
    #
    # print("Primal Wasserstein distance : ", primal_wasserstein[0])# 0.6
    # print("Chamfer distance : ", primal_chamfer[0])

    optimizer_dis = optim.Adam(params=discriminator.parameters(), lr=1e-5) #, weight_decay=1e-4)
    optimizer_gen = optim.Adam(params=generator.parameters(), lr=1e-3)
    scheduler_gen = ReduceLROnPlateau(optimizer_gen, verbose=True, factor=0.5, patience=3, mode='min')
    scheduler_dis = ReduceLROnPlateau(optimizer_dis, verbose=True, factor=0.5, patience=3, mode='min')

    path = os.getcwd() + '/DPP_adversarial_training.txt'
    log_file = open(path, 'w')
    print('#####################################################')
    print('Adversarial Training')
    print('#####################################################\n')

    # path_gen = os.getcwd() + '/generator_jaccard.pt'; path_metric = os.getcwd() + '/generator_jaccard_'
    # path_gen = os.getcwd() + '/generator_perfect_dist.pt'; path_metric = os.getcwd() + '/generator_perfect_dist_'
    # path_gen = os.getcwd() + '/generator_sparse_vector_dist.pt'; path_metric = os.getcwd() + '/generator_sparse_vector_dist_'
    # path_gen = os.getcwd() + '/generator_lambda_zero.pt'; path_metric = os.getcwd() + '/generator_lambda_zero_'
    # path_gen = os.getcwd() + '/generator_modecollapse_regularised.pt'; path_metric = os.getcwd() + '/generator_modecollapse_regularised_'
    # path_gen = os.getcwd() + '/generator_nopretraining.pt'; path_metric = os.getcwd() + '/generator_nopretraining_'

    MAX_EPOCH = 200
    min_perfect_dist = 1000000
    epoch_min = 0
    patience = 15
    old_loglik = None
    d_phi_val_list = []
    loglike_true_train_list = []
    loglike_true_test_list = []
    dist_wasserstein_jaccard_list = []
    dist_wasserstein_perfect_list = []
    dist_wasserstein_sparse_vector_list = []
    for epoch in range(1, MAX_EPOCH):
        print("EPOCH :", epoch)
        L_gen = get_kernel(generator).detach().numpy()
        log_file.write("ADVERSARIAL EPOCH : {}\n".format(epoch))
        random.shuffle(dataset.train_col)
        random.shuffle(dataset.test_col)
        random.shuffle(dataset.val_col)
        for x in dataset.train_col: random.shuffle(x)
        for x in dataset.val_col: random.shuffle(x)
        for x in dataset.test_col: random.shuffle(x)
        generator, discriminator = adversarial_training(generator, discriminator, optimizer_gen, optimizer_dis, dataset.train_col,
                             d_step=D_STEP, g_step=G_STEP, minibatch_len=dataset.batch_size, lambd=lambda_disc_lips,
                             set_len=dataset.set_size, model_embed=embedding_model)

        fake_test_col = sample_sublinear(len(dataset.val_col), generator)
        fake_col_snd = sample_sublinear(len(dataset.val_col), generator)
        d_phi_val = compute_dphi(discriminator, dataset.val_col, fake_test_col, set_len=dataset.set_size)
        d_phi_true = compute_dphi(discriminator, dataset.test_col, dataset.val_col, set_len=dataset.set_size)
        d_phi_fake = compute_dphi(discriminator, fake_test_col, fake_col_snd, set_len=dataset.set_size)
        print("d_hpi en validation : ", d_phi_val)
        print('d_phi True : ', d_phi_true)
        print('d_phi Fake : ', d_phi_fake)

        save_bar_chart(fake_test_col, epoch)

        dist_wasserstein_jaccard, _ = eval_sampling(dataset.test_col, fake_test_col, dataset, dist='jaccard', total_dist=False)
        dist_wasserstein_perfect_item_dist, _ = eval_sampling(dataset.test_col, fake_test_col, dataset, dist='perfect_item', total_dist=False)
        dist_wasserstein_sparse_vector_dist, _ = eval_sampling(dataset.test_col, fake_test_col, dataset, dist='sparse_vector', total_dist=False)
        # print("Sampling evaluation : ", dist_wasserstein_jaccard[0])
        print("Sampling evaluation perfect : ", dist_wasserstein_perfect_item_dist[0])
        print("Sampling evaluation jaccard : ", dist_wasserstein_jaccard[0])
        print("Sampling evaluation sparse vector : ", dist_wasserstein_sparse_vector_dist[0])
        fake_col = sample_sublinear(10, generator)
        print(fake_col)
        d_phi_val_list.append(d_phi_val)
        dist_wasserstein_jaccard_list.append(dist_wasserstein_jaccard[0])
        dist_wasserstein_perfect_list.append(dist_wasserstein_perfect_item_dist[0])
        dist_wasserstein_sparse_vector_list.append(dist_wasserstein_sparse_vector_dist[0])
        # np.save(path_metric + 'jaccard_dist.npy' , dist_wasserstein_jaccard_list)
        # np.save(path_metric + 'perfect_dist.npy' , dist_wasserstein_perfect_list)
        # np.save(path_metric + 'd_phi.npy' , d_phi_val_list)
        # np.save(path_metric + 'sparse_vector_dist.npy' , dist_wasserstein_sparse_vector_list)
        if dist_wasserstein_perfect_item_dist[0] < min_perfect_dist:
            # torch.save(generator, path_gen)
            min_perfect_dist = dist_wasserstein_perfect_item_dist[0]
            epoch_min = epoch
            print("MIN ", min_perfect_dist)
        print()

        # if dist_wasserstein_perfect_item_dist[0] > min_perfect_dist and epoch > epoch_min+patience:
        #     print("Best model found")
        #     break


# root = embedding_model.get_set_embedding(from_list_to_padded_tensor([[1, 2, 3]], 4))
# t = embedding_model.get_set_embedding(from_list_to_padded_tensor([[1, 2, 19]], 4))
# print(torch.norm(root - t))