import torch.nn as nn
import torch
from data import get_data
from Models_Dual.set_transformer_dual import Generator, Discriminator
import numpy as np
import time
from tqdm import tqdm
from torch.autograd import Variable

# Define the models
file_path = '/Files/1_100_100_100_apparel-diaper-feeding_regs.csv'
data_dict = get_data(file_path, ratio_train=0.005, batch_size=1, sorted=False)
generator = Generator(data_dict)
generator.train()
discriminator = Discriminator(data_dict)
discriminator.train()

lr = 0.0001
# Optimizers & Loss
optimizer_G = torch.optim.Adam(generator.parameters(), lr=lr, betas=(0.5, 0.999))
optimizer_D = torch.optim.Adam(discriminator.parameters(), lr=lr, betas=(0.5, 0.999))
criterion = nn.BCELoss()
epochs = 1000
g_steps_per_epoch = 1
d_steps_per_epochs = 1
#Define the noise function
valid_item_list = data_dict['ALL_CLASS'][:-1] # Minus the stop item
def generate_noise(item_list=valid_item_list):
    # noise_batch = torch.tensor([])
    # for _ in range(data_dict['BATCH_SIZE']):
    #     noise_set = torch.FloatTensor([0 for _ in range(data_dict['SET_SIZE'])])
    #     noise_set[0] = float(item_list[np.random.choice(len(item_list))])
    #     noise_batch = torch.cat((noise_batch, noise_set.unsqueeze(0)))
    # noise_batch.requires_grad = True
    batch_size = int(data_dict['BATCH_SIZE'])
    set_size = int(data_dict['SET_SIZE'])
    min_item = int(data_dict['ALL_CLASS'][0])
    max_item = int(data_dict['ALL_CLASS'][-1])

    germ = torch.FloatTensor(size=[batch_size, set_size]).random_(min_item, max_item)
    germ[:, 1:] = 0
    # germ.requires_grad = True
    return Variable(germ) #noise_batch
generate_noise().shape
print(generator(generate_noise()))


def relu(x) : return max(x, 0)

train_loader = data_dict['train_loader']
dataset_iter = iter(train_loader)
for e in tqdm(range(epochs)):
    #Discriminator training
    for d_step in range(d_steps_per_epochs):
        try:
            (x, y) = dataset_iter.next()
        except StopIteration:
            dataset_iter = iter(train_loader)
            (x, y) = dataset_iter.next()
        fake_x = generator(generate_noise())
        real_dx = discriminator(x[0])
        fake_dx = discriminator(fake_x)

        loss_adv_d = relu(1.0 + fake_dx) + relu(1.0 - real_dx)

        optimizer_D.zero_grad()
        loss_adv_d.backward()
        optimizer_D.step()

    #Generator training
    for g_step in range(g_steps_per_epoch):
        fake_x = generator(generate_noise())
        fake_dx = discriminator(fake_x)

        loss_adv_g = 1 - fake_dx
        optimizer_G.zero_grad()
        loss_adv_g.backward()
        optimizer_G.step()

    print('G:', loss_adv_g.item(), 'D:', loss_adv_d.item())

print(generator(generate_noise()))


for p in generator.parameters():
    print(p.grad)