# import sys
# sys.path.extend(['/Users/l.anquetil/Desktop/gen-sets'])
import torch
import torch.nn as nn
import torch.nn.functional as F
from copy import deepcopy
import random
torch.random.manual_seed(5)
random.seed(5)
from Models.modules_set_transformer import PMA, SAB
import numpy as np
from data import get_data
import torch.multiprocessing as mp
from time import time
import os

class Generator(nn.Module):
    """ Generator """

    def __init__(self, dataset, vocab_size, embedding_dim, hidden_dim, use_cuda):
        super(Generator, self).__init__()
        self.dataset = dataset
        self.hidden_dim = hidden_dim
        self.use_cuda = use_cuda
        self.vocab_size = vocab_size
        self.embed = nn.Embedding(vocab_size, embedding_dim, padding_idx=0)
        self.model = None
        self.fc = nn.Linear(hidden_dim, vocab_size)

    def forward(self, x):
        """
        Embeds input and applies the model on the input set.

        Inputs: x
            - x: (batch_size, set_len), batch of sets of size set_len
        Outputs: out
            - out: (batch_size, vocab_size), model output prediction
        """
        return

    def get_embedding(self, item):
        emb = self.embed(item.type(torch.long))
        return emb

    def pad_minibatch(self, x, set_size):
        zero = torch.zeros(x.size(0), set_size - x.size(1)).type(torch.long)
        return torch.cat((x, zero), dim=1)

    @staticmethod
    def create_set_from_list(arg_set, set_size):
        one_set = []
        class_list = arg_set.copy()
        for c in class_list:
            one_set.append(c)
        for _ in range(set_size - len(one_set)):
            one_set.append(0)

        return torch.Tensor([one_set])

    @staticmethod
    def mask_already_item_pred(vect_pred, sample):
        updated_vect = vect_pred.clone()
        for item in sample:
            item_idx = item-1
            updated_vect[0, item_idx] = 0

        return updated_vect

    def sample(self, batch_size):
        """
        Samples the generator given the first item based on its popularity
        and sample until we found the stopping item

        Outputs: out
            - out: (batch_size, dataset.set_size) : Batch of padded sets
        """
        samples = []
        dataset = self.dataset
        while len(samples) < batch_size:
            new_item = dataset.stop_item
            item_list = []
            while new_item == dataset.stop_item:
                idx_class = torch.multinomial(dataset.popularity.type(torch.float), num_samples=1)
                new_item = dataset.all_item[idx_class]

            item_list.append(new_item)
            crt_set = self.create_set_from_list(item_list, dataset.set_size)

            while new_item != dataset.stop_item and len(item_list) < dataset.set_size:
                out = self(crt_set)
                vect_pred = torch.exp(out)
                updated_proba = self.mask_already_item_pred(vect_pred, item_list)
                idx_class = torch.multinomial(updated_proba, num_samples=1)

                if dataset.all_item[idx_class] == dataset.stop_item:
                    break
                new_item = dataset.all_item[idx_class]

                item_list.append(new_item)
                crt_set = self.create_set_from_list(item_list, dataset.set_size)

            samples.append(crt_set)
        return torch.stack(samples, dim=1).squeeze(0)


    def sample_item_from_germ(self, germ):
        """
        Samples 1 item from the generator based on the germ set
        germ shape = [batch, set_size]

        Outputs: out
            - germ_up: A copy of germ with one added item
        germ_up shape = [batch, set_size]

        """
        dataset = self.dataset
        germ_up = []
        set_germ_proba = torch.exp(self(germ)) # Probabilities of every set in the germ batch

        for set_germ, proba in zip(germ, set_germ_proba):

            item_list = [int(item.item()) for item in set_germ if item.item() > 0]
            updated_proba = self.mask_already_item_pred(proba.unsqueeze(0), item_list)
            idx_class = torch.multinomial(updated_proba, num_samples=1)

            if dataset.all_item[idx_class] == dataset.stop_item:
                set_germ_up = self.create_set_from_list(item_list, dataset.set_size)
                germ_up.append(set_germ_up)
                continue

            new_item = dataset.all_item[idx_class]

            item_list.append(new_item)
            set_germ_up = self.create_set_from_list(item_list, dataset.set_size)
            germ_up.append(set_germ_up)

        germ_up = torch.stack(germ_up, dim=1).squeeze(0)
        return germ_up

    @staticmethod
    def sample_one_set(model):
        dataset = model.dataset
        new_item = dataset.stop_item
        item_list = []
        while new_item == dataset.stop_item:
            idx_class = torch.multinomial(dataset.popularity.type(torch.float), num_samples=1)
            new_item = dataset.all_item[idx_class]
        item_list.append(new_item)
        crt_set = model.create_set_from_list(item_list, dataset.set_size)

        while new_item != dataset.stop_item and len(item_list) < dataset.set_size:
            out = model(crt_set)
            vect_pred = torch.exp(out)
            updated_proba = model.mask_already_item_pred(vect_pred, item_list)
            idx_class = torch.multinomial(updated_proba, num_samples=1)
            if dataset.all_item[idx_class] == dataset.stop_item:
                break
            new_item = dataset.all_item[idx_class]
            item_list.append(new_item)
            crt_set = model.create_set_from_list(item_list, dataset.set_size)

        return crt_set


    def sample_parallel(self, batch_size):
        model = self
        model.share_memory()
        p = mp.Pool()
        array = [model for _ in range(batch_size)]
        result = p.map(self.sample_one_set, array)
        p.close()
        p.join()
        return torch.stack(result, dim=1).squeeze(0)

class LSTM_Generator(Generator):
    def __init__(self, dataset, vocab_size, embedding_dim, hidden_dim, use_cuda):
        super().__init__(dataset, vocab_size, embedding_dim, hidden_dim, use_cuda)
        self.model = nn.LSTM(embedding_dim, hidden_dim, batch_first=True)
    def forward(self, x):
        self.model.flatten_parameters()
        emb = self.embed(x.type(torch.long))
        enc, _ = self.model(emb.type(torch.float))
        out = self.fc(enc)
        out = torch.log_softmax(out, dim=2)
        pred = out[:, -1, :]
        return pred

class SetTransformer_Generator(Generator):
    def __init__(self, dataset, vocab_size, embedding_dim, hidden_dim, use_cuda, n_head=4):
        super().__init__(dataset, vocab_size, embedding_dim, hidden_dim, use_cuda)
        self.model = nn.Sequential(
            SAB(dim_in=embedding_dim, dim_out=hidden_dim, num_heads=n_head),
            PMA(dim=hidden_dim, num_heads=1, num_seeds=1))
    def forward(self, x):
        emb = self.embed(x.type(torch.long))
        out = self.model(emb)
        out_fc = self.fc(out.reshape(-1, self.hidden_dim))
        return torch.log_softmax(out_fc, dim=1)

class DeepSet_Generator(Generator):
    def __init__(self, dataset, vocab_size, embedding_dim, hidden_dim, use_cuda, n_enc=2, n_dec=2):
        super().__init__(dataset, vocab_size, embedding_dim, hidden_dim, use_cuda)
        self.enc = []
        self.enc.append(nn.Linear(embedding_dim, hidden_dim))
        self.enc.append(nn.ReLU())
        for _ in range(n_enc):
            self.enc.append(nn.Linear(hidden_dim, hidden_dim))
            self.enc.append(nn.ReLU())
        self.enc.append(nn.Linear(hidden_dim, hidden_dim))
        self.enc = nn.Sequential(*self.enc)
        self.dec = []
        for _ in range(n_dec):
            self.dec.append(nn.Linear(hidden_dim, hidden_dim))
            self.dec.append(nn.ReLU())
        self.dec = nn.Sequential(*self.dec)
    def forward(self, x):
        emb = self.embed(x.type(torch.long))
        enc = self.enc(emb)
        pool = enc.max(dim=1)[0]
        out = self.dec(pool)
        out = self.fc(out)
        out = torch.log_softmax(out, dim=1)
        return out


# def sample_one_set(model):
#     dataset = model.dataset
#     new_item = dataset.stop_item
#     item_list = []
#     while new_item == dataset.stop_item:
#         idx_class = torch.multinomial(dataset.popularity.type(torch.float), num_samples=1)
#         new_item = dataset.all_item[idx_class]
#     item_list.append(new_item)
#     crt_set = model.create_set_from_list(item_list, dataset.set_size)
#
#     while new_item != dataset.stop_item and len(item_list) < dataset.set_size:
#         out = model(crt_set)
#         vect_pred = torch.exp(out)
#         updated_proba = model.mask_already_item_pred(vect_pred, item_list)
#         idx_class = torch.multinomial(updated_proba, num_samples=1)
#         if dataset.all_item[idx_class] == dataset.stop_item:
#             break
#         new_item = dataset.all_item[idx_class]
#         item_list.append(new_item)
#         crt_set = model.create_set_from_list(item_list, dataset.set_size)
#
#     return crt_set
#
# def sample_parallel(model, batch_size):
#     model.share_memory()
#     p = mp.Pool()
#     array = [model for _ in range(batch_size)]
#     result = p.map(sample_one_set, array)
#     p.close()
#     p.join()
#     return torch.stack(result, dim=1).squeeze(0)

# file_path = '/Files/toy_problem.csv'
# dataset = get_data(file_path, ratio_train=.8, batch_size=128)
# model = SetTransformer_Generator(dataset, 101, 32, 64, False)
# start = time()
# # out = sample_parallel(model, 10000)
# out = model.sample_parallel(10000)
# end = time() - start
# print(out.shape)
# print("Multiprocessing took ", end)
# start = time()
# out1 = model.sample(10000)
# end = time() - start
# print(out1.shape)
# print("Classic sampling took ", end)
#
# # Multiprocessing took  83.94663119316101
# # Classic sampling took  212.71500205993652
# from torch.utils.data import DataLoader, TensorDataset
#
# # dataset_train = TensorDataset(torch.stack(subset_train), torch.stack(subset_train_target))
# fake_loader = DataLoader(out, batch_size=64, shuffle=True, drop_last=True)