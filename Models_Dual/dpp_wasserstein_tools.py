import ot
import torch
import numpy as np
import matplotlib.pyplot as plt
import os
from Models.dpp_builder.nonsymmetric_dpp_learning import compute_log_likelihood
from copy import deepcopy
import metrics
import random
torch.random.manual_seed(5)
random.seed(5)
np.random.seed(5)

'''
Map a collection of sets minibatch of items {1,...,n} to their index in the DPP kernel {0,...,n-1}
'''
def map_item_to_index(minibatch):
    def map(l): return [item - 1 for item in l]
    return [map(set) for set in minibatch]


'''
Map a collection of sets minibatch of index {0,...,n-1} (sampled from the DPP for example)
    to their respective item in the catalog {1,...,n}
'''
def map_index_to_item(minibatch):
    def map(l): return [item + 1 for item in l]
    return [map(set) for set in minibatch]


'''
Transform a collection of set (list of list) into a tensor (number_set, set_length)
'''
def from_list_to_padded_tensor(col, set_len):
    padded_col = []
    max_len = max([len(s) for s in col])
    length = max(max_len, set_len)
    for s in col:
        padded_set = deepcopy(s)
        for _ in range(len(padded_set), length):
            padded_set.append(0)
        padded_col.append(padded_set)
    return torch.tensor(padded_col)


'''
Build the likelihood kernel out of the nonsymmetric DPP object
'''
def get_kernel(model, nonsymmetric=False):
    V = model.get_v_embeddings()
    if nonsymmetric:
      B = model.get_b_embeddings()
      C = model.get_c_embeddings()
      L = V.mm(V.transpose(0, 1)) + B.mm(C.transpose(0, 1)) - C.mm(B.transpose(0, 1))
    else:
        L = V.mm(V.transpose(0, 1))
    return L


'''
Compute a distance between a real collection of sets real_collection and a fake collection of sets (sampled inside)
The distance is made of multiple random boostraps
'''
def eval_sampling(real_col, fake_col, dataset, total_dist=False, dist='perfect_item'):
    if not (dist == 'perfect_item' or dist == 'jaccard' or dist == 'sparse_vector'):
        raise ValueError("Eval sampling : Wrong set distance arg")
    dist_wasserstein, dist_chamfer = metrics.wasserstein_chamfer_distance(real_col, fake_col,
                                                                          set_distance=dist,
                                                                          dataset=dataset, total_distance=total_dist)
    return dist_wasserstein, dist_chamfer


'''
Compute the loglikelihood of the collection of sets minibatch from the DPP generator.
'''
def get_loglikeli_from_gen(gen, minibatch):
    indexed_minibatch = map_item_to_index(minibatch)
    loglikeli = compute_log_likelihood(gen, indexed_minibatch, reduce=False)
    return loglikeli


'''
Compute the output of the discriminator on the collection of sets minibatch
'''
def get_realness(dis, minibatch, set_len):
    pad_minibatch = from_list_to_padded_tensor(minibatch, set_len)
    return dis(pad_minibatch)


'''
Compute a dummy realness on the collection of sets minibatch just for sanity check of the generator updates
'''
def get_dummy_realness(minibatch):
    realness_list = []
    for s in minibatch:
        realness = torch.rand(1).item()
        nb_item = 0.
        for i in s:
            if i < 5:
                nb_item = nb_item + 1.
        realness_list.append(max(realness, nb_item))
    return torch.tensor(realness_list)

def get_perfect_toy_realness(minibatch):
    realness_list = []
    for s in minibatch:
        realness = torch.rand(1).item() / 10
        nb_item = 0.
        for idx in range(1, len(s)):
            if s[idx] == s[idx-1] + 1:
                nb_item = nb_item + 1.
        realness_list.append(max(realness, nb_item))
    return torch.tensor(realness_list)


def save_bar_chart(collection, epoch):
    item_popularity = {}
    for set in collection:
        for item in set:
            if item in item_popularity.keys():
                item_popularity[item] = item_popularity[item] + 1
            else :
                item_popularity[item] = 1
    keys = list(item_popularity.keys())
    keys.sort()
    values = [item_popularity[k] for k in keys]
    plt.figure(figsize=(10,8))
    plt.xlabel("Items")
    plt.ylabel("Frequency of apparition")
    plt.title("Item popularity in the fake sets at epoch " + str(epoch))
    plt.bar(keys, values, alpha=1, label='Item popularity')
    plt.savefig(os.getcwd()+'/item_popularity_fake_set_epoch' + str(epoch) + '.png')
    plt.close()


def jaccard_distance(fst_set, snd_set):
    fst_set = set(fst_set)
    snd_set = set(snd_set)
    intersection = len(fst_set.intersection(snd_set))
    union = len(fst_set.union(snd_set))
    return 1 - intersection/(max(union, 10**-6)) + 1e-10

def perfect_set_dist_toy_data(fst_set, snd_set):
    sorted_fst_set = deepcopy(fst_set)
    sorted_fst_set.sort()
    sorted_snd_set = deepcopy(snd_set)
    sorted_snd_set.sort()
    #If the sets don't have the same length
    if len(sorted_fst_set) < len(sorted_snd_set):
        sorted_fst_set.extend([0 for _ in range(len(sorted_snd_set)-len(sorted_fst_set))])
    else:
        sorted_snd_set.extend([0 for _ in range(len(sorted_fst_set)-len(sorted_snd_set))])
    dist = 0
    for idx in range(len(sorted_fst_set)):
        i1 = sorted_fst_set[idx]
        i2 = sorted_snd_set[idx]
        dist = dist + (i1 - i2)**2

    return np.sqrt(dist + 1e-10)


def compute_lipschitz_const(true_pred, true_minibatch, fake_pred, fake_minibatch, model_embed=None, set_len=4):
    const = torch.tensor([0.])
    n_pairs = len(true_minibatch) // 2
    first_boostrap_idx = np.random.randint(size=n_pairs, low=0, high=len(true_minibatch))
    second_bootstrp_idx = np.random.randint(size=n_pairs, low=0, high=len(fake_minibatch))
    for fst_idx in first_boostrap_idx:
        for snd_idx in second_bootstrp_idx:
            true_set, true_p = true_minibatch[fst_idx], true_pred[fst_idx]
            fake_set, fake_p = fake_minibatch[snd_idx], fake_pred[snd_idx]
            true_set_padded = from_list_to_padded_tensor([true_set], set_len)
            fake_set_padded = from_list_to_padded_tensor([fake_set], set_len)
            true_set_embedding = model_embed.get_set_embedding(true_set_padded)
            fake_set_embedding = model_embed.get_set_embedding(fake_set_padded)
            value = torch.norm(true_set_embedding - fake_set_embedding)
            # value = torch.norm((torch.norm(true_p - fake_p) / jaccard_distance(true_set, fake_set)) - 1)
            # value = torch.norm((torch.norm(true_p - fake_p) / perfect_set_dist_toy_data(true_set, fake_set)) - 1)
            # value = torch.norm((torch.norm(true_p - fake_p) / metrics.l2_sparse_vect(true_set, fake_set)) - 1)
            const = const + torch.max(torch.tensor([0.]), value)
    return torch.mean(const)

'''
Compute a stopping criterion for the pre-training of the discriminator;
set_col : list of list (collection of sets)
minibatch_len : int (length of a minibatch)
'''
def dis_has_converge(dis, set_col, minibatch_len):
    idx = np.random.randint(low=0, high=len(set_col), size=minibatch_len).tolist()
    fst_random_minibatch = [set_col[i] for i in idx]
    idx = np.random.randint(low=0, high=len(set_col), size=minibatch_len).tolist()
    snd_random_minibatch = [set_col[i] for i in idx]

    fst_random_minibatch_padded = from_list_to_padded_tensor(fst_random_minibatch, minibatch_len)
    fst_random_minibatch_pred = dis(fst_random_minibatch_padded)

    snd_random_minibatch_padded = from_list_to_padded_tensor(snd_random_minibatch, minibatch_len)
    snd_random_minibatch_pred = dis(snd_random_minibatch_padded)

    # Computing the criterion
    has_converged = False
    if torch.abs(torch.mean(fst_random_minibatch_pred) - torch.mean(snd_random_minibatch_pred)) \
            - (2 * torch.sqrt(torch.var(fst_random_minibatch_pred) / minibatch_len)) < 1e-3:
        has_converged = True

    return has_converged


'''
TRAINING FUNCTIONS
'''
def train_discriminator(dis, optimizer, true_col, fake_col, minibatch_len=64, set_len=24, lambd=1e-15, model_embed=None):
    sum_loss = 0.
    n_minibatch = int(len(true_col) / minibatch_len)

    for idx in range(n_minibatch):
        optimizer.zero_grad()

        true_minibatch = deepcopy(true_col[int(idx*minibatch_len) : int((idx+1)*minibatch_len)])
        true_minibatch_padded = from_list_to_padded_tensor(true_minibatch, set_len)
        true_pred = dis(true_minibatch_padded)

        fake_minibatch = deepcopy(fake_col[int(idx*minibatch_len) : int(minibatch_len*(idx+1))])
        fake_minibatch_padded = from_list_to_padded_tensor(fake_minibatch, set_len)
        fake_pred = dis(fake_minibatch_padded)

        loss = torch.mean(true_pred) - torch.mean(fake_pred) - lambd*compute_lipschitz_const(true_pred, true_minibatch,
                                                                                             fake_pred, fake_minibatch,
                                                                                             model_embed)

        sum_loss = sum_loss + loss.item()
        (-loss).backward()
        optimizer.step()

    return sum_loss


'''
EVALUATION FUNCTION
'''
def eval_discriminator(dis, true_col, fake_col, minibatch_len=64, set_len=24, lambd=1e-15, model_embed=None):
    sum_loss = 0.
    n_minibatch = int(len(true_col) / minibatch_len)

    for idx in range(n_minibatch):

        true_minibatch = deepcopy(true_col[int(idx*minibatch_len) : int((idx+1)*minibatch_len)])
        true_minibatch_padded = from_list_to_padded_tensor(true_minibatch, set_len)
        true_pred = dis(true_minibatch_padded)

        fake_minibatch = deepcopy(fake_col[int(idx*minibatch_len) : int(minibatch_len*(idx+1))])
        fake_minibatch_padded = from_list_to_padded_tensor(fake_minibatch, set_len)
        fake_pred = dis(fake_minibatch_padded)

        loss = torch.mean(true_pred) - torch.mean(fake_pred) - lambd*compute_lipschitz_const(true_pred, true_minibatch,
                                                                                             fake_pred, fake_minibatch,
                                                                                             model_embed)
        sum_loss = sum_loss + loss.item()
    return sum_loss

def eval_generator(gen, dis, fake_col, n_minibatch, minibatch_len, set_len):
    avg_loss = 0.

    for idx in range(n_minibatch):
        fake_minibatch = fake_col[int(idx*minibatch_len) : int((idx+1)*minibatch_len)]
        random.shuffle(fake_minibatch)


        fake_loglikeli = get_loglikeli_from_gen(gen, fake_minibatch)
        fake_realness = get_realness(dis, fake_minibatch, set_len)

        loss = fake_loglikeli * fake_realness
        mean_loss = torch.mean(loss)
        avg_loss = avg_loss + mean_loss.item()

    return avg_loss / n_minibatch


def compute_dphi(dis, fst_col, snd_col, set_len=24):
    fst_minibatch = deepcopy(fst_col)
    fst_minibatch_padded = from_list_to_padded_tensor(fst_minibatch, set_len)
    fst_pred = dis(fst_minibatch_padded)

    snd_minibatch = deepcopy(snd_col)
    snd_minibatch_padded = from_list_to_padded_tensor(snd_minibatch, set_len)
    snd_pred = dis(snd_minibatch_padded)

    loss = torch.abs(torch.mean(fst_pred) - torch.mean(snd_pred))
    avg_loss = loss.item()
    return avg_loss





'''
Adding noise to true data for a more robust pre training of the discriminator.
    -> Doesn't work
'''
def add_noise_one_item(minibatch, all_item):
    noised_minibatch = []
    for set in minibatch:
        noised_set = deepcopy(set)
        rand_idx = np.random.randint(len(noised_set))
        rand_item = all_item[np.random.randint(len(all_item))]
        noised_set[rand_idx] = rand_item
        noised_minibatch.append(noised_set)
    return noised_minibatch
def replace_with_noise(minibatch, all_item):
    noised_minibatch = []
    for set in minibatch:
        noised_set = np.random.choice(all_item, size=len(set)).tolist()
        noised_minibatch.append(noised_set)

    return noised_minibatch

def train_discriminator_random(dis, optimizer, true_col, all_item, randomness, minibatch_len=64, set_len=24, lambd=1e-15):
    sum_loss = 0.
    n_minibatch = int(len(true_col) / minibatch_len)

    for idx in range(n_minibatch):
        optimizer.zero_grad()

        true_minibatch = deepcopy(true_col[int(idx*minibatch_len) : int((idx+1)*minibatch_len)])
        true_minibatch_padded = from_list_to_padded_tensor(true_minibatch, set_len)
        true_pred = dis(true_minibatch_padded)

        if randomness == 'one_item':
            fake_minibatch = add_noise_one_item(true_minibatch, all_item)
        if randomness == 'total':
            fake_minibatch = replace_with_noise(true_minibatch, all_item)
        fake_minibatch_padded = from_list_to_padded_tensor(fake_minibatch, set_len)
        fake_pred = dis(fake_minibatch_padded)

        loss = torch.mean(true_pred) - torch.mean(fake_pred) - lambd*compute_lipschitz_const(true_pred, true_minibatch,\
                                                                                             fake_pred, fake_minibatch)

        sum_loss = sum_loss + loss.item()
        (-loss).backward()
        optimizer.step()

    return sum_loss

