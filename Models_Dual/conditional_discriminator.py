import torch
import torch.nn as nn
import torch.nn.functional as F
from copy import deepcopy
import random
torch.random.manual_seed(5)
random.seed(5)
from Models.modules_set_transformer import PMA, SAB
import numpy as np

class Conditional_Discriminator(nn.Module):
    """ Discriminator """
    def __init__(self, vocab_size, embedding_dim, hidden_dim, use_cuda):
        super(Conditional_Discriminator, self).__init__()
        self.hidden_dim = hidden_dim
        self.use_cuda = use_cuda
        self.vocab_size = vocab_size
        self.embed = nn.Embedding(vocab_size, embedding_dim, padding_idx=0)
        self.model = None
        self.fc_out = nn.Linear(vocab_size, 1)
        self.fc_hidden = nn.Linear(hidden_dim, vocab_size)

    def forward(self, context, x):
        """
        Embeds the context set to produce one prediction from it.
        Compare the prediction to x
        From this comparison produce a scalar

        Inputs: context, x
            - context : (batch_size, set_len), batch of padded subsets of size set_len
            - x: (batch_size, vocab_size), batch of predictions on the item set
        Outputs: out
            - out: (batch_size, 1), sigmoid value stating the realness of the x given the context
        """
        return

    def pred_from_context(self, context):
        """
        Embeds the context set to produce one prediction from it.

        Inputs: context
            - context : (batch_size, set_len), batch of padded subsets of size set_len
        Outputs: out
            - out: (batch_size, vocab_size), batch of predictions on the item set given the context
        """
        return


class LSTM_Conditional_Discriminator(Conditional_Discriminator):
    def __init__(self, vocab_size, embedding_dim, hidden_dim, use_cuda):
        super().__init__(vocab_size, embedding_dim, hidden_dim, use_cuda)
        self.model = nn.LSTM(embedding_dim, hidden_dim, batch_first=True)

    def pred_from_context(self, context):
        self.model.flatten_parameters()
        emb = self.embed(context.type(torch.long))
        enc, _ = self.model(emb.type(torch.float))
        out = self.fc_hidden(enc)
        out = torch.log_softmax(out, dim=2)
        pred = out[:, -1, :]
        return pred

    def forward(self, context, x):
        disc_pred = self.pred_from_context(context)
        compared = x - disc_pred
        out = torch.sigmoid(self.fc_out(compared))
        return out


class SetTransformer_Conditional_Discriminator(Conditional_Discriminator):
    def __init__(self, vocab_size, embedding_dim, hidden_dim, use_cuda, n_head=4):
        super().__init__(vocab_size, embedding_dim, hidden_dim, use_cuda)
        self.model = nn.Sequential(
            SAB(dim_in=embedding_dim, dim_out=hidden_dim, num_heads=n_head),
            PMA(dim=hidden_dim, num_heads=1, num_seeds=1))

    def pred_from_context(self, context):
        emb = self.embed(context.type(torch.long))
        out = self.model(emb)
        out_fc = self.fc_hidden(out.reshape(-1, self.hidden_dim))
        return torch.log_softmax(out_fc, dim=1)

    def forward(self, context, x):
        disc_pred = self.pred_from_context(context) # [64, 301]
        compared = x - disc_pred
        out = torch.sigmoid(self.fc_out(compared))
        return out


class DeepSet_Conditional_Discriminator(Conditional_Discriminator):
    def __init__(self, vocab_size, embedding_dim, hidden_dim, use_cuda, n_enc=2, n_dec=2):
        super().__init__(vocab_size, embedding_dim, hidden_dim, use_cuda)
        self.enc = []
        self.enc.append(nn.Linear(embedding_dim, hidden_dim))
        self.enc.append(nn.ReLU())
        for _ in range(n_enc):
            self.enc.append(nn.Linear(hidden_dim, hidden_dim))
            self.enc.append(nn.ReLU())
        self.enc.append(nn.Linear(hidden_dim, hidden_dim))
        self.enc = nn.Sequential(*self.enc)
        self.dec = []
        for _ in range(n_dec):
            self.dec.append(nn.Linear(hidden_dim, hidden_dim))
            self.dec.append(nn.ReLU())
        self.dec = nn.Sequential(*self.dec)

    def pred_from_context(self, context):
        emb = self.embed(context.type(torch.long))
        enc = self.enc(emb)
        pool = enc.max(dim=1)[0]
        out = self.dec(pool)
        out = self.fc_hidden(out)
        out = torch.log_softmax(out, dim=1)
        return out

    def forward(self, context, x):
        disc_pred = self.pred_from_context(context) # [64, 301]
        compared = x - disc_pred
        out = torch.sigmoid(self.fc_out(compared))
        return out

class Conv_Conditional_Discriminator(nn.Module):
    """
    A CNN for text classification.
    Uses an embedding layer, followed by a convolutional, max-pooling and softmax layer.
    Highway architecture based on the pooled feature maps is added. Dropout is adopted.
    """

    def __init__(self, num_classes, vocab_size, embedding_dim, filter_sizes, num_filters, dropout_prob):
        super(Conv_Conditional_Discriminator, self).__init__()
        self.embed = nn.Embedding(vocab_size, embedding_dim)
        self.convs = nn.ModuleList([
            nn.Conv2d(1, num_f, (f_size, embedding_dim)) for f_size, num_f in zip(filter_sizes, num_filters)
        ])
        self.highway = nn.Linear(sum(num_filters), sum(num_filters))
        self.dropout = nn.Dropout(p = dropout_prob)
        # self.fc = nn.Linear(sum(num_filters), num_classes)

    def pred_from_context(self, x):
        """
        Inputs: x
            - x: (batch_size, seq_len)
        Outputs: out
            - out: (batch_size, num_classes)
        """
        emb = self.embed(x.type(torch.long)).unsqueeze(1)
        convs = [F.relu(conv(emb)).squeeze(3) for conv in self.convs]
        pools = [F.max_pool1d(conv, conv.size(2)).squeeze(2) for conv in convs]
        out = torch.cat(pools, 1)
        highway = self.highway(out)
        transform = torch.sigmoid(highway)
        out = transform * F.relu(highway) + (1. - transform) * out
        out = torch.log_softmax(self.fc_hidden(self.dropout(out)), dim=1)
        return out

    def forward(self, context, x):
        disc_pred = self.pred_from_context(context) # [64, 301]
        compared = x - disc_pred
        out = torch.sigmoid(self.fc_out(compared))
        return out