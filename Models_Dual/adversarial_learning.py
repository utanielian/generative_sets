import torch
import torch.nn as nn
import torch.optim as optim
import torch.backends.cudnn as cudnn

from Models_Dual.generator import SetTransformer_Generator, DeepSet_Generator, LSTM_Generator
from Models_Dual.conditional_discriminator import LSTM_Conditional_Discriminator, \
    SetTransformer_Conditional_Discriminator, DeepSet_Conditional_Discriminator, Conv_Conditional_Discriminator

from data import get_data, get_data_all_subset
from Models_Dual.training_tools import train_generator, eval_generator, \
    train_conditional_discriminator_one_item, eval_conditional_discriminator, adversarial_train_conditional_disc
import random
torch.random.manual_seed(5)
random.seed(5)
from time import time
import os
import numpy as np
import metrics


if __name__ == '__main__':
    MAX_EPOCH = 50
    rounds = 20
    g_embed_dim = 32
    g_hidden_dim = 64
    g_seq_len = 24
    seed = 5
    cuda = torch.cuda.is_available()
    torch.manual_seed(seed)
    if cuda:
        torch.cuda.manual_seed(seed)

    # Get Data
    print('#####################################################')
    print('Building dataset')
    print('#####################################################\n')
    batch_size = 64
    file_path = '/Files/1_100_100_100_apparel-diaper-feeding_regs.csv'
    # file_path = '/Files/toy_problem.csv'
    dataset = get_data(file_path, ratio_train=.8, batch_size=batch_size)
    # dataset = get_data_all_subset(file_path, ratio_train=.8, batch_size=128, max_set_size=10)

    # Set models, criteria, optimizers
    vocab_size = dataset.n_item
    g_seq_len = dataset.set_size
    n_samples = len(dataset.train_loader)
    generator = SetTransformer_Generator(dataset, vocab_size, g_embed_dim, g_hidden_dim, cuda)
    # generator = DeepSet_Generator(dataset, vocab_size, g_embed_dim, g_hidden_dim, cuda)
    # generator = LSTM_Generator(dataset, vocab_size, g_embed_dim, g_hidden_dim, cuda)
    discriminator = SetTransformer_Conditional_Discriminator(vocab_size, g_embed_dim, g_hidden_dim, cuda)
    # discriminator = LSTM_Conditional_Discriminator(vocab_size, g_embed_dim, g_hidden_dim, cuda)
    # discriminator = DeepSet_Conditional_Discriminator(vocab_size, g_embed_dim, g_hidden_dim, cuda)
    # d_num_class = 1
    # d_embed_dim = 64
    # d_filter_sizes = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20]
    # d_num_filters = [100, 200, 200, 200, 200, 100, 100, 100, 100, 100, 160, 160]
    # d_dropout_prob = 0.2
    # discriminator = Conv_Conditional_Discriminator(d_num_class, vocab_size, d_embed_dim, d_filter_sizes, d_num_filters, d_dropout_prob)

    nll_loss = nn.NLLLoss(reduction='none')
    bce_loss = nn.BCELoss()
    gen_optimizer = optim.Adam(params=generator.parameters(), lr=1e-3)
    dis_optimizer = optim.Adam(params=discriminator.parameters(), lr=1e-3)

    if cuda:
        generator = generator.cuda()
        discriminator = discriminator.cuda()
        nll_loss = nll_loss.cuda()
        cudnn.benchmark = True


    # Container of experiment data
    gen_pretrain_train_loss = []
    gen_pretrain_eval_loss = []
    dis_pretrain_train_loss = []
    dis_pretrain_train_acc = []
    dis_pretrain_eval_loss = []
    dis_pretrain_eval_acc = []
    gen_adversarial_eval_loss = []
    dis_adversarial_train_loss = []
    dis_adversarial_train_acc = []
    dis_adversarial_eval_loss = []
    dis_adversarial_eval_acc = []

    path = os.getcwd() + '/generator_untrained.pt'
    torch.save(generator, path)
    path = os.getcwd() + '/discriminator_untrained.pt'
    torch.save(discriminator, path)

    # Set the log file
    path = os.getcwd() + '/log_pretraining.txt'
    log_file = open(path, 'w')

    print('#####################################################')
    print('Start Generator Pre-Training')
    print('#####################################################\n')
    log_file.write('MLE GENERATOR PRETRAINING\n')
    min_loss = [np.inf, -1]
    patience = 4
    for epoch in range(MAX_EPOCH):
        print("Epoch {}".format(epoch))
        # start = time()
        gen_loss = train_generator(generator, dataset.train_loader, nll_loss,
            gen_optimizer)
        # end = time() - start
        # print('One generator train epoch required ', end)
        gen_test_loss = eval_generator(generator, dataset.test_loader, nll_loss)
        gen_pretrain_eval_loss.append(gen_test_loss)
        gen_pretrain_train_loss.append(gen_loss)
        print("Train loss: {:.5f}".format(gen_loss))
        print("Eval loss: {:.5f}\n".format(gen_test_loss))

        log_file.write('EPOCH {} : Train loss : {:.5f}, Test loss : {:.5f}\n'.format(epoch, gen_loss, gen_test_loss))
        # Early Stopping criterion
        gen_val_loss = eval_generator(generator, dataset.val_loader, nll_loss)
        if gen_val_loss < min_loss[0]:
            min_loss[0] = gen_val_loss
            min_loss[1] = epoch
        else:
            if min_loss[1] + patience <= epoch:
                break
    print('#####################################################\n\n')
    path = os.getcwd() + '/generator_pretrained.pt'
    torch.save(generator, path)

    # # Load pretrained generator's weights
    # path = os.getcwd() + '/generator_pretrained.pt'
    # generator = torch.load(path)


    print('#####################################################')
    print('Start Discriminator Pre-Training')
    print('#####################################################\n')
    accuracy_thresh = 0.75
    log_file.write('MLE DISCRIMINATOR PRETRAINING\n')
    for epoch in range(MAX_EPOCH):
        print("Epoch {}".format(epoch))
        # start = time()
        dis_train_loss, dis_avg_acc = train_conditional_discriminator_one_item(discriminator, generator,
                                                                             dataset.train_loader, bce_loss,
                                                                             dis_optimizer, dataset)
        # end = time() - start
        # print('One discriminator train epoch required ', end)     45 sec
        print("\nTrain loss: {:.5f} | Train acc (True/Fake): {:.5f}/{:.5f}".format(dis_train_loss, dis_avg_acc[0], dis_avg_acc[1]))
        dis_test_loss, dis_test_acc = eval_conditional_discriminator(discriminator, generator,
                                                                     dataset.test_loader, bce_loss, dataset)

        print("Test loss: {:.5f} | Test acc (True/Fake): {:.5f}/{:.5f}: \n".format(dis_test_loss, dis_test_acc[0], dis_test_acc[1]))
        dis_pretrain_eval_loss.append(dis_test_loss)
        dis_pretrain_eval_acc.append(dis_test_acc)
        dis_adversarial_train_loss.append(dis_train_loss)
        log_file.write('EPOCH {} : Train loss : {:.5f}, Test loss : {:.5f}\n'.format(epoch, dis_train_loss, dis_test_loss))

        # Accuracy stopping criterion
        dis_test_loss, dis_test_acc = eval_conditional_discriminator(discriminator, generator,
                                                                     dataset.val_loader, bce_loss, dataset)
        if np.mean(dis_test_acc) >= accuracy_thresh:
            break
    print('#####################################################\n\n')
    log_file.close()
    path = os.getcwd() + '/discriminator_pretrained.pt'
    torch.save(discriminator, path)

    # Loading the models already pretrained weights
    path = os.getcwd() + '/generator_pretrained.pt'
    generator = torch.load(path)
    path = os.getcwd() + '/discriminator_pretrained.pt'
    discriminator = torch.load(path)

    # Set optimizers for the adversarial learning
    gen_dual_optimizer = optim.Adam(params=generator.parameters(), lr=1e-4, weight_decay=1e-4)
    dis_dual_optimizer = optim.Adam(params=discriminator.parameters(), lr=1e-4)

    path = os.getcwd() + '/log_adversarial_training.txt'
    log_file = open(path, 'w')
    gen_train_list = []
    gen_test_list = []
    dis_train_list = []
    dis_test_list = []
    print('#####################################################')
    print('Start Adversarial Training')
    print('#####################################################\n')
    MAX_EPOCH = 20
    min_loss = [np.inf, -1]
    patience = 4
    flag_overfitting = False
    for epoch in range(MAX_EPOCH):
        print("Round {}".format(epoch))
        # start = time()
        adversarial_train_conditional_disc(generator, discriminator, dataset.train_loader, bce_loss,
                                           nll_loss, gen_dual_optimizer, dis_dual_optimizer, 1, 1, dataset)

        gen_train_loss = eval_generator(generator, dataset.train_loader, nll_loss)
        gen_test_loss = eval_generator(generator, dataset.test_loader, nll_loss)
        dis_train_loss, dis_train_acc = eval_conditional_discriminator(discriminator, generator,
                                                                     dataset.train_loader, bce_loss, dataset)
        dis_test_loss, dis_test_acc = eval_conditional_discriminator(discriminator, generator,
                                                                     dataset.test_loader, bce_loss, dataset)
        # end = time() - start
        # print('Time 1 epoch ', end)
        print("Gen Train loss: {:.5f}, Test loss : {:.5f}".format(gen_train_loss, gen_test_loss))
        print("Disc Train loss: {:.5f}, Test loss : {:.5f} | Train acc (True/Fake): {:.5f}/{:.5f}\n".format(dis_train_loss, dis_test_loss, dis_train_acc[0], dis_train_acc[1]))
        log_file.write('ROUND {} : Gen Train loss : {:.5f}, Test loss : {:.5f} | Dis Train loss : {:.5f}, Test loss : {:.5f}\n'.format(epoch, gen_train_loss, gen_test_loss, dis_train_loss, dis_test_loss))

        gen_train_list.append(gen_train_loss)
        gen_test_list.append(gen_test_loss)
        dis_train_list.append(dis_train_loss)
        dis_test_list.append(dis_test_loss)
        # Early Stopping criterion
        if not flag_overfitting:
            gen_val_loss = eval_generator(generator, dataset.val_loader, nll_loss)
            if gen_val_loss < min_loss[0]:
                min_loss[0] = gen_val_loss
                min_loss[1] = epoch
            else:
                # Overfitting stopping criterion
                if min_loss[1] + patience <= epoch:
                    flag_overfitting = True
                    log_file.close()
                    path = os.getcwd() + '/discriminator_dual_trained.pt'
                    torch.save(discriminator, path)
                    path = os.getcwd() + '/generator_dual_trained.pt'
                    torch.save(generator, path)

                    # Let's pursue in the overfitting just to compare the evaluation afterward
                    path = os.getcwd() + '/log_post_adversarial_training.txt'
                    log_file = open(path, 'w')

        # Save each epoch's models weights to do the evaluation through the epochs afterward
        path = os.getcwd() + '/tmp_dual_models/generator_epoch' + str(epoch) + '.pt'
        torch.save(generator, path)
        path = os.getcwd() + '/tmp_dual_models/discriminator_epoch' + str(epoch) + '.pt'
        torch.save(discriminator, path)
    log_file.close()

    # Load the final model's weight
    path = os.getcwd() + '/discriminator_overfitted.pt'
    torch.save(discriminator, path)
    path = os.getcwd() + '/generator_overfitted.pt'
    torch.save(generator, path)

    print('#####################################################')
    print('Evaluation of the generator')
    print('#####################################################\n')
    path = os.getcwd() + '/evaluation.txt'
    log_file = open(path, 'w')

    path = os.getcwd() + '/tmp_dual_models/generator_epoch19.pt'
    evaluated_gen = torch.load(path)


    # Before any training
    log_file.write("Before any training\n")
    path = os.getcwd() + '/generator_untrained.pt'
    evaluated_gen = torch.load(path)
    n_train = len(dataset.train_col)
    sample = evaluated_gen.sample(n_train)
    sample_list = sample.numpy().tolist()
    dist_wasserstein, dist_chamfer = metrics.wasserstein_chamfer_distance(dataset.train_col, sample_list)

    log_file.write("Wasserstein : ({:.5f}, ({:.5f}, {:.5f}))\n".format(dist_wasserstein[0], dist_wasserstein[1][0], dist_wasserstein[1][1]))
    log_file.write("Chamfer : ({:.5f}, ({:.5f}, {:.5f}))\n".format(dist_chamfer[0], dist_chamfer[1][0], dist_chamfer[1][1]))

    # After the pretaining
    log_file.write("\nPre-trained Generator\n")
    path = os.getcwd() + '/generator_pretrained.pt'
    evaluated_gen = torch.load(path)
    n_train = len(dataset.train_col)
    sample = evaluated_gen.sample(n_train)
    sample_list = sample.numpy().tolist()
    # dist_wasserstein, dist_chamfer = metrics.wasserstein_chamfer_distance(dataset.train_col, sample_list)
    # dist_wasserstein, dist_chamfer = metrics.wasserstein_chamfer_distance(dataset.train_col, sample_list, set_distance='cosine', dataset=dataset)
    dist_wasserstein, dist_chamfer = metrics.wasserstein_chamfer_distance(dataset.train_col, sample_list,
                                                                          set_distance='cosine_embedded', dataset=dataset, generator=generator)
    print(dist_wasserstein)
    log_file.write("Wasserstein : ({:.5f}, ({:.5f}, {:.5f}))\n".format(dist_wasserstein[0], dist_wasserstein[1][0], dist_wasserstein[1][1]))
    log_file.write("Chamfer : ({:.5f}, ({:.5f}, {:.5f}))\n".format(dist_chamfer[0], dist_chamfer[1][0], dist_chamfer[1][1]))

    # After having done the adversarial learning stopped with our overfitting criterion
    log_file.write("\nAdversarially trained Generator\n")
    path = os.getcwd() + '/generator_dual_trained.pt'
    evaluated_gen = torch.load(path)
    n_train = len(dataset.train_col)
    sample = evaluated_gen.sample(n_train)
    sample_list = sample.numpy().tolist()
    # dist_wasserstein, dist_chamfer = metrics.wasserstein_chamfer_distance(dataset.train_col, sample_list)
    dist_wasserstein, dist_chamfer = metrics.wasserstein_chamfer_distance(dataset.train_col, sample_list,
                                                                          set_distance='cosine_embedded', dataset=dataset, generator=evaluated_gen)
    print(dist_wasserstein)
    log_file.write("Wasserstein : ({:.5f}, ({:.5f}, {:.5f}))\n".format(dist_wasserstein[0], dist_wasserstein[1][0], dist_wasserstein[1][1]))
    log_file.write("Chamfer : ({:.5f}, ({:.5f}, {:.5f}))\n".format(dist_chamfer[0], dist_chamfer[1][0], dist_chamfer[1][1]))

    # After having "overfitted" in the adversarial learning
    log_file.write("\nAdversarially overfitted Generator\n")
    for epoch_nb in range(6, MAX_EPOCH + 1):
        print(epoch_nb)
        path = os.getcwd() + '/tmp_dual_models/generator_epoch' + str(epoch_nb) + '.pt'
        evaluated_gen = torch.load(path)
        n_train = len(dataset.train_col)
        sample = evaluated_gen.sample(n_train)
        sample_list = sample.numpy().tolist()
        dist_wasserstein, dist_chamfer = metrics.wasserstein_chamfer_distance(dataset.train_col, sample_list)
        print(dist_wasserstein)
        log_file.write("Wasserstein : ({:.5f}, ({:.5f}, {:.5f}))\n".format(dist_wasserstein[0], dist_wasserstein[1][0], dist_wasserstein[1][1]))
        log_file.write("Chamfer : ({:.5f}, ({:.5f}, {:.5f}))\n".format(dist_chamfer[0], dist_chamfer[1][0], dist_chamfer[1][1]))

    log_file.close()