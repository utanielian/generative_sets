import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch
from Models.modules_set_transformer import PMA, SAB
from data import get_data


class Generator(nn.Module):     # Input : None, Output : Set of gumbel_softmax one_hot [SET_SIZE, N_CLASS]
    def __init__(self, data_dict, dim_hidden=64, n_head=4):
        super(Generator, self).__init__()

        self.dim_output = data_dict['N_CLASS']
        self.dim_in = data_dict['SET_SIZE']
        self.batch_size = data_dict['BATCH_SIZE']
        self.data_dict = data_dict

        self.model = nn.Sequential(
            SAB(dim_in=self.dim_in, dim_out=dim_hidden, num_heads=n_head),
            PMA(dim=dim_hidden, num_heads=n_head, num_seeds=1),
            nn.Linear(in_features=dim_hidden, out_features=self.dim_output)
            )

        def weights_init(m):
            if isinstance(m, nn.Linear):
                nn.init.kaiming_normal_(m.weight)
                nn.init.zeros_(m.bias)
        self.apply(weights_init)

    def pad_list_to_tensor(self, item_list):
        set = []
        for item in item_list:
            set.append(item)
        # Padding the set with empty item
        for _ in range(len(set), self.data_dict['SET_SIZE']):
            no_item = torch.zeros(self.data_dict['N_CLASS'])
            set.append(no_item)
        return torch.stack(set).view(1, self.data_dict['N_CLASS'], self.data_dict['SET_SIZE'])

    def gumbel(self, shape, eps=1e-20):
        U = torch.rand(shape)
        return -torch.log(-torch.log(U + eps) + eps)

    def gumbel_softmax(self, logits, temperature):
        y = logits + self.gumbel(logits.size())
        y = F.softmax(y / temperature, dim=-1)
        shape = y.size()
        _, ind = y.max(dim=-1)
        y_hard = torch.zeros_like(y).view(-1, shape[-1])
        y_hard.scatter_(1, ind.view(-1, 1), 1)
        y_hard = y_hard.view(*shape)
        y_hard = (y_hard - y).detach() + y
        return y_hard, y
    def compute_pred_hot(self, pred):
        shape = pred.size()
        _, ind = pred.max(dim=-1)
        y_hard = torch.zeros_like(pred).view(-1, shape[-1])
        y_hard.scatter_(1, ind.view(-1, 1), 1)
        y_hard = y_hard.view(*shape)
        y_hard = (y_hard - pred).detach() + pred
        return y_hard

    def forward(self, temperature=1):
        gumbel_list = []
        germ = self.pad_list_to_tensor(gumbel_list)             # germ is a null set= zeros of shape [SET_SIZE, N_CLASS]
        output = self.model(germ)
        output = output[0, 0, :]
        logits = output / torch.sum(output)  # Transform raw output of the model into logits
        pred_hot, pred = self.gumbel_softmax(logits, temperature)       # pred is a one_hot vector of size N_CLASS
        pred = F.softmax(output, dim=0)
        gumbel_list.append(pred)
        item_idx = 1

        while item_idx < self.data_dict['SET_SIZE']:
            set = self.pad_list_to_tensor(gumbel_list)          # First items of the set are probs vect padded w/ zeros
            output = self.model(set)
            output = output[0, 0, :]
            logits = output / torch.sum(output)  # Transform raw output of the model into logits
            pred_hot, pred = self.gumbel_softmax(logits, temperature)  # pred is a one_hot vector of size N_CLASS
            # pred = F.softmax(output, dim=0)
            pred_hot = self.compute_pred_hot(pred)
            if pred_hot[-1] == 1:
                break
            gumbel_list.append(pred)
            item_idx = item_idx + 1

        for _ in range(len(gumbel_list), self.data_dict['SET_SIZE']):
            no_item = torch.zeros(self.data_dict['N_CLASS'])
            gumbel_list.append(no_item)
        return torch.stack(gumbel_list)


class Discriminator(nn.Module):     # Input : Set [SET_SIZE, N_CLASS], Output : Sigmoid bw/ 0 and 1
    def __init__(self, data_dict, dim_hidden=64, n_head=4):
        super(Discriminator, self).__init__()

        self.dim_output = data_dict['N_CLASS']
        self.dim_in = data_dict['SET_SIZE']
        self.batch_size = data_dict['BATCH_SIZE']
        self.data_dict = data_dict

        self.model = nn.Sequential(
            SAB(dim_in=self.dim_in, dim_out=dim_hidden, num_heads=n_head),
            PMA(dim=dim_hidden, num_heads=n_head, num_seeds=1),
            nn.Linear(in_features=dim_hidden, out_features=1)
            )
        self.activation = nn.Sigmoid()

        def weights_init(m):
            if isinstance(m, nn.Linear):
                nn.init.kaiming_uniform_(m.weight)
                nn.init.zeros_(m.bias)

        self.apply(weights_init)

    def forward(self, minibatch):
        input = minibatch.view(1, self.data_dict['N_CLASS'], self.data_dict['SET_SIZE'])
        out = self.model(input)
        return self.activation(out)     # Sigmoid activation


if __name__ == '__main__':
    file_path = '/Files/1_100_100_100_apparel-diaper-feeding_regs.csv'

    data_dict = get_data(file_path, ratio_train=0.005, batch_size=3, sorted=False)
    generator = Generator(data_dict)
    discriminator = Discriminator(data_dict)
    print(generator)
    print(discriminator)
