from Models.dpp_builder import build_dpp
from Models.dpp_builder.nonsymmetric_dpp_learning import compute_log_likelihood
from data import  get_data
import torch.optim as optim
import torch
import torch.nn as nn
import metrics
from copy import deepcopy
from dppy.finite_dpps import FiniteDPP
import numpy as np
from Models.modules_set_transformer import PMA, SAB
from time import time

def sample(model, collection_len): # shifted to sublinear sampling : DPPy sampling
    collection_sample = []

    # Convert kernel to marginal kernel (K)
    L = get_kernel(model)
    num_items_in_catalog = L.size()[0]
    eye = torch.eye(num_items_in_catalog)  # Id pour le terme de normalisation
    K = eye - (L + eye).inverse()  # K = Id - (L + Id)-1

    for _ in range(collection_len):
        K_copy = K.clone().detach()
        one_set = []
        for j in range(num_items_in_catalog):
            if torch.rand(1) < K_copy[j, j]:
                item = j
                one_set.append(item)
            else:
                K_copy[j, j] -= 1
            K_copy[j + 1:, j] /= K_copy[j, j]
            K_copy[j + 1:, j + 1:] -= torch.ger(K_copy[j + 1:, j], K_copy[j, j + 1:])
        if len(one_set) == 0:
            continue
        collection_sample.append(one_set)
    return collection_sample


def get_prediction(model, minibatch):
    loglikeli = compute_log_likelihood(model, minibatch)
    batch_pred = torch.exp(loglikeli)
    out = batch_pred / batch_pred.sum()
    # print(batch_pred)
    # print(out)
    return out


def train_adversarial_generator(gen, dis, optimizer, n_minibatch, minibatch_len):
    avg_loss = 0.
    criterion = nn.BCELoss()
    fooled_target = torch.DoubleTensor([1])

    for _ in range(n_minibatch):
        sampled_minibatch = sample(gen, minibatch_len)
        fake_pred = get_prediction(dis, sampled_minibatch)
        optimizer.zero_grad()
        loss = criterion(fake_pred, fooled_target)
        avg_loss = avg_loss + loss.item()
        loss.backward()       # We want it to classify sampled sets as True sets
        optimizer.step()

    return avg_loss / n_minibatch

def map_item_to_index(minibatch):
    def map(l): return [item - 1 for item in l]
    return [map(set) for set in minibatch]


def list_to_padded_tensor(arg_list, set_len):
    padded_batch = []
    for s in arg_list:
        padded_s = deepcopy(s)
        for _ in range(len(padded_s), set_len):
            padded_s.append(0)
        padded_batch.append(padded_s)
    return torch.tensor(padded_batch)

def train_adversarial_discriminator(dpp_sampler, dis, optimizer, loader, fake_samples, minibatch_len=64, set_len=24):
    avg_loss = 0.
    true_acc = 0.
    fake_acc = 0.

    criterion = nn.BCELoss()
    n_minibatch = int(len(loader) / minibatch_len)
    # for _ in range(len(loader) * minibatch_len):
    #     dpp_sampler.sample_exact(mode='vfx', verbose=False)
    # fake_samples = dpp_sampler.list_of_samples

    for idx in range(n_minibatch):
        optimizer.zero_grad()
        # Real data
        true_set = loader[int(idx*minibatch_len) : int((idx+1)*minibatch_len)]
        true_index = map_item_to_index(true_set) # need to shift to the index to compute the BCELoss
        true_pred = get_prediction(dis, true_index)
        print(true_pred)
        # padded_true_index = list_to_padded_tensor(true_index, set_len)
        # true_pred = dis(padded_true_index)
        true_acc = true_acc + len(true_pred[true_pred > 0.5])
        true_target = torch.ones(true_pred.shape)
        true_loss = criterion(true_pred, true_target)
        avg_loss = avg_loss + true_loss.item()
        # print(true_loss)
        true_loss.backward()

        # Fake data sampling
        fake_minibatch = fake_samples[int(idx*minibatch_len) : int(minibatch_len*(idx+1))]
        fake_pred = get_prediction(dis, fake_minibatch)
        # print(fake_pred)
        # padded_fake_minibatch = list_to_padded_tensor(fake_minibatch, set_len)
        # fake_pred = dis(padded_fake_minibatch)
        fake_acc = fake_acc + len(fake_pred[fake_pred < 0.5])
        fake_target = torch.zeros(fake_pred.shape)
        fake_loss = criterion(fake_pred, fake_target)
        # print(fake_loss)
        avg_loss = avg_loss + fake_loss.item()
        fake_loss.backward()
        optimizer.step()

    avg_loss = avg_loss / (len(loader) * 2)
    fake_acc = fake_acc / (len(loader))
    true_acc = true_acc / (len(loader))
    return avg_loss, (true_acc, fake_acc)


def get_kernel(model, nonsymmetric=False):
    V = model.get_v_embeddings()
    if nonsymmetric:
      B = model.get_b_embeddings()
      C = model.get_c_embeddings()
      L = V.mm(V.transpose(0, 1)) + B.mm(C.transpose(0, 1)) - C.mm(B.transpose(0, 1))
    else:
        L = V.mm(V.transpose(0, 1))
    return L

def eval_kernel(X, Y=None):
    if X is None:
        raise ValueError('Error of eval kernel function')
    if Y is None:
        return eval_kernel(X, Y=X)
    return L_gen[X.flatten(), :][:, Y.flatten()]

class SetTransformer_fullset_Discriminator(nn.Module):
    def __init__(self, vocab_size, embedding_dim, hidden_dim, cuda, n_head=4):
        super(SetTransformer_fullset_Discriminator, self).__init__()
        self.hidden_dim = hidden_dim
        self.use_cuda = cuda
        self.vocab_size = vocab_size
        self.embed = nn.Embedding(vocab_size, embedding_dim, padding_idx=0)
        self.fc_out = nn.Linear(hidden_dim, 1)
        self.model = nn.Sequential(
            SAB(dim_in=embedding_dim, dim_out=hidden_dim, num_heads=n_head),
            PMA(dim=hidden_dim, num_heads=1, num_seeds=1))
    '''
    In : One Set
    Out : One sigmoid value
    '''
    def forward(self, x):
        emb = self.embed(x.type(torch.long))
        out = self.model(emb)
        pred = torch.sigmoid(self.fc_out(out))
        return pred[:,0,0]

if __name__ == '__main__':
    generator = None
    discriminator = None
    G_STEP = 1
    D_STEP = 1

    # Get Data
    print('#####################################################')
    print('Building dataset')
    print('#####################################################\n')
    batch_size = 64
    file_path = '/Files/1_100_100_100_apparel-diaper-feeding_regs.csv'
    # file_path = '/Files/toy_problem.csv'
    dataset = get_data(file_path, ratio_train=.8, batch_size=batch_size)
    # dataset = get_data_all_subset(file_path, ratio_train=.8, batch_size=128, max_set_size=10)

    # Argument for the DPPy vfx sampling
    X_data = np.arange(300)
    X_data = np.reshape(X_data, (300, 1))

    print('#####################################################')
    print('Start Generator Pre-Training')
    print('#####################################################\n')
    generator = build_dpp.build(file_path, dataset)             # Non symmetric DPP object
    generator.train()

    print('#####################################################')
    print('Start Discriminator Pre-Training')
    print('#####################################################\n')
    discriminator = build_dpp.build(file_path, dataset)        # Non symmetric DPP object
    # Do MLE pretraining sigmoid loss
    discriminator.train()

    # g_embed_dim = 32
    # g_hidden_dim = 64
    # g_seq_len = 24
    # vocab_size = 300
    # st_discriminator_fullset = SetTransformer_fullset_Discriminator(vocab_size, g_embed_dim, g_hidden_dim, cuda=False)


    optimizer_gen = optim.Adam(params=generator.parameters(), lr=1e-3)
    optimizer_dis = optim.Adam(params=discriminator.parameters(), lr=1e-2)
    # optimizer_dis = optim.Adam(params=st_discriminator_fullset.parameters(), lr=1e-5)

    print('#####################################################')
    print('Adversarial Training')
    print('#####################################################\n')
    MAX_EPOCH = 50
    for epoch in range(MAX_EPOCH):
        for g_step in range(G_STEP):
            gen_loss = train_adversarial_generator(generator, discriminator, optimizer_gen, len(dataset.raw_train_loader),
                                        dataset.batch_size)
        global L_gen
        L_gen = get_kernel(generator).detach().numpy()
        dpp_sampler = FiniteDPP('likelihood',
                                **{'L_eval_X_data': (eval_kernel, X_data)})

        for g_step in range(20):
            dis_loss, (real_acc, fake_acc) = train_adversarial_discriminator(dpp_sampler, discriminator, optimizer_dis, dataset.train_col, fake_samples)
            # print('Sampling fake sets')
            # start = time()
            for _ in range(len(dataset.train_col)):
                dpp_sampler.sample_exact(mode='vfx', verbose=False)
            fake_samples = dpp_sampler.list_of_samples
            # dis_loss, (real_acc, fake_acc) = train_adversarial_discriminator(dpp_sampler, st_discriminator_fullset, optimizer_dis, dataset.train_col, fake_samples)
            # end = time() - start
            print(dis_loss)
            print(real_acc)
            print(fake_acc)
            # print('Time : ', end)

# 24960
for _ in range(len(dataset.raw_train_loader) * 64):
    print()
    dpp_sampler.sample_exact(mode='vfx', verbose=False)
fake_samples = dpp_sampler.list_of_samples

for true_set in dataset.raw_train_loader:
    print(true_set)
    break
for true_col in dataset.train_col:
    print()
    break
true_index = map_item_to_index(true_set)  # need to shift to the index to compute the BCELoss
# true_pred = get_prediction(dis, true_index)
padded_true_index = list_to_padded_tensor(true_index, set_len)
true_pred = dis(padded_true_index)
true_acc = true_acc + len(true_pred[true_pred > 0.5])
true_target = torch.ones(true_pred.shape)