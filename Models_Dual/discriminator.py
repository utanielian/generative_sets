import torch
import torch.nn as nn
import torch.nn.functional as F
from copy import deepcopy
import random
torch.random.manual_seed(5)
random.seed(5)
from Models.modules_set_transformer import PMA, SAB
import numpy as np

class Discriminator(nn.Module):
    """ Discriminator """
    def __init__(self, vocab_size, embedding_dim, hidden_dim, use_cuda):
        super(Discriminator, self).__init__()
        self.hidden_dim = hidden_dim
        self.use_cuda = use_cuda
        self.vocab_size = vocab_size
        self.embed = nn.Embedding(vocab_size, embedding_dim, padding_idx=0)
        self.model = None
        self.fc = nn.Linear(hidden_dim, 1)

    def forward(self, x):
        """
    Embeds the input set, then goes through the model and finally return a scalar whether

        Inputs: x
            - x: (batch_size, set_len), batch of sets of size set_len
        Outputs: out
            - out: (batch_size, vocab_size), model output prediction
        """
        return


class LSTM_Discriminator(Discriminator):
    def __init__(self, vocab_size, embedding_dim, hidden_dim, use_cuda):
        super().__init__(vocab_size, embedding_dim, hidden_dim, use_cuda)
        self.model = nn.LSTM(embedding_dim, hidden_dim, batch_first=True)
    def forward(self, x):
        self.model.flatten_parameters()
        emb = self.embed(x.type(torch.long))
        enc, _ = self.model(emb.type(torch.float))
        out = self.fc(enc)
        pred = out[:, -1, :]
        return torch.sigmoid(pred)


class SetTransformer_Discriminator(Discriminator):
    def __init__(self, vocab_size, embedding_dim, hidden_dim, use_cuda, n_head=4):
        super().__init__(vocab_size, embedding_dim, hidden_dim, use_cuda)
        self.model = nn.Sequential(
            SAB(dim_in=embedding_dim, dim_out=hidden_dim, num_heads=n_head),
            PMA(dim=hidden_dim, num_heads=1, num_seeds=1))
    def forward(self, x):
        emb = self.embed(x.type(torch.long))
        out = self.model(emb)
        out_fc = self.fc(out.reshape(-1, self.hidden_dim))
        return torch.sigmoid(out_fc)


class DeepSet_Discriminator(Discriminator):
    def __init__(self, vocab_size, embedding_dim, hidden_dim, use_cuda, n_enc=2, n_dec=2):
        super().__init__(vocab_size, embedding_dim, hidden_dim, use_cuda)
        self.enc = []
        self.enc.append(nn.Linear(embedding_dim, hidden_dim))
        self.enc.append(nn.ReLU())
        for _ in range(n_enc):
            self.enc.append(nn.Linear(hidden_dim, hidden_dim))
            self.enc.append(nn.ReLU())
        self.enc.append(nn.Linear(hidden_dim, hidden_dim))
        self.enc = nn.Sequential(*self.enc)
        self.dec = []
        for _ in range(n_dec):
            self.dec.append(nn.Linear(hidden_dim, hidden_dim))
            self.dec.append(nn.ReLU())
        self.dec = nn.Sequential(*self.dec)
    def forward(self, x):
        emb = self.embed(x.type(torch.long))
        enc = self.enc(emb)
        pool = enc.max(dim=1)[0]
        out = self.dec(pool)
        out = self.fc(out)
        return torch.sigmoid(out)

class Conv_Discriminator(nn.Module):
    """
    A CNN for text classification.
    Uses an embedding layer, followed by a convolutional, max-pooling and softmax layer.
    Highway architecture based on the pooled feature maps is added. Dropout is adopted.
    """

    def __init__(self, num_classes, vocab_size, embedding_dim, filter_sizes, num_filters, dropout_prob):
        super(Conv_Discriminator, self).__init__()
        self.embed = nn.Embedding(vocab_size, embedding_dim)
        self.convs = nn.ModuleList([
            nn.Conv2d(1, num_f, (f_size, embedding_dim)) for f_size, num_f in zip(filter_sizes, num_filters)
        ])
        self.highway = nn.Linear(sum(num_filters), sum(num_filters))
        self.dropout = nn.Dropout(p = dropout_prob)
        self.fc = nn.Linear(sum(num_filters), num_classes)

    def forward(self, x):
        """
        Inputs: x
            - x: (batch_size, seq_len)
        Outputs: out
            - out: (batch_size, num_classes)
        """
        emb = self.embed(x.type(torch.long)).unsqueeze(1)
        convs = [F.relu(conv(emb)).squeeze(3) for conv in self.convs]
        pools = [F.max_pool1d(conv, conv.size(2)).squeeze(2) for conv in convs]
        out = torch.cat(pools, 1)
        highway = self.highway(out)
        transform = torch.sigmoid(highway)
        out = transform * F.relu(highway) + (1. - transform) * out
        out = torch.sigmoid(self.fc(self.dropout(out)))
        return out