import numpy as np
import torch.nn as nn
import torch
from Models.modules_set_transformer import PMA, SAB
from data import get_data
from torch.autograd import Variable

class Generator(nn.Module):     # Takes nothing as input and output a minibatch of padded sets
    def __init__(self, data_dict, dim_hidden=64, n_head=4):
        super(Generator, self).__init__()

        self.dim_output = data_dict['SET_SIZE']
        self.batch_size = data_dict['BATCH_SIZE']
        self.data_dict = data_dict
        # Input : set / Output : item to be added to the set
        self.model = nn.Sequential(
            SAB(dim_in=1, dim_out=dim_hidden, num_heads=n_head),
            PMA(dim=dim_hidden, num_heads=n_head, num_seeds=1),
            nn.Linear(in_features=dim_hidden, out_features=self.dim_output)
            )
        self.softmax = nn.Softmax(dim=1)
        self.linear = nn.Linear(in_features=10, out_features=23)

        def weights_init(m):
            if isinstance(m, nn.Linear):
                nn.init.kaiming_normal_(m.weight)
                nn.init.zeros_(m.bias)

        self.apply(weights_init)

    def mask_already_item_pred(self, vect_pred, sample):
        updated_vect = vect_pred.clone()
        updated_vect = updated_vect[0]
        for item in sample:
            if item == 0:
                continue
            item_idx = item-1
            updated_vect[int(item_idx)] = 0
        return updated_vect

    def sample_from_noise(self, sampled_set, temperature):
        input_model = sampled_set.unsqueeze(0).unsqueeze(-1)
        output = self.model(input_model)
        return output[0,0,:]
        # output = output.squeeze(1)
        # vect_pred = self.softmax(output / temperature)
        # # vect_pred = self.mask_already_item_pred(vect_pred, sampled_set.tolist())
        # idx_class = torch.multinomial(vect_pred, num_samples=1)
        #
        # new_item = self.data_dict['ALL_CLASS'][idx_class]
        # return
        # if new_item == self.data_dict['STOP_CLASS']:
        #     break
        # sampled_set[item_idx] = float(new_item)
        # item_idx = item_idx + 1

        return sampled_set
    def forward(self, noise, temperature=1):        # Noise is a padded set with random class at the first position
        minibatch = self.sample_from_noise(noise, temperature)
        return minibatch



class Discriminator(nn.Module):     # Takes as input 1 set and outputs a scalar wether or not it's real
    def __init__(self, data_dict, dim_hidden=64, n_head=4):
        super(Discriminator, self).__init__()

        self.dim_output = data_dict['N_CLASS']
        self.batch_size = data_dict['BATCH_SIZE']
        self.data_dict = data_dict

        self.model = nn.Sequential(
            SAB(dim_in=1, dim_out=dim_hidden, num_heads=n_head),
            PMA(dim=dim_hidden, num_heads=n_head, num_seeds=1),
            nn.Linear(in_features=dim_hidden, out_features=1)
        )
        self.activation = nn.Sigmoid()

        def weights_init(m):
            if isinstance(m, nn.Linear):
                nn.init.kaiming_uniform_(m.weight)
                nn.init.zeros_(m.bias)

        self.apply(weights_init)

    def forward(self, minibatch):
        input = minibatch.view(1, self.data_dict['SET_SIZE'], self.data_dict['BATCH_SIZE'])
        out = self.model(input)
        return self.activation(out).view(1)     # Sigmoid activation


if __name__ == '__main__':
    file_path = '/Files/1_100_100_100_apparel-diaper-feeding_regs.csv'

    data_dict = get_data(file_path, ratio_train=0.005, batch_size=1, sorted=False)
    generator = Generator(data_dict)
    discriminator = Discriminator(data_dict)
    print(generator)
    print(discriminator)
    valid_item_list = data_dict['ALL_CLASS'][:-1]  # Minus the stop item


    def generate_noise(item_list=valid_item_list):
        # noise_batch = torch.tensor([])
        # for _ in range(data_dict['BATCH_SIZE']):
        #     noise_set = torch.FloatTensor([0 for _ in range(data_dict['SET_SIZE'])])
        #     noise_set[0] = float(item_list[np.random.choice(len(item_list))])
        #     noise_batch = torch.cat((noise_batch, noise_set.unsqueeze(0)))
        # noise_batch.requires_grad = True
        batch_size = int(data_dict['BATCH_SIZE'])
        set_size = int(data_dict['SET_SIZE'])
        min_item = int(data_dict['ALL_CLASS'][0])
        max_item = int(data_dict['ALL_CLASS'][-1])

        germ = torch.FloatTensor(set_size).random_(min_item, max_item)
        # germ[1:] = 0
        # germ.requires_grad = True
        return Variable(germ)  # noise_batch


    generate_noise().shape
    print(generator(generate_noise()))
