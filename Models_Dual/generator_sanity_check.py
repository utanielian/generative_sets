import torch.nn as nn
import torch
from data import get_data
from Models_Dual.set_transformer_dual import Generator, Discriminator
import numpy as np

# Define the models
file_path = '/Files/1_100_100_100_apparel-diaper-feeding_regs.csv'
data_dict = get_data(file_path, ratio_train=0.005, batch_size=3, sorted=False)
generator = Generator(data_dict)
discriminator = Discriminator(data_dict)

lr = 0.0001
# Optimizers & Loss
optimizer = torch.optim.Adam(generator.parameters(), lr=lr)
criterion = nn.BCELoss()

#Define the noise function
valid_item_list = data_dict['ALL_CLASS'][:-1] # Minus the stop item
def generate_noise(item_list=valid_item_list):
    noise_batch = torch.tensor([])
    for _ in range(data_dict['BATCH_SIZE']):
        noise_set = torch.FloatTensor([0 for _ in range(data_dict['SET_SIZE'])])
        noise_set[0] = float(item_list[np.random.choice(len(item_list))])
        noise_batch = torch.cat((noise_batch, noise_set.unsqueeze(0)))
    noise_batch.requires_grad = True
    return noise_batch


# Training
n_epochs = 100
epoch_loss_list = []
for epoch in range(n_epochs):
    epoch_loss = 0

    for _ in range(10):
        # Generate a minibatch of sets
        noise = generate_noise()

        # Forward pass of the generator
        sampled_batch = generator(noise)
        n_fake = discriminator(sampled_batch)
        g_loss = 1 - criterion(n_fake, torch.tensor([1.]))

        # Update of the generator
        optimizer.zero_grad()
        g_loss.backward()
        optimizer.step()

        epoch_loss = epoch_loss + g_loss.item()

    print('EPOCH_LOSS (G) : ', epoch_loss)
    print()
