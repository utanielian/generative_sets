"""
Synopsis: Class intended to replace the command line use of the repository.
"""
import os


class SubArg(object):

    def __init__(self, file_path):
        self.hogwild = False
        self.scores_file = os.getcwd() + '/Models/dpp_builder/' + 'nonsymmetric-DPP-eval-scores'
        self.batch_size = 64 #200
        self.input_file = os.getcwd() + file_path
        self.input_file_test_negatives = None
        self.disjoint_sets_file_w = None
        self.input_file_disjoint_sets = None
        self.num_iterations = 1000
        self.num_baskets = None
        self.max_basket_size = float('inf')
        self.alpha = 0.1
        self.beta = 0.0
        self.gamma = 0.0
        self.use_metadata = False
        self.use_price = False
        self.use_fasttext = False
        self.prepend_meta = True
        self.num_threads = 1
        self.db_path = 'logs.db'
        self.disable_gpu = False
        self.dataset_name = 'basket_ids'
        self.hidden_dims = []
        self.num_sym_embedding_dims = 3
        self.num_nonsym_embedding_dims = 0 # 30 / In order to make a symmetric DPP
        self.product_id_embedding_dim = 30
        self.aisle_id_embedding_dim = 20
        self.department_id_embedding_dim = 20
        self.learning_rate = 0.1
        self.activation = 'selu'
        self.dropout = 0
        self.persisted_model_dir = os.getcwd() + '/Models/dpp_builder/' + 'dpp_saved_models'
        self.kernel_collection_dir = os.getcwd() + 'Models/dpp_builder/'
        self.num_val_baskets = None
        self.num_test_baskets = None
        self.inference = True
        self.tsne = False
        self.disable_eval = True
        self.num_bootstraps = 1
