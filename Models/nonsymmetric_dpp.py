import torch
import os
import pickle
import torch.nn as nn
from Models.dpp_builder import build_dpp
from tqdm import tqdm
from data import get_data
from metrics import wasserstein_chamfer_distance
import matplotlib.pyplot as plt
import random
import numpy as np
torch.random.manual_seed(5)
random.seed(5)
np.random.seed(5)


class NonsymmetricDppBaseline(nn.Module):

    def __init__(self, dataset):
        super(NonsymmetricDppBaseline, self).__init__()

        self.dataset = dataset
        self.kernel = None

    def train(self, file_path):
        model = build_dpp.build(file_path, self.dataset)
        L = self.get_kernel(model)
        self.kernel = L

    def get_kernel(self, model, nonsymmetric=False):
        V = model.get_v_embeddings()
        if nonsymmetric:
            B = model.get_b_embeddings()
            C = model.get_c_embeddings()
            L = V.mm(V.transpose(0, 1)) + B.mm(C.transpose(0, 1)) - C.mm(B.transpose(0, 1))
        else:
            L = V.mm(V.transpose(0, 1))
        return L

    def sample(self, collection_len, save=False):
        collection_sample = []

        # Convert kernel to marginal kernel (K)
        L = self.kernel
        num_items_in_catalog = L.size()[0]
        eye = torch.eye(num_items_in_catalog)       # Id pour le terme de normalisation
        K = eye - (L + eye).inverse()        # K = Id - (L + Id)-1

        for _ in tqdm(range(collection_len)):
            K_copy = K.clone().detach()
            one_set = []

            for j in range(num_items_in_catalog):
                if torch.rand(1) < K_copy[j, j]:
                    one_set.append(j)
                else:
                    K_copy[j, j] -= 1

                K_copy[j + 1:, j] /= K_copy[j, j]
                K_copy[j + 1:, j + 1:] -= torch.ger(K_copy[j + 1:, j], K_copy[j, j + 1:])

            if len(one_set) == 0:           # The empty set of item in not considered as an order
                continue

            collection_sample.append(one_set)

        if save:
            root = os.getcwd()
            folder_path = root + '/Results/'
            if not os.path.isdir(folder_path):
                os.makedirs(folder_path)

            pickle_out = open(folder_path + "nonsymmetric_dpp_model_sample.pickle", "wb")
            pickle.dump(collection_sample, pickle_out)
            pickle_out.close()

        return collection_sample

    def sample_from_item(self, item, set_size):
        sample_set = [item]
        # Convert kernel to marginal kernel (K)
        L = self.kernel
        num_items_in_catalog = L.size()[0]
        eye = torch.eye(num_items_in_catalog)       # Id pour le terme de normalisation
        K = eye - (L + eye).inverse()        # K = Id - (L + Id)-1

        while len(sample_set) < set_size:
            K_copy = K.clone().detach()

            for j in range(num_items_in_catalog):
                if torch.rand(1) < K_copy[j, j]:
                    sample_set.append(j)
                else:
                    K_copy[j, j] -= 1

                K_copy[j + 1:, j] /= K_copy[j, j]
                K_copy[j + 1:, j + 1:] -= torch.ger(K_copy[j + 1:, j], K_copy[j, j + 1:])

        return sample_set

    def sample_from_set(self, arg_set, nb_new_item):
        sample_set = arg_set
        item_count = 0
        # Convert kernel to marginal kernel (K)
        L = self.kernel
        num_items_in_catalog = L.size()[0]
        eye = torch.eye(num_items_in_catalog)       # Id pour le terme de normalisation
        K = eye - (L + eye).inverse()        # K = Id - (L + Id)-1

        while item_count < nb_new_item:
            K_copy = K.clone().detach()

            for j in range(num_items_in_catalog):
                if torch.rand(1) < K_copy[j, j]:
                    if j not in sample_set:
                        sample_set.append(j)
                        item_count = item_count + 1
                else:
                    K_copy[j, j] -= 1

                K_copy[j + 1:, j] /= K_copy[j, j]
                K_copy[j + 1:, j + 1:] -= torch.ger(K_copy[j + 1:, j], K_copy[j, j + 1:])

        return sample_set

if __name__ == '__main__':

    file_path = '/Files/1_100_100_100_apparel-diaper-feeding_regs.csv'
    dataset = get_data(file_path, ratio_train=0.8, sorted=False)
    model = NonsymmetricDppBaseline(dataset)
    model.train(file_path)

    print('AFTER TRAINING')
    n_train = len(dataset.train_col)
    sample = model.sample(n_train)
    dist_wasserstein, dist_chamfer = wasserstein_chamfer_distance(dataset.train_col,sample)
    print("The distance (mean, (min,max)) between model and train data")
    print("Wasserstein : ", dist_wasserstein)
    print("Chamfer : ", dist_chamfer)

    n_test = len(dataset.test_col)
    sample = model.sample(n_test)
    dist_wasserstein, dist_chamfer = wasserstein_chamfer_distance(dataset.test_col,sample)
    print("The distance (mean, (min,max)) between model and test data")
    print("Wasserstein : ", dist_wasserstein)
    print("Chamfer : ", dist_chamfer)
    # torch.save(model.state_dict(), os.getcwd() + '/Results/NonsymmetricDPP/final_model.pt')
