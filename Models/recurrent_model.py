import torch
from torch import nn
from torch.nn import functional as F
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint, GradientAccumulationScheduler, EarlyStopping
import os
import pickle
from data import get_data
from metrics import wasserstein_chamfer_distance
import matplotlib.pyplot as plt
from tqdm import tqdm
import random
import numpy as np
torch.random.manual_seed(5)
random.seed(5)
np.random.seed(5)


'''
Class definition and everything needed to operates this baseline
'''


class RecurrentModelBaseline(pl.LightningModule):
    def __init__(self, dataset, n_cell=100, embedding_dim=32, n_layer=1):
        super(RecurrentModelBaseline, self).__init__()
        self.dataset = dataset
        self.vocab_size = dataset.n_item
        self.embed = nn.Embedding(self.vocab_size, embedding_dim, padding_idx=0)
        self.model = nn.LSTM(embedding_dim, n_cell, batch_first=True)
        self.fc = nn.Linear(n_cell, self.vocab_size, bias=True)

    def forward(self, x, temperature=1):
        self.model.flatten_parameters()
        emb = self.embed(x.type(torch.long))
        enc, _ = self.model(emb.type(torch.float))
        out = self.fc(enc)
        out = torch.log_softmax(out/temperature, dim=2)
        pred = out[:, -1, :]
        return pred

    def cross_entropy_loss(self, logits, labels):
        return F.nll_loss(logits, labels.contiguous().view(-1))

    def training_step(self, train_batch, batch_idx):
        x, y = train_batch
        logits = self.forward(x)
        y_node = y - 1
        loss = self.cross_entropy_loss(logits, y_node)

        logs = {'train_loss': loss}
        return {'loss': loss, 'log': logs}

    def validation_step(self, val_batch, batch_idx):
        x, y = val_batch
        logits = self.forward(x)
        y_node = y - 1
        loss = self.cross_entropy_loss(logits, y_node)
        return {'val_loss': loss}

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([x['val_loss'] for x in outputs]).mean()
        tensorboard_logs = {'val_loss': avg_loss}
        return {'avg_val_loss': avg_loss, 'log': tensorboard_logs}

    def test_step(self, test_batch, batch_idx):
        x, y = test_batch
        logits = self.forward(x)
        y_node = y - 1
        loss = self.cross_entropy_loss(logits, y_node)
        return {'test_loss': loss}

    def test_epoch_end(self, outputs):
        avg_loss = torch.stack([x['test_loss'] for x in outputs]).mean()
        tensorboard_logs = {'test_loss': avg_loss}
        return {'avg_test_loss': avg_loss, 'log': tensorboard_logs}

    def train_dataloader(self):
        return self.dataset.train_loader

    def val_dataloader(self):
        return self.dataset.val_loader

    def test_dataloader(self):
        return self.dataset.test_loader

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-2, weight_decay=1e-10)
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', patience=3,
                                                               verbose=True, factor=0.75)
        return [optimizer], [scheduler]

    @staticmethod
    def create_set_from_list(arg_set, set_size):
        one_set = []
        class_list = arg_set.copy()
        for c in class_list:
            one_set.append(c)
        for _ in range(set_size - len(one_set)):
            one_set.append(0)

        return torch.Tensor(one_set).unsqueeze(0)

    @staticmethod
    def create_empty_set(set_size):
        return torch.Tensor([0 for _ in range(set_size)]).unsqueeze(0)

    @staticmethod
    def mask_already_item_pred(vect_pred, sample):
        updated_vect = vect_pred.clone()
        for item in sample:
            item_idx = item-1
            updated_vect[0, item_idx] = 0
        return updated_vect


    def sample(self, collection_len, save=False):
        self.eval()
        collection_sample = []

        for _ in tqdm(range(collection_len)):
            sample = []
            # Sampling the first item from the empty set
            new_item = self.dataset.stop_item
            while new_item == self.dataset.stop_item:
                empty_set = self.create_empty_set(set_size=self.dataset.set_size)
                out = self(empty_set)
                # out = out.unsqueeze(0)
                vect_pred = torch.exp(out)
                idx_class = torch.multinomial(vect_pred.squeeze(0), num_samples=1)
                new_item = self.dataset.all_item[idx_class]
            sample.append(new_item)

            while new_item != self.dataset.stop_item and len(sample) < self.dataset.set_size:
                current_item_set = self.create_set_from_list(arg_set=sample, set_size=self.dataset.set_size)
                out = self(current_item_set)
                # out = out.unsqueeze(0)
                vect_pred = torch.exp(out)
                vect_pred = self.mask_already_item_pred(vect_pred, sample)
                idx_class = torch.multinomial(vect_pred.squeeze(0), num_samples=1)
                new_item = self.dataset.all_item[idx_class]
                sample.append(new_item)

            collection_sample.append(sample[:-1])

        if save:
            root = os.getcwd()
            folder_path = root + '/Results/'
            if not os.path.isdir(folder_path):
                os.makedirs(folder_path)

            pickle_out = open(folder_path + "recurrent_model_sample.pickle", "wb")
            pickle.dump(collection_sample, pickle_out)
            pickle_out.close()
        return collection_sample

    def sample_from_item(self, item, set_size):
        sample = [item]
        item_count = 1
        while item_count < set_size:
            current_item_set = self.create_set_from_list(arg_set=sample, set_size=self.dataset.set_size)
            out = self(current_item_set)
            vect_pred = torch.exp(out)
            vect_pred = self.mask_already_item_pred(vect_pred, sample)
            idx_class = torch.multinomial(vect_pred.squeeze(0), num_samples=1)
            new_item = self.dataset.all_item[idx_class]
            sample.append(new_item)
            item_count = item_count + 1
        return sample

    def sample_from_set(self, arg_set, nb_new_item):
        sample = arg_set
        item_count = 0
        while item_count < nb_new_item:
            current_item_set = self.create_set_from_list(arg_set=sample, set_size=self.dataset.set_size)
            out = self(current_item_set)
            vect_pred = torch.exp(out)
            vect_pred = self.mask_already_item_pred(vect_pred, sample)
            idx_class = torch.multinomial(vect_pred.squeeze(0), num_samples=1)
            new_item = self.dataset.all_item[idx_class]
            sample.append(new_item)
            item_count = item_count + 1
        return sample


if __name__ == '__main__':

    file_path = '/Files/1_100_100_100_apparel-diaper-feeding_regs.csv'
    file_path = '/Files/toy_problem.csv'
    # dataset = get_data(file_path, ratio_train=0.8, batch_size=800, sorted=True)
    dataset = get_data(file_path, ratio_train=0.8, batch_size=64, sorted=True)
    model = RecurrentModelBaseline(dataset, n_cell=200, n_layer=1)
    # checkpoint_callback = ModelCheckpoint(filepath=os.getcwd() + '/Results/Recurrent/', verbose=True)

    # print('BEFORE TRAINING')
    # n_train = len(dataset.train_col)
    # sample = model.sample(n_train)
    # dist_wasserstein, dist_chamfer = wasserstein_chamfer_distance(dataset.train_col, sample)
    # print("The distance (mean, (min,max)) between model and train data")
    # print("Wasserstein : ", dist_wasserstein)
    # print("Chamfer : ", dist_chamfer)
    #
    # n_test = len(dataset.test_col)
    # sample = model.sample(n_test)
    # dist_wasserstein, dist_chamfer = wasserstein_chamfer_distance(dataset.test_col, sample)
    # print("The distance (mean, (min,max)) between model and test data")
    # print("Wasserstein : ", dist_wasserstein)
    # print("Chamfer : ", dist_chamfer)
    #
    # dist_wasserstein, dist_chamfer = wasserstein_chamfer_distance(dataset.test_col,
    #                                                               dataset.train_col)
    # print("The distance (mean, (min,max)) between train and test data")
    # print("Wasserstein : ", dist_wasserstein)
    # print("Chamfer : ", dist_chamfer)

    print('TRAINING')
    early_stopping = EarlyStopping('val_loss', patience=10, verbose=True, mode='min', min_delta=1e-6)
    # trainer = pl.Trainer(checkpoint_callback=checkpoint_callback, max_nb_epochs=1000,
    #                      early_stop_callback=early_stopping)
    trainer = pl.Trainer(max_nb_epochs=5, early_stop_callback=early_stopping)
    trainer.fit(model)
    model.sample(5)
    # print('AFTER TRAINING')
    # n_train = len(dataset.train_col)
    # sample = model.sample(n_train)
    # dist_wasserstein, dist_chamfer = wasserstein_chamfer_distance(dataset.train_col,sample)
    # print("The distance (mean, (min,max)) between model and train data")
    # print("Wasserstein : ", dist_wasserstein)
    # print("Chamfer : ", dist_chamfer)
    #
    # n_test = len(dataset.test_col)
    # sample = model.sample(n_test)
    # dist_wasserstein, dist_chamfer = wasserstein_chamfer_distance(dataset.test_col,sample)
    # print("The distance (mean, (min,max)) between model and test data")
    # print("Wasserstein : ", dist_wasserstein)
    # print("Chamfer : ", dist_chamfer)

    # torch.save(model.state_dict(), os.getcwd() + '/Results/Recurrent/final_model.pt')
